from django.apps import AppConfig


class PoaGeneralConfig(AppConfig):
    name = 'POA_General'
