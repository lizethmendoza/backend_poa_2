from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .models import POAGeneral
from .serializers import POAGeneralSerializer

from .models import FormulariosGenerales
from .serializers import FormulariosGeneralesSerializer

from .models import ObjetivosGestionGeneral
from .serializers import ObjetivosGestionGeneralSerializer
from .serializers import formulariosOGSerializer

from .serializers import formulariosRespoCargoSerializer
from .serializers import FormulariosGeneralesDetallePOASerializer

from .models import RecursosHumanosGeneral
from .serializers import RecursosHumanosGeneralSerializer
from .serializers import formulariosRRHHSerializer

from .models import ServiciosNoPersonalesGeneral
from .serializers import ServiciosNoPersonalesGeneralSerializer
from .serializers import  formulariosSNPSerializer

from .models import MaterialesYSuministrosGeneral
from .serializers import MaterialesYSuministrosGeneralSerializer
from .serializers import  formulariosMSSerializer

from .models import activosFijosGeneral
from .serializers import activosFijosGeneralSerializer
from .serializers import formulariosAFSerializer

from .models import MontosProgramados
from .serializers import MontosProgramadosSerializer

from .models import  OperacionesFuncionamientoGeneral 
from .serializers import formulariosOFSerializer
from .serializers import OperacionesFuncionamientoGeneralSerializer

from .serializers import UnidadSolicitanteGeneralSerializer
from .serializers import UnidadesSolicitantesFormularioSerializer
from .models import UnidadSolicitanteGeneral
from .models import UnidadSolicitanteGeneralAF
from .serializers import UnidadSolicitanteGeneralAFSerializer
from .serializers import UnidadSolicitanteGeneralAFFormularioSerializer
from ..Formulacion.models import formularios
from django.db.models import Q
from .serializers import ObservacionPoaGeneralSerializer
from .models import ObservacionPoaGeneral

# Create your views here.
class ListaPOAGeneral(APIView):
    def get(self, request,format=None):
        ListaPOAGeneral = POAGeneral.objects.all().order_by('Gestion')
        serializer = POAGeneralSerializer(ListaPOAGeneral, many=True)
        return Response(serializer.data)

'''Saca el POA de una gestion dada'''
class ListaPoaGeneralGestion(APIView):
    def get(self, request, Gestion,version, format=None):        
        snippets= POAGeneral.objects.filter(Gestion=Gestion).filter(VersionPOA=version)
        serializer = POAGeneralSerializer(snippets,many=True)
        return Response(serializer.data)

class ListaPoaGeneralGestionX(APIView):
    def get_object(self, Gestion):
        try:
            return POAGeneral.objects.get(Gestion=Gestion)
        except POAGeneral.DoesNotExist:
            raise Http404

    def get(self, request, Gestion, format=None):
        snippet = self.get_object(Gestion)
        serializer = POAGeneralSerializer(snippet)
        return Response(serializer.data)

'''Crea un POA general'''
class GuardarPoaGeneral(APIView):
    def post(self, request, format=None):
        serializer = POAGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaFormulariosGeneral(APIView):
    def get(self, request,format=None):
        ListaFormulariosGenerales = FormulariosGenerales.objects.all()
        serializer = FormulariosGeneralesSerializer(ListaFormulariosGenerales, many=True)
        return Response(serializer.data)

class GuardarFormularios(APIView):
     def post(self, request, format=None):
        serializer = FormulariosGeneralesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
     

#Registro de RRHH  GENERA
class GuardarRecursosHumanosGeneral(APIView):
    def post(self, request, format=None):
        serializer = RecursosHumanosGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Registro de SNP  GENERAL
class GuardarServiciosNoPersonalesGeneral(APIView):
    def post(self, request, format=None):
        serializer = ServiciosNoPersonalesGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Registro de MS  GENERAL
class GuardarMaterialesYSuministrosGeneral(APIView):
    def post(self, request, format=None):
        serializer = MaterialesYSuministrosGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# #Registro de AF  GENERAL
class GuardaractivosFijosGeneralGeneral(APIView):
    def post(self, request, format=None):
        serializer = activosFijosGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#ANTES DE LA REFORMULACION
# '''lista a los formularios con una gestion y tipo dado'''
# class ListaFormulariosGeneralGestionTipo(APIView):
#     def get(self,request, gestion, tipo,format=None):
#         snippets = FormulariosGenerales.objects.filter(Gestion=gestion).filter(tipo=tipo)
#         serializer = FormulariosGeneralesDetallePOASerializer(snippets, many=True)
#         return Response(serializer.data)

#DESPUES DE LA REFORMULACION
'''lista a los formularios con una gestion y tipo dado'''
class ListaFormulariosGeneralGestionTipo(APIView):
    def get(self,request, gestion, tipo,idPOA,format=None):
        snippets = FormulariosGenerales.objects.filter(Gestion=gestion).filter(tipo=tipo).filter(POAGPertenece=idPOA)
        serializer = FormulariosGeneralesDetallePOASerializer(snippets, many=True)
        return Response(serializer.data)
        
'''saca el detalle de un formulario con un id dado '''
class ListaDetalleFormulariosGeneral(APIView):
    def get_object(self, pk):
        try:
            return FormulariosGenerales.objects.get(pk=pk)
        except FormulariosGenerales.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = formulariosRespoCargoSerializer(snippet)
        return Response(serializer.data)

'''lista los formularios con un id dado en el formulario de RRHH General'''
class DetalleRRHHGeneral(APIView):
    def get(self, request, id, format=None):
        snippets= RecursosHumanosGeneral.objects.filter(idFormularios=id)
        serializer = formulariosRRHHSerializer(snippets, many=True)
        return Response(serializer.data)

'''lista los formularios con un id dado en el formulario de SNP General'''
class DetalleSNPGeneral(APIView):
    def get(self, request, id, format=None):
        snippets= ServiciosNoPersonalesGeneral.objects.filter(idFormularios=id)
        serializer = formulariosSNPSerializer(snippets, many=True)
        return Response(serializer.data)

'''lista los formularios con un id dado en el formulario de MS General'''
class DetalleMSGeneral(APIView):
    def get(self, request, id, format=None):
        snippets= MaterialesYSuministrosGeneral.objects.filter(idFormularios=id).order_by('idDetalleMS__CodigoPrioridad','idDetalleMS_id')
        serializer = formulariosMSSerializer(snippets, many=True)
        return Response(serializer.data)

'''lista los formularios con un id dado en el formulario de AF General'''
class DetalleAFGeneral(APIView):
    def get(self, request, id, format=None):
        snippets= activosFijosGeneral.objects.filter(idFormularios=id).order_by('idDetalleAF__CodigoPrioridad','idDetalleAF_id')
        serializer = formulariosAFSerializer(snippets, many=True)
        return Response(serializer.data)

'''lista los formularios con un id dado en el formulario de OG General'''
class DetalleOGGeneral(APIView):
    def get(self, request, id, format=None):
        snippets= ObjetivosGestionGeneral.objects.filter(idFormularios=id)
        serializer = formulariosOGSerializer(snippets, many=True)
        return Response(serializer.data)
'''listar para mostrar por unidad los objetivos de gestion ya guardados'''

class DetalleOGGeneral_v1(APIView):
    def get(self, request, id,unidad,format=None):
        snippets= ObjetivosGestionGeneral.objects.filter(idFormularios=id).filter(idObjGestion__UnidadRes=unidad)
        serializer = formulariosOGSerializer(snippets, many=True)
        return Response(serializer.data)

''' GUARDAR OBJETIVOS DE GESTION'''
class GuardarObjetivosDeGestion(APIView):
    def post(self, request, format=None):
        serializer = ObjetivosGestionGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''lista los formularios con un id dado en el formulario de OF General'''
class DetalleOFeneral(APIView):
    def get(self, request, id, format=None):
        snippets= OperacionesFuncionamientoGeneral.objects.filter(idFormularios=id)
        serializer = formulariosOFSerializer(snippets, many=True)
        return Response(serializer.data)

#  def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).order_by('Detalle_CodigoPrioridad')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
#         serializer = activosFijosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

class DetalleOFeneral_v1(APIView):
    def get(self, request, id,unidad, format=None):
        snippets=OperacionesFuncionamientoGeneral.objects.filter(idFormularios=id).filter(idOperacion__UnidadRes=unidad)
        # snippets= OperacionesFuncionamientoGeneral.objects.all()
        serializer = formulariosOFSerializer(snippets, many=True)
        return Response(serializer.data)

''' GUARDAR OPERACIONES DE FUNCIONAMIENTO'''
class GuardarOperacionesFuncionamiento(APIView):
    def post(self, request, format=None):
        serializer = OperacionesFuncionamientoGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' GUARDAR MONTOS PROGRAMADOS'''
class GuardaMontosProgramados(APIView):
    def post(self, request, format=None):
        serializer = MontosProgramadosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''lista los montos programados de cada formulario = id'''
class ListarMontosProgramadoId(APIView):
    def get(self, request, id, format=None):
        snippets= MontosProgramados.objects.filter(idFormularios=id)
        serializer = MontosProgramadosSerializer(snippets, many=True)
        return Response(serializer.data)

'''Lista los POAS con un estado de aprobacion'''
class ListaPoaGeneralEstado(APIView):
    def get(self, request, format=None):        
        snippets= POAGeneral.objects.filter(Q(Estado="Proceso Aprobacion") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") | Q(Estado="Cumplido")).order_by('-Gestion')
        serializer = POAGeneralSerializer(snippets,many=True)
        return Response(serializer.data)
       
''' ACTUALIZAR POA de alguna gestion''' 
class actualizarPOAGeneral(APIView):
    def get_object(self, pk):
        try:
            return POAGeneral.objects.get(pk=pk)
        except POAGeneral.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = POAGeneralSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = POAGeneralSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class GuardarUnidadSolicitanteGeneralSerializer(APIView):
    def post(self, request, format=None):
        serializer = UnidadSolicitanteGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#UNIDADES SOLICITANTES YA GUARDADOS 
class UnidadesSolicitantes_MS_GENERAL(APIView):
    def get(self, request,idMS,idFMS,format=None):
        snippets= UnidadSolicitanteGeneral.objects.filter(idDetalleMS=idMS).filter(idMSG=idFMS).order_by('idUnidad__cod_unidad')
        serializer = UnidadesSolicitantesFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

class GuardarUnidadSolicitanteGeneral_AF(APIView):
    def post(self, request, format=None):
        serializer = UnidadSolicitanteGeneralAFSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#UNIDADES SOLICITANTES YA GUARDADOS 
class UnidadesSolicitantes_AF_GENERAL(APIView):
    def get(self, request,idMS,idFMS,format=None):
        snippets= UnidadSolicitanteGeneralAF.objects.filter(idDetalleMS=idMS).filter(idMSG=idFMS).order_by('idUnidad__cod_unidad')
        serializer = UnidadSolicitanteGeneralAFFormularioSerializer(snippets, many=True)
        return Response(serializer.data)
    
class GuardarObservacionPoaGeneralSerializer(APIView):
    def post(self, request, format=None):
        serializer = ObservacionPoaGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListarObservcionPOAid(APIView):
    def get(self, request, id, format=None):
        snippets= ObservacionPoaGeneral.objects.filter(idPOA=id)
        serializer = ObservacionPoaGeneralSerializer(snippets, many=True)
        return Response(serializer.data)

'''Lista los POAS con estado APROBADO SSU para unidad'''
class ListaPoaGeneralEstado_U(APIView):
    def get(self, request, format=None):
        snippets= POAGeneral.objects.filter(Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES")| Q(Estado="Cumplido")).order_by('VersionPOA','-Gestion')
        serializer = POAGeneralSerializer(snippets,many=True)
        return Response(serializer.data)

'''Lista los POAS con estado APROBADO SSU y activo el que esta vigente'''
class ListaPoaGeneralEstado_A(APIView):
    def get(self, request, format=None):        
        snippets= POAGeneral.objects.filter(Estado="Aprobado SSU").filter(Estado_version='Activo').order_by('-Gestion')
        serializer = POAGeneralSerializer(snippets,many=True)
        return Response(serializer.data)


'''Lista los POAS con estado APROBACION para unidad'''
class ListaPoaGeneralEstadoPA(APIView):
    def get(self, request, format=None):
        snippets= POAGeneral.objects.filter(Estado="Proceso Aprobacion")
        serializer = POAGeneralSerializer(snippets,many=True)
        return Response(serializer.data)