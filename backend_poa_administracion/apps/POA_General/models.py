from django.db import models
from ..responsablesInformacion.models import Personal
from ..responsablesInformacion.models import Unidad
from ..Formulacion.models import objetivosGestion
from ..Formulacion.models import operacionesFuncionamiento
from ..Formulacion.models import recursosHumanos
from ..Formulacion.models import serviciosNoPersonales
from ..Formulacion.models import materialesSumunistros
# from ..Formulacion.models import activosFijos
from ..Listas_para_Formulacion.models import Material_y_Suministro 
from ..Listas_para_Formulacion.models import Activo_y_Equipo
# Create your models here.

class POAGeneral(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
    ('Proceso Consolidacion', 'Proceso Consolidacion'),
    ('Proceso Revision', 'Proceso Revision'), 
    ('Proceso Aprobacion', 'Proceso Aprobacion'),
    ('Aprobado PROMES', 'Aprobado PROMES'),
    ('Aprobado SSU', 'Aprobado SSU'),
    ('Vigente', 'Vigente'),
    ('Observado', 'Observado'),
    ('Cumplido', 'Cumplido'),
    ('Cumplido Reformulado', 'Cumplido Reformulado'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso Formulacion')
    ESTADOV_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo'),
    )
    VersionPOA = models.CharField(max_length=50)#campos para la reformulacion
    Estado_version = models.CharField(max_length=50, choices=ESTADOV_CHOICES, default='Inactivo')#campos para la reformulacion
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de POAs General")

class FormulariosGenerales(models.Model):
    Gestion = models.IntegerField()
    Fecha=models.DateField()
    TIPO_CHOICES= (
    ('AccionesCortoPlazo', 'Acciones de Corto Plazo'),
    ('ObjetivosGestion', 'Objetivos de Gestion'),
    ('AccionesyOperaciones', 'Acciones y Operaciones'),
    ('recursosHumanos', 'Recursos Humanos'),
    ('serviciosNoPersonales', 'Servicios No Personales'),
    ('materialeSuministro', 'Materiales y Suministros'),
    ('activoFijo', 'Activos Fijos'),
    )
    tipo = models.CharField(max_length=50, choices=TIPO_CHOICES)
    ResponsableInformacion=models.ForeignKey(Personal,on_delete=models.CASCADE)
    POAGPertenece=models.ForeignKey(POAGeneral,on_delete=models.CASCADE)
    MontoTotalProgramado=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios En General")

#registro de objetivos de gestion
class ObjetivosGestionGeneral(models.Model):
    idObjGestion=models.ForeignKey(objetivosGestion,on_delete=models.CASCADE)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Objetivos de Gestión PROMES")

#registro de operaciones de funcionamiento
class OperacionesFuncionamientoGeneral(models.Model):
    idOperacion=models.ForeignKey(operacionesFuncionamiento,on_delete=models.CASCADE)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Operaciones de Funcionamiento PROMES")

#registro de RRHH en general
class RecursosHumanosGeneral(models.Model):
    idRRHH=models.ForeignKey(recursosHumanos,on_delete=models.CASCADE)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Recursos Humanos PROMES")
#registro de SNP en general
class ServiciosNoPersonalesGeneral(models.Model):
    idSNP=models.ForeignKey(serviciosNoPersonales,on_delete=models.CASCADE)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Servicios No Personal PROMES")
        
# Modelo GENERAL para Materiales y SUministros 
class MaterialesYSuministrosGeneral(models.Model):
    idDetalleMS= models.ForeignKey(Material_y_Suministro,on_delete=models.PROTECT)
    CExistente=models.IntegerField()
    CNecesario=models.IntegerField()
    MesSolicitud=models.CharField(max_length=50,blank=True,null=True)
    Cunitario=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoTotalP=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Materiales y Suministros  PROMES")

class UnidadSolicitanteGeneral(models.Model):
    idDetalleMS= models.ForeignKey(Material_y_Suministro,on_delete=models.PROTECT)
    idMSG= models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    CSolicitado=models.IntegerField()
    idUnidad= models.ForeignKey(Unidad,on_delete=models.PROTECT)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Unidad Solicitante General-- MS")
        
# Modelo GENERAL para Activos Fijos
class activosFijosGeneral(models.Model):
    idDetalleAF=models.ForeignKey(Activo_y_Equipo,on_delete=models.PROTECT)
    CExistente=models.IntegerField()
    CNecesario=models.IntegerField()
    MesSolicitud=models.CharField(max_length=50,blank=True,null=True)
    PartidaP=models.CharField(max_length=50,blank=True,null=True)
    PrecioR=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    CostoTotal=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    idcotizacion=models.CharField(max_length=200,blank=True,null=True)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Activos Fijos PROMES")

class UnidadSolicitanteGeneralAF(models.Model):
    idDetalleMS= models.ForeignKey(Activo_y_Equipo,on_delete=models.PROTECT)
    idMSG= models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    CSolicitado=models.IntegerField()
    idUnidad= models.ForeignKey(Unidad,on_delete=models.PROTECT)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Unidad Solicitante General - AF")

#modelo para guardar los montos de cada tipo de formulario
class MontosProgramados(models.Model):
    MontoRRHH=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoSNP=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoME=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontosP=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoMEL=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoML=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoLAB=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoOdo=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoMedI=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoAF=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoEM=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    idFormularios=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Monto Totales")

class ObservacionPoaGeneral(models.Model):
    Descripcion =  models.CharField(max_length=500)
    Fecha = models.DateField(auto_now=True)
    NombreR = models.CharField(max_length=100)
    ApellidosR = models.CharField(max_length=100)
    Cargo = models.CharField(max_length=100)
    idPOA=models.ForeignKey(POAGeneral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Observaciones del POA general")