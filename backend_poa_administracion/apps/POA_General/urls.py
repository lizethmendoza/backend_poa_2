from django.urls import path
from .views import ListaPOAGeneral
from .views import ListaPoaGeneralGestion
from .views import GuardarPoaGeneral
from .views import ListaPoaGeneralGestionX

from .views import ListaFormulariosGeneral
from .views import GuardarFormularios

from .views import ListaFormulariosGeneralGestionTipo
from .views import ListaDetalleFormulariosGeneral

from .views import GuardarRecursosHumanosGeneral
from .views import DetalleRRHHGeneral

from .views import GuardarServiciosNoPersonalesGeneral
from .views import DetalleSNPGeneral

from .views import GuardarMaterialesYSuministrosGeneral
from .views import DetalleMSGeneral

from .views import GuardaractivosFijosGeneralGeneral
from .views import DetalleAFGeneral

from .views import GuardarObjetivosDeGestion
from .views import DetalleOGGeneral

from .views import DetalleOFeneral
from .views import GuardarOperacionesFuncionamiento

from .views import GuardaMontosProgramados
from .views import ListarMontosProgramadoId
from .views import ListaPoaGeneralEstado
from .views import actualizarPOAGeneral
from .views import GuardarUnidadSolicitanteGeneralSerializer
from .views import UnidadesSolicitantes_MS_GENERAL
from .views import GuardarUnidadSolicitanteGeneral_AF
from .views import UnidadesSolicitantes_AF_GENERAL
from .views import DetalleOGGeneral_v1
from .views import DetalleOFeneral_v1
from .views import GuardarObservacionPoaGeneralSerializer
from .views import ListarObservcionPOAid
from .views import ListaPoaGeneralEstado_U
from .views import ListaPoaGeneralEstado_A
from .views import ListaPoaGeneralEstadoPA
urlpatterns = [
    path('ListaPOAGeneral/', ListaPOAGeneral.as_view(),name="ListaPOAGeneral"),
    path('ListaPoaGeneralGestion/<int:Gestion>/<int:version>/', ListaPoaGeneralGestion.as_view(),name="ListaPoaGeneralGestion"),
    path('GuardarPoaGeneral/', GuardarPoaGeneral.as_view(),name="GuardarPoaGeneral"),
    path('ListaPoaGeneralGestionX/<int:Gestion>/', ListaPoaGeneralGestionX.as_view(),name="ListaPoaGeneralGestionX"),
    path('ListaFormulariosGeneral/', ListaFormulariosGeneral.as_view(),name="ListaFormulariosGeneral"),
    path('GuardarFormularios/', GuardarFormularios.as_view(),name="GuardarFormularios"),
    path('GuardarObjetivosDeGestion/', GuardarObjetivosDeGestion.as_view(),name="GuardarObjetivosDeGestion"),
    path('DetalleOGGeneral/<int:id>/', DetalleOGGeneral.as_view(), name="DetalleOGGeneral"),
    path('GuardarOperacionesFuncionamiento/', GuardarOperacionesFuncionamiento.as_view(),name="GuardarOperacionesFuncionamiento"),
    path('DetalleOFeneral/<int:id>/', DetalleOFeneral.as_view(), name="DetalleOFeneral"),
    path('GuardarRecursosHumanosGeneral/', GuardarRecursosHumanosGeneral.as_view(),name="GuardarRecursosHumanosGeneral"),
    path('ListaFormulariosGeneralGestionTipo/<int:gestion>/<str:tipo>/<int:idPOA>/', ListaFormulariosGeneralGestionTipo.as_view(),name="ListaFormulariosGeneralGestionTipo"),
    path('ListaDetalleFormulariosGeneral/<int:pk>/', ListaDetalleFormulariosGeneral.as_view(), name="ListaDetalleFormulariosGeneral"),
    path('DetalleRRHHGeneral/<int:id>/', DetalleRRHHGeneral.as_view(), name="DetalleRRHHGeneral"),
    path('GuardarServiciosNoPersonalesGeneral/', GuardarServiciosNoPersonalesGeneral.as_view(),name="GuardarServiciosNoPersonalesGeneral"),
    path('DetalleSNPGeneral/<int:id>/', DetalleSNPGeneral.as_view(), name="DetalleSNPGeneral"),
    path('GuardarMaterialesYSuministrosGeneral/', GuardarMaterialesYSuministrosGeneral.as_view(),name="GuardarMaterialesYSuministrosGeneral"),
    path('DetalleMSGeneral/<int:id>/', DetalleMSGeneral.as_view(), name="DetalleMSGeneral"),
    path('GuardaractivosFijosGeneralGeneral/', GuardaractivosFijosGeneralGeneral.as_view(),name="GuardaractivosFijosGeneralGeneral"),
    path('DetalleAFGeneral/<int:id>/', DetalleAFGeneral.as_view(), name="DetalleAFGeneral"),

    path('GuardaMontosProgramados/', GuardaMontosProgramados.as_view(),name="GuardaMontosProgramados"),
    path('ListarMontosProgramadoId/<int:id>/', ListarMontosProgramadoId.as_view(), name="ListarMontosProgramadoId"),
    path('ListaPoaGeneralEstado/', ListaPoaGeneralEstado.as_view(),name="ListaPoaGeneralEstado"),
    path('actualizarPOAGeneral/<int:pk>/', actualizarPOAGeneral.as_view(), name="actualizarPOAGeneral"),
    path('GuardarUnidadSolicitanteGeneralSerializer/', GuardarUnidadSolicitanteGeneralSerializer.as_view(),name="GuardarUnidadSolicitanteGeneralSerializer"),
    path('UnidadesSolicitantes_MS_GENERAL/<int:idMS>/<int:idFMS>/', UnidadesSolicitantes_MS_GENERAL.as_view(), name="UnidadesSolicitantes_MS_GENERAL"),
    path('GuardarUnidadSolicitanteGeneral_AF/', GuardarUnidadSolicitanteGeneral_AF.as_view(),name="GuardarUnidadSolicitanteGeneral_AF"),
    path('UnidadesSolicitantes_AF_GENERAL/<int:idMS>/<int:idFMS>/', UnidadesSolicitantes_AF_GENERAL.as_view(), name="UnidadesSolicitantes_AF_GENERAL"),
    path('DetalleOGGeneral_v1/<int:id>/<str:unidad>', DetalleOGGeneral_v1.as_view(), name="DetalleOGGeneral_v1"),
    path('DetalleOFeneral_v1/<int:id>/<str:unidad>/', DetalleOFeneral_v1.as_view(), name="DetalleOFeneral_v1"),
    path('GuardarObservacionPoaGeneralSerializer/', GuardarObservacionPoaGeneralSerializer.as_view(), name="GuardarObservacionPoaGeneralSerializer"),
    path('ListarObservcionPOAid/<int:id>/', ListarObservcionPOAid.as_view(), name="ListarObservcionPOAid"),
    path('ListaPoaGeneralEstado_U/', ListaPoaGeneralEstado_U.as_view(), name="ListaPoaGeneralEstado_U"),
    path('ListaPoaGeneralEstado_A/', ListaPoaGeneralEstado_A.as_view(), name="ListaPoaGeneralEstado_A"),
    path('ListaPoaGeneralEstadoPA/', ListaPoaGeneralEstadoPA.as_view(), name="ListaPoaGeneralEstadoPA"),
]