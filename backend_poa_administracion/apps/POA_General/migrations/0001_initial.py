# Generated by Django 2.2.3 on 2019-07-24 04:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Formulacion', '0001_initial'),
        ('responsablesInformacion', '0001_initial'),
        ('Listas_para_Formulacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormulariosGenerales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Gestion', models.IntegerField()),
                ('Fecha', models.DateField()),
                ('tipo', models.CharField(choices=[('AccionesCortoPlazo', 'Acciones de Corto Plazo'), ('ObjetivosGestion', 'Objetivos de Gestion'), ('AccionesyOperaciones', 'Acciones y Operaciones'), ('recursosHumanos', 'Recursos Humanos'), ('serviciosNoPersonales', 'Servicios No Personales'), ('materialeSuministro', 'Materiales y Suministros'), ('activoFijo', 'Activos Fijos')], max_length=50)),
                ('MontoTotalProgramado', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
            ],
            options={
                'verbose_name_plural': 'Formularios En General',
            },
        ),
        migrations.CreateModel(
            name='POAGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Gestion', models.IntegerField()),
                ('FechaElaboracion', models.DateField(auto_now=True)),
                ('Estado', models.CharField(choices=[('Proceso Consolidacion', 'Proceso Consolidacion'), ('Proceso Revision', 'Proceso Revision'), ('Proceso Aprobacion', 'Proceso Aprobacion'), ('Aprobado PROMES', 'Aprobado PROMES'), ('Aprobado SSU', 'Aprobado SSU'), ('Vigente', 'Vigente'), ('Observado', 'Observado'), ('Cumplido', 'Cumplido'), ('Cumplido Reformulado', 'Cumplido Reformulado')], default='Proceso Formulacion', max_length=50)),
                ('VersionPOA', models.CharField(max_length=50)),
                ('Estado_version', models.CharField(choices=[('Activo', 'Activo'), ('Inactivo', 'Inactivo')], default='Inactivo', max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Lista de POAs General',
            },
        ),
        migrations.CreateModel(
            name='UnidadSolicitanteGeneralAF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CSolicitado', models.IntegerField()),
                ('idDetalleMS', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Listas_para_Formulacion.Activo_y_Equipo')),
                ('idMSG', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idUnidad', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='responsablesInformacion.Unidad')),
            ],
            options={
                'verbose_name_plural': 'Unidad Solicitante General - AF',
            },
        ),
        migrations.CreateModel(
            name='UnidadSolicitanteGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CSolicitado', models.IntegerField()),
                ('idDetalleMS', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Listas_para_Formulacion.Material_y_Suministro')),
                ('idMSG', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idUnidad', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='responsablesInformacion.Unidad')),
            ],
            options={
                'verbose_name_plural': 'Unidad Solicitante General-- MS',
            },
        ),
        migrations.CreateModel(
            name='ServiciosNoPersonalesGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idSNP', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Formulacion.serviciosNoPersonales')),
            ],
            options={
                'verbose_name_plural': 'Servicios No Personal PROMES',
            },
        ),
        migrations.CreateModel(
            name='RecursosHumanosGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idRRHH', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Formulacion.recursosHumanos')),
            ],
            options={
                'verbose_name_plural': 'Recursos Humanos PROMES',
            },
        ),
        migrations.CreateModel(
            name='OperacionesFuncionamientoGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idOperacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Formulacion.operacionesFuncionamiento')),
            ],
            options={
                'verbose_name_plural': 'Operaciones de Funcionamiento PROMES',
            },
        ),
        migrations.CreateModel(
            name='ObservacionPoaGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Descripcion', models.CharField(max_length=500)),
                ('Fecha', models.DateField(auto_now=True)),
                ('NombreR', models.CharField(max_length=100)),
                ('ApellidosR', models.CharField(max_length=100)),
                ('Cargo', models.CharField(max_length=100)),
                ('idPOA', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.POAGeneral')),
            ],
            options={
                'verbose_name_plural': 'Observaciones del POA general',
            },
        ),
        migrations.CreateModel(
            name='ObjetivosGestionGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
                ('idObjGestion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Formulacion.objetivosGestion')),
            ],
            options={
                'verbose_name_plural': 'Objetivos de Gestión PROMES',
            },
        ),
        migrations.CreateModel(
            name='MontosProgramados',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('MontoRRHH', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoSNP', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoME', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontosP', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoMEL', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoML', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoLAB', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoOdo', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoMedI', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoAF', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoEM', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
            ],
            options={
                'verbose_name_plural': 'Monto Totales',
            },
        ),
        migrations.CreateModel(
            name='MaterialesYSuministrosGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CExistente', models.IntegerField()),
                ('CNecesario', models.IntegerField()),
                ('MesSolicitud', models.CharField(blank=True, max_length=50, null=True)),
                ('Cunitario', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('MontoTotalP', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('idDetalleMS', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Listas_para_Formulacion.Material_y_Suministro')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
            ],
            options={
                'verbose_name_plural': 'Materiales y Suministros  PROMES',
            },
        ),
        migrations.AddField(
            model_name='formulariosgenerales',
            name='POAGPertenece',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.POAGeneral'),
        ),
        migrations.AddField(
            model_name='formulariosgenerales',
            name='ResponsableInformacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='responsablesInformacion.Personal'),
        ),
        migrations.CreateModel(
            name='activosFijosGeneral',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('CExistente', models.IntegerField()),
                ('CNecesario', models.IntegerField()),
                ('MesSolicitud', models.CharField(blank=True, max_length=50, null=True)),
                ('PartidaP', models.CharField(blank=True, max_length=50, null=True)),
                ('PrecioR', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('CostoTotal', models.DecimalField(blank=True, decimal_places=2, max_digits=19, null=True)),
                ('idcotizacion', models.CharField(blank=True, max_length=200, null=True)),
                ('idDetalleAF', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Listas_para_Formulacion.Activo_y_Equipo')),
                ('idFormularios', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='POA_General.FormulariosGenerales')),
            ],
            options={
                'verbose_name_plural': 'Activos Fijos PROMES',
            },
        ),
    ]
