from rest_framework import serializers
from .models import POAGeneral
from ..responsablesInformacion.serializers import personal_Cargo_UnidadSerializer
from ..responsablesInformacion.serializers import CargoSerializer
from ..responsablesInformacion.serializers import UnidadSerializer

from .models import FormulariosGenerales
from .models import ObjetivosGestionGeneral
from .models import RecursosHumanosGeneral
from .models import ServiciosNoPersonalesGeneral
from .models import MaterialesYSuministrosGeneral
from .models import activosFijosGeneral
from .models import MontosProgramados
from .models import  OperacionesFuncionamientoGeneral

from ..Formulacion.serializers import objetivosGestionFormularioSerializer
from ..Formulacion.serializers import operacionesFuncionamientoSerializer_1
from ..Formulacion.serializers import recursosHumanosFormularioSerializer
from ..Formulacion.serializers import serviciosNoPersonalesFormularioSerializer
from ..Formulacion.serializers import materialesSumunistrosFormularioSerializer
from ..Formulacion.serializers import activosFijosFormularioSerializer
from ..Listas_para_Formulacion.serializers import Material_y_SuministroSerializer
from ..Listas_para_Formulacion.serializers import Activo_y_EquipoSerializer

from .models import UnidadSolicitanteGeneral
from .models import UnidadSolicitanteGeneralAF
from .models import ObservacionPoaGeneral

class POAGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = POAGeneral
        fields = '__all__'

#para sacar el estado del POA General
class FormulariosGeneralesDetallePOASerializer(serializers.ModelSerializer):
    POAGPertenece=POAGeneralSerializer(read_only=True)
    class Meta:
        model = FormulariosGenerales
        fields = '__all__'

class FormulariosGeneralesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormulariosGenerales
        fields = '__all__'

#Serializers para mostrar el POA general 
class RecursosHumanosGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecursosHumanosGeneral
        fields = '__all__'

class ServiciosNoPersonalesGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiciosNoPersonalesGeneral
        fields = '__all__'

class MaterialesYSuministrosGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaterialesYSuministrosGeneral
        fields = '__all__'

class activosFijosGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = activosFijosGeneral
        fields = '__all__'

#Serializers para mostrar el POA general
class formulariosRespoCargoSerializer(serializers.ModelSerializer):
    ResponsableInformacion = personal_Cargo_UnidadSerializer(read_only=True)
    cargo=CargoSerializer(read_only=True)
    class Meta:
        model = FormulariosGenerales
        fields = '__all__'

class formulariosRRHHSerializer(serializers.ModelSerializer):
    idRRHH = recursosHumanosFormularioSerializer(read_only=True)
    class Meta:
        model = RecursosHumanosGeneral
        fields = '__all__'

class formulariosSNPSerializer(serializers.ModelSerializer):
    idSNP = serviciosNoPersonalesFormularioSerializer(read_only=True)
    class Meta:
        model = ServiciosNoPersonalesGeneral
        fields = '__all__'

class formulariosMSSerializer(serializers.ModelSerializer):
    idMS = materialesSumunistrosFormularioSerializer(read_only=True)
    idDetalleMS=Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = MaterialesYSuministrosGeneral
        fields = '__all__'

class formulariosAFSerializer(serializers.ModelSerializer):
    idAF= activosFijosFormularioSerializer(read_only=True)
    idDetalleAF=Activo_y_EquipoSerializer(read_only=True)
    class Meta:
        model = activosFijosGeneral
        fields = '__all__'

class MontosProgramadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = MontosProgramados
        fields = '__all__'

class ObjetivosGestionGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjetivosGestionGeneral
        fields = '__all__'

class formulariosOGSerializer(serializers.ModelSerializer):
    idObjGestion = objetivosGestionFormularioSerializer(read_only=True)
    class Meta:
        model = ObjetivosGestionGeneral
        fields = '__all__'

class OperacionesFuncionamientoGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperacionesFuncionamientoGeneral
        fields = '__all__'

class formulariosOFSerializer(serializers.ModelSerializer):
    idOperacion = operacionesFuncionamientoSerializer_1(read_only=True)
    class Meta:
        model = OperacionesFuncionamientoGeneral
        fields = '__all__'  

class UnidadSolicitanteGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnidadSolicitanteGeneral
        fields = '__all__'

class UnidadesSolicitantesFormularioSerializer(serializers.ModelSerializer):
    idUnidad = UnidadSerializer(read_only=True)
    class Meta:
        model = UnidadSolicitanteGeneral
        fields = '__all__'

class UnidadSolicitanteGeneralAFSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnidadSolicitanteGeneralAF
        fields = '__all__'

class UnidadSolicitanteGeneralAFFormularioSerializer(serializers.ModelSerializer):
    idUnidad = UnidadSerializer(read_only=True)
    class Meta:
        model = UnidadSolicitanteGeneralAF
        fields = '__all__'

class ObservacionPoaGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObservacionPoaGeneral
        fields = '__all__'