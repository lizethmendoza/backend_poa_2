from django.contrib import admin
from .models import POAGeneral
from .models import FormulariosGenerales
from .models import ObjetivosGestionGeneral
from .models import OperacionesFuncionamientoGeneral
from .models import RecursosHumanosGeneral
from .models import ServiciosNoPersonalesGeneral
from .models import MaterialesYSuministrosGeneral
from .models import activosFijosGeneral
from .models import MontosProgramados
from .models import UnidadSolicitanteGeneral
from .models import UnidadSolicitanteGeneralAF
from .models import ObservacionPoaGeneral

# Register your models here.
@admin.register(POAGeneral)
class POAGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","Gestion", "FechaElaboracion", "Estado","VersionPOA","Estado_version"]

@admin.register(FormulariosGenerales)
class FormulariosGeneralesAdmin(admin.ModelAdmin):
    list_display = ["pk","Gestion", "Fecha", "tipo","ResponsableInformacion","POAGPertenece","MontoTotalProgramado"]

@admin.register(ObjetivosGestionGeneral)
class ObjetivosGestionGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idObjGestion", "idFormularios"]

@admin.register(OperacionesFuncionamientoGeneral)
class OperacionesFuncionamientoGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idOperacion", "idFormularios"]

@admin.register(RecursosHumanosGeneral)
class RecursosHumanosGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idRRHH","idFormularios"]

@admin.register(ServiciosNoPersonalesGeneral)
class ServiciosNoPersonalesGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idSNP","idFormularios"]

@admin.register(MaterialesYSuministrosGeneral)
class MaterialesYSuministrosGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idDetalleMS","CExistente","CNecesario","MesSolicitud","Cunitario","MontoTotalP","idFormularios"]

@admin.register(activosFijosGeneral)
class activosFijosGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idDetalleAF","CExistente","CNecesario","MesSolicitud","PartidaP","PrecioR","CostoTotal","idFormularios","idcotizacion"]

@admin.register(MontosProgramados)
class MontosProgramadosAdmin(admin.ModelAdmin):
    list_display = ["pk","MontoRRHH","MontoSNP","MontoME","MontosP","MontoMEL","MontoML","MontoLAB","MontoOdo","MontoMedI","MontoAF","MontoEM","idFormularios"]

@admin.register(UnidadSolicitanteGeneral)
class UnidadSolicitanteGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","idDetalleMS","idMSG","CSolicitado","idUnidad"]

@admin.register(UnidadSolicitanteGeneralAF)
class UnidadSolicitanteGeneralAFAdmin(admin.ModelAdmin):
    list_display = ["pk","idDetalleMS","idMSG","CSolicitado","idUnidad"]

@admin.register(ObservacionPoaGeneral)
class ObservacionPoaGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion","Fecha","NombreR","ApellidosR","Cargo","idPOA"]
