from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import formularios
from .models import recursosHumanos
from .models import materialesSumunistros
from .models import activosFijos
from .models import serviciosNoPersonales
from .models import objetivosGestion
from .models import operacionesFuncionamiento
from .models import mediosVerificacion
from .models import CotizacionFile

@admin.register(formularios)
class formulariosAdmin(admin.ModelAdmin):
    list_display = ["pk","Gestion", "Fecha","Estado","tipo", "ResponsableInformacion","POAPertenece"]

@admin.register(objetivosGestion)
class objetivosGestionAdmin(admin.ModelAdmin):
    list_display = ["pk","ACP", "AE","OP","Codigo","ObjetivoG","LineaBase", "LineaGestion","IndicadorEficiencia","indicadorEficacia","UnidadRes","TurnoUnidad","Observaciones","CodigoOrd","idFormularios"]

@admin.register(operacionesFuncionamiento)
class operacionesFuncionamientoAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion", "UnidadRes","FechaInicio","FechaFin","CodigoOrd","Formularios"]

@admin.register(mediosVerificacion)
class mediosVerificacionAdmin(admin.ModelAdmin):
    list_display = ["pk","descripcionMV","idOperacion","ObjetivoPertence"]

@admin.register(recursosHumanos)
class recursosHumanosAdmin(admin.ModelAdmin):
    list_display = ["pk","Denominacion","CargaHoraria", "Profesional","NEPermanenteExistente","NEPermanenteRequerido","NENOPermanenteExistente", "NENOPermanenteRequerido","EscalaSalarial","TiempoDe","TiempoHasta","TiempoMeses","MontoMensual","MontoAnual","Observaciones","Codigo","Formularios"]

@admin.register(serviciosNoPersonales)
class serviciosNoPersonalesAdmin(admin.ModelAdmin):
    list_display = ["pk","Modalidad", "TipoContratacion","DenominacionServicio","MesProgramado","AñoProgramado","NumeroMeses", "PrecioMensual","PFPrecioReferencial","FFCodigo","FFDetalle","Codigo","Formularios"]

@admin.register(materialesSumunistros)
class materialesSumunistrosAdmin(admin.ModelAdmin):
    list_display = ["pk","Detalle","Unidad","CantidadExistente","CantidadNecesario","CREnero","CRFebrero", "CRMarzo","CRAbril","CRMayo","CRJunio","CRJulio","CRAgosto","CRSeptiembre","CROctubre","CRNoviembre","CRDiciembre","CostoUnitario","MontoTotal","Codigo","Formularios"]

@admin.register(activosFijos)
class activosFijosAdmin(admin.ModelAdmin):
    list_display = ["pk","Detalle","CantidadExistente","CantidadNecesario","CREnero","CRFebrero", "CRMarzo","CRAbril","CRMayo","CRJunio","CRJulio","CRAgosto","CRSeptiembre","CROctubre","CRNoviembre","CRDiciembre","PartidaPresupuestaria","PrecioReferencial","CostoTotal","Codigo","Formularios","Cotizacion"]

@admin.register(CotizacionFile)
class CotizacionFileAdmin(admin.ModelAdmin):
    list_display = ["pk","file"]
