from django.urls import path

from .views import ListaFormularios
from .views import ListaDetalleFormularios
from .views import ListaFormulariosGestion
from .views import GuardarFormulario
from .views import ListaFormulariosGestionIdPOA
from .views import ListaFormulariosGestionTipo

from .views import GuardarRecursosHumanos
from .views import RecursosDetalleID
from .views import actualizarRecursoHumano
from .views import ListaFormulariosPOAi

from .views import GuardarServiciosNoPersonales
from .views import ServiciosDetalleID
from .views import actualizarServiciosNoPersonales
from .views import ListaFormulariosPOAi1

from .views import GuardarMaterialesSuministros
from .views import MaterialesSuministrosDetalleID
from .views import actualizarMaterialesySuministros
from .views import ListaFormulariosPOAi2
from .views import MaterialesSuministrosDetalle

from .views import GuardarActivosFijos
from .views import ActivosFijosDetalleID
from .views import actualizarActivosFijos
from .views import ListaFormulariosPOAi3,ListaFormulariosPOAi3Total
from .views import ActivosFijosDetalle

from .views import SumaCantidadNecesariaMS,PromedioPrecioUnitario
from .views import ListaFormulariosPOAi2TOTAL,SumaMontoTotalMS
from .views import SumaTotalMontoTotal
from .views import SumaTotalTipo

from .views import SumaTotalMontoTotalRRHH
from .views import SumaTotalMontoTotalSNP

from .views import SumaCantidadNecesariaAF
from .views import PromedioPrecioUnitarioAF
from .views import SumaMontoTotalAF
from .views import SumaTotalTipoAF
from .views import SumaTotalMontoTotalAF

from .views import SumaPromedioSumatotalMS
from .views import UnidadesSolicitantes_MS

from .views import SumaPromedioSumatotalAF
from .views import UnidadesSolicitantes_AF

from .views import GuardarObjetivosGestion
from .views import ObjetivosDetalleID
from .views import actualizarObjetivoGestion
from .views import ObjetivosGestionUnidadGestion

from .views import GuardaroperacionesFuncionamiento
from .views import OperacionesDetalleID
from .views import actualizaroperacionesFuncionamienton
from .views import ListaFormulariosPOAi_OG
from .views import ListaFormulariosPOAi_OG_v1

from .views import GuardarmediosVerificacion
from .views import ListaMediosVerificacion
from .views import MedioVerificacionDetalleID
from .views import actualizarmediosVerificacion
from .views import MedioVerificacionOperacion
from .views import ListaFormulariosPOAi_OF
from .views import ListaFormulariosPOAi_OF_v1
from .views import ListaFormulariosPOAi_v1
from .views import ListaFormulariosPOAi1_v1

from .views import FileUploadView,ListaArchivos,ListaArchivos_id,actualizarArchivoCotizacion
from .views import Cotizaciones_AF
from .views import actualizarFormularios

from .views import ListaFormulariosPOAi_OG_v2
from .views import ListaFormulariosPOAi_OF_v2
from .views import ListaFormulariosPOAi_v2
from .views import ListaFormulariosPOAi1_v2
from .views import ListaFormulariosPOAi2_v2
from .views import ListaFormulariosPOAi3_v2

urlpatterns = [
    path('ListaFormularios/', ListaFormularios.as_view(),name="ListaFormularios"),
    path('ListaDetalleFormularios/<int:pk>/', ListaDetalleFormularios.as_view(), name="ListaDetalleFormularios"),
    path('ListaFormulariosGestion/<int:gestion>/<int:idP>/<str:type>/', ListaFormulariosGestion.as_view(), name="ListaFormulariosGestion"),
    path('RegistrarForm/', GuardarFormulario.as_view(),name="GuardarFormulario"),
    path('ListaFormulariosGestionIdPOA/<int:gestion>/<int:idP>/', ListaFormulariosGestionIdPOA.as_view(), name="ListaFormulariosGestionIdPOA"),
    path('ListaFormulariosGestionTipo/<int:gestion>/<str:type>/', ListaFormulariosGestionTipo.as_view(), name="ListaFormulariosGestionTipo"),

    path('RegistrarRecursosHumanos/', GuardarRecursosHumanos.as_view(),name="GuardarRecursosHumanos"),
    path('RecursosDetalleID/<int:id>/', RecursosDetalleID.as_view(),name="RecursosDetalleID"),
    path('actualizarRecursoHumano/<int:pk>/', actualizarRecursoHumano.as_view(), name="actualizarRecursoHumano"),
    path('ListaFormulariosPOAi/<int:gestion>/', ListaFormulariosPOAi.as_view(), name="ListaFormulariosPOAi"),

    path('RegistrarServiciosNoPersonales/', GuardarServiciosNoPersonales.as_view(),name="GuardarServiciosNoPersonales"),
    path('ServiciosDetalleID/<int:id>/', ServiciosDetalleID.as_view(),name="ServiciosDetalleID"),
    path('actualizarServiciosNoPersonales/<int:pk>/', actualizarServiciosNoPersonales.as_view(), name="actualizarServiciosNoPersonales"),
    path('ListaFormulariosPOAi1/<int:gestion>/', ListaFormulariosPOAi1.as_view(), name="ListaFormulariosPOAi1"),
    
    path('RegistrarMaterialesSuministros/', GuardarMaterialesSuministros.as_view(),name="GuardarMaterialesSuministros"),
    path('MaterialesSuministrosDetalleID/<int:id>/', MaterialesSuministrosDetalleID.as_view(),name="MaterialesSuministrosDetalleID"),
    path('MaterialesSuministrosDetalle/<int:id>/', MaterialesSuministrosDetalle.as_view(),name="MaterialesSuministrosDetalle"),
    path('actualizarMaterialesySuministros/<int:pk>/', actualizarMaterialesySuministros.as_view(), name="actualizarMaterialesySuministros"),
    path('ListaFormulariosPOAi2/<int:gestion>/', ListaFormulariosPOAi2.as_view(), name="ListaFormulariosPOAi2"),

    path('RegistrarActivosFijos/', GuardarActivosFijos.as_view(),name="GuardarActivosFijos"),
    path('ActivosFijosDetalleID/<int:id>/', ActivosFijosDetalleID.as_view(),name="ActivosFijosDetalleID"),
    path('ActivosFijosDetalle/<int:id>/', ActivosFijosDetalle.as_view(),name="ActivosFijosDetalle"),
    path('ListaFormulariosPOAi3Total/<int:gestion>/', ListaFormulariosPOAi3Total.as_view(), name="ListaFormulariosPOAi3Total"),
    path('actualizarActivosFijos/<int:pk>/', actualizarActivosFijos.as_view(), name="actualizarActivosFijos"),
    path('ListaFormulariosPOAi3/<int:gestion>/', ListaFormulariosPOAi3.as_view(), name="ListaFormulariosPOAi3"),
    
    # para hacer la prueba de sum y count 
    # path('prueba/<int:id>/', prueba.as_view(),name="prueba"),
    path('ListaFormulariosPOAi2TOTAL/<int:gestion>/', ListaFormulariosPOAi2TOTAL.as_view(), name="ListaFormulariosPOAi2TOTAL"),
    path('SumaCantidadNecesariaMS/<int:gestion>/<int:fk>/', SumaCantidadNecesariaMS.as_view(), name="SumaCantidadNecesariaMS"),
    path('PromedioPrecioUnitario/<int:gestion>/<int:fk>/', PromedioPrecioUnitario.as_view(), name="PromedioPrecioUnitario"),
    path('SumaMontoTotalMS/<int:gestion>/<int:fk>/', SumaMontoTotalMS.as_view(), name="SumaMontoTotalMS"),
    path('SumaTotalMontoTotal/<int:gestion>/', SumaTotalMontoTotal.as_view(), name="SumaTotalMontoTotal"),
    path('SumaTotalTipo/<int:gestion>/<str:tipo>/', SumaTotalTipo.as_view(), name="ListaFormulariosGestionTipo"),
    path('SumaTotalMontoTotalRRHH/<int:gestion>/', SumaTotalMontoTotalRRHH.as_view(), name="SumaTotalMontoTotalRRHH"),
    path('SumaTotalMontoTotalSNP/<int:gestion>/', SumaTotalMontoTotalSNP.as_view(), name="SumaTotalMontoTotalSNP"),

    path('SumaCantidadNecesariaAF/<int:gestion>/<int:fk>/', SumaCantidadNecesariaAF.as_view(), name="SumaCantidadNecesariaAF"),
    path('PromedioPrecioUnitarioAF/<int:gestion>/<int:fk>/', PromedioPrecioUnitarioAF.as_view(), name="PromedioPrecioUnitarioAF"),
    path('SumaMontoTotalAF/<int:gestion>/<int:fk>/', SumaMontoTotalAF.as_view(), name="SumaMontoTotalAF"),
    path('SumaTotalTipoAF/<int:gestion>/<str:tipo>/', SumaTotalTipoAF.as_view(), name="ListaFormulariosGestionTipoAF"),
    path('SumaTotalMontoTotalAF/<int:gestion>/', SumaTotalMontoTotalAF.as_view(), name="SumaTotalMontoTotalAF"),

    path('SumaPromedioSumatotalMS/<int:gestion>/', SumaPromedioSumatotalMS.as_view(), name="SumaPromedioSumatotalMS"),
    path('UnidadesSolicitantes_MS/<int:gestion>/<int:fk>/', UnidadesSolicitantes_MS.as_view(), name="UnidadesSolicitantes_MS"),

    path('SumaPromedioSumatotalAF/<int:gestion>/', SumaPromedioSumatotalAF.as_view(), name="SumaPromedioSumatotalAF"),
    path('UnidadesSolicitantes_AF/<int:gestion>/<int:fk>/', UnidadesSolicitantes_AF.as_view(), name="UnidadesSolicitantes_AF"),

    path('GuardarObjetivosGestion/', GuardarObjetivosGestion.as_view(),name="GuardarObjetivosGestion"),
    path('ObjetivosDetalleID/<int:id>/', ObjetivosDetalleID.as_view(),name="ObjetivosDetalleID"),
    path('actualizarObjetivoGestion/<int:pk>/', actualizarObjetivoGestion.as_view(), name="actualizarObjetivoGestion"),
    path('ObjetivosGestionUnidadGestion/<int:gestion>/<str:Nunidad>/<int:idPOA>/', ObjetivosGestionUnidadGestion.as_view(), name="ObjetivosGestionUnidadGestion"),
    
    path('GuardaroperacionesFuncionamiento/', GuardaroperacionesFuncionamiento.as_view(),name="GuardaroperacionesFuncionamiento"),
    path('OperacionesDetalleID/<int:id>/', OperacionesDetalleID.as_view(),name="OperacionesDetalleID"),
    path('actualizaroperacionesFuncionamienton/<int:pk>/', actualizaroperacionesFuncionamienton.as_view(), name="actualizaroperacionesFuncionamienton"),

    path('GuardarmediosVerificacion/', GuardarmediosVerificacion.as_view(),name="GuardarmediosVerificacion"),
    path('ListaMediosVerificacion/', ListaMediosVerificacion.as_view(),name="ListaMediosVerificacion"),
    path('MedioVerificacionDetalleID/<int:idO>/', MedioVerificacionDetalleID.as_view(),name="MedioVerificacionDetalleID"),
    path('actualizarmediosVerificacion/<int:pk>/', actualizarmediosVerificacion.as_view(), name="actualizarmediosVerificacion"),
    path('ListaFormulariosPOAi_OG/<int:gestion>/', ListaFormulariosPOAi_OG.as_view(), name="ListaFormulariosPOAi_OG"),
    path('ListaFormulariosPOAi_OG_v1/<int:gestion>/<str:unidad>/', ListaFormulariosPOAi_OG_v1.as_view(), name="ListaFormulariosPOAi_OG_v1"),

    path('ListaFormulariosPOAi_OF/<int:gestion>/', ListaFormulariosPOAi_OF.as_view(), name="ListaFormulariosPOAi_OF"),
    path('ListaFormulariosPOAi_OF_v1/<int:gestion>/<str:unidad>/', ListaFormulariosPOAi_OF_v1.as_view(), name="ListaFormulariosPOAi_OF_v1"),

    path('MedioVerificacionOperacion/<int:idO>/', MedioVerificacionOperacion.as_view(),name="MedioVerificacionOperacion"),
    
    #PARA SUBIR ARCHIVOS
    path('FileUploadView/', FileUploadView.as_view(),name="FileUploadView"),
    path('ListaArchivos/', ListaArchivos.as_view(),name="ListaArchivos"),
    path('ListaArchivos_id/<int:id>/', ListaArchivos_id.as_view(),name="ListaArchivos_id"),
    path('actualizarArchivoCotizacion/<int:pk>/', actualizarArchivoCotizacion.as_view(), name="actualizarArchivoCotizacion"),
    path('Cotizaciones_AF/<int:gestion>/<int:fk>/', Cotizaciones_AF.as_view(), name="Cotizaciones_AF"),
    path('ListaFormulariosPOAi_v1/<int:gestion>/<str:unidad>/', ListaFormulariosPOAi_v1.as_view(), name="ListaFormulariosPOAi_v1"),
    path('ListaFormulariosPOAi1_v1/<int:gestion>/<str:unidad>/', ListaFormulariosPOAi1_v1.as_view(), name="ListaFormulariosPOAi1_v1"),
    path('actualizarFormularios/<int:pk>/', actualizarFormularios.as_view(), name="actualizarFormularios"),

    #REFORMULACION
    
    path('ListaFormulariosPOAi_OG_v2/<int:idPOA>/', ListaFormulariosPOAi_OG_v2.as_view(),name="ListaFormulariosPOAi_OG_v2"),
    path('ListaFormulariosPOAi_OF_v2/<int:idPOA>/', ListaFormulariosPOAi_OF_v2.as_view(),name="ListaFormulariosPOAi_OF_v2"),
    path('ListaFormulariosPOAi_v2/<int:idPOA>/', ListaFormulariosPOAi_v2.as_view(),name="ListaFormulariosPOAi_v2"),
    path('ListaFormulariosPOAi1_v2/<int:idPOA>/', ListaFormulariosPOAi1_v2.as_view(),name="ListaFormulariosPOAi1_v2"),
    path('ListaFormulariosPOAi2_v2/<int:idPOA>/', ListaFormulariosPOAi2_v2.as_view(),name="ListaFormulariosPOAi2_v2"),
    path('ListaFormulariosPOAi3_v2/<int:idPOA>/', ListaFormulariosPOAi3_v2.as_view(),name="ListaFormulariosPOAi3_v2"),
    
    
]
