from rest_framework import serializers

from .models import formularios
from .models import recursosHumanos
from .models import serviciosNoPersonales
from .models import materialesSumunistros
from .models import activosFijos
from .models import objetivosGestion
from .models import operacionesFuncionamiento
from .models import mediosVerificacion

from ..responsablesInformacion.serializers import personal_Cargo_UnidadSerializer
from ..responsablesInformacion.serializers import CargoSerializer

from ..Poai.serializers import POAiUnidadSerializer
from ..Listas_para_Formulacion.serializers import Material_y_SuministroSerializer
from ..Listas_para_Formulacion.serializers import Activo_y_EquipoSerializer
from .models import CotizacionFile

class formulariosRespoCargoSerializer(serializers.ModelSerializer):
    ResponsableInformacion = personal_Cargo_UnidadSerializer(read_only=True)
    cargo=CargoSerializer(read_only=True)
    class Meta:
        model = formularios
        fields = '__all__'

class formulariosRespoCargoSerializer_1(serializers.ModelSerializer):
    ResponsableInformacion = personal_Cargo_UnidadSerializer(read_only=True)
    class Meta:
        model = formularios
        fields = '__all__'

class formulariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = formularios
        fields = '__all__'

class formulariosPOAISerializer(serializers.ModelSerializer):
    POAPertenece = POAiUnidadSerializer(read_only=True)
    class Meta:
        model = formularios
        fields = '__all__'

class recursosHumanosSerializer(serializers.ModelSerializer):
    class Meta:
        model = recursosHumanos
        fields = '__all__'

class serviciosNoPersonalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = serviciosNoPersonales
        fields = '__all__'

class materialesSumunistrosSerializer(serializers.ModelSerializer):
    class Meta:
        model = materialesSumunistros
        fields = '__all__'

class materialesSumunistrosSerializer_D(serializers.ModelSerializer):
    Detalle = Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = materialesSumunistros
        fields = '__all__'

class activosFijosSerializer(serializers.ModelSerializer):
    class Meta:
        model = activosFijos
        fields = '__all__'

# SUBIR ARCHIVO DE COTIZACION
class CotizacionFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CotizacionFile
        fields = "__all__"

class activosFijosSerializer_D(serializers.ModelSerializer):
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    Cotizacion = CotizacionFileSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = '__all__'

#para listar todos los los recursos humanos con detalle del formulario anidado
class recursosHumanosFormularioSerializer(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = recursosHumanos
        fields = '__all__'

class serviciosNoPersonalesFormularioSerializer(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = serviciosNoPersonales
        fields = '__all__'

class materialesSumunistrosFormularioSerializer(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    Detalle = Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = materialesSumunistros
        fields = '__all__'

class activosFijosFormularioSerializer(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    Cotizacion = CotizacionFileSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = '__all__'

class activosFijosFormularioSerializer1(serializers.ModelSerializer):
    sum = serializers.IntegerField()
    # sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = ('Detalle','sum')

class activosFijosFormularioSerializer2(serializers.ModelSerializer):
    avg = serializers.DecimalField(max_digits=19, decimal_places=2)
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = ('avg','Detalle')

class activosFijosFormularioSerializer3(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = ('Detalle','sum')

''' serializador para devolver sum avg y sum 1 en activos fijos'''
class activosFijosFormularioSerializer4(serializers.ModelSerializer):
    sum = serializers.IntegerField()
    avg=serializers.DecimalField(max_digits=19, decimal_places=2)
    sum1=serializers.DecimalField(max_digits=19, decimal_places=2)

    # Cotizacion=CotizacionFileSerializer(read_only=True) #aumetado
    class Meta:
        model = activosFijos
        fields = ('Detalle_id','sum','avg','sum1','Cotizacion_id')

class activosFijosFormularioSerializer5(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    Detalle = Activo_y_EquipoSerializer(read_only=True)
    Cotizacion=CotizacionFileSerializer(read_only=True)
    class Meta:
        model = activosFijos
        fields = '__all__'
        
class recursosHumanosFormularioSerializer1(serializers.ModelSerializer):
    sum = serializers.IntegerField()
    class Meta:
        model = recursosHumanos
        fields = ('Profesional','sum')

#para sacar y ordenar por prioridad
class materialesSumunistrosFormularioSerializer1(serializers.ModelSerializer):
    sum = serializers.IntegerField()
    Detalle = Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = materialesSumunistros
        fields = ('Detalle','sum')

''' serializador para sacar el AVG de los precios'''
class materialesSumunistrosFormularioSerializer2(serializers.ModelSerializer):
    avg = serializers.DecimalField(max_digits=19, decimal_places=2)
    Detalle = Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = materialesSumunistros
        fields = ('Detalle','avg')

''' serializador para sumar el monto total que es formato decimal'''
class materialesSumunistrosFormularioSerializer3(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    Detalle = Material_y_SuministroSerializer(read_only=True)
    class Meta:
        model = materialesSumunistros
        fields = ('Detalle','sum')

''' serializador para devolver sum avg y sum 1 en materiales y suministros'''
class materialesSumunistrosFormularioSerializer4(serializers.ModelSerializer):
    sum = serializers.IntegerField()
    avg=serializers.DecimalField(max_digits=19, decimal_places=2)
    sum1=serializers.DecimalField(max_digits=19, decimal_places=2)
    class Meta:
        model = materialesSumunistros
        fields = ('Detalle_id','sum','avg','sum1')

class recursosHumanosFormularioSerializer3(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    Formularios = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = recursosHumanos
        fields = ('Formularios','sum')
        
class serviciosNoPersonalesFormularioSerializer3(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    Formularios = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = serviciosNoPersonales
        fields = ('Formularios','sum')

class objetivosGestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = objetivosGestion
        fields = '__all__'

class objetivosGestionFormularioSerializer(serializers.ModelSerializer):
    idFormularios = formulariosSerializer(read_only=True)
    class Meta:
        model = objetivosGestion
        fields = '__all__'
    
class operacionesFuncionamientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = operacionesFuncionamiento
        fields = '__all__'

    
class operacionesFuncionamientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = operacionesFuncionamiento
        fields = '__all__'

class mediosVerificacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = mediosVerificacion
        fields = '__all__'

class mediosVerificacionSerializer_OF_OG(serializers.ModelSerializer):
    idOperacion = operacionesFuncionamientoSerializer(read_only=True)
    ObjetivoPertence = objetivosGestionSerializer(read_only=True)
    class Meta:
        model = mediosVerificacion
        fields = '__all__'

class operacionesFuncionamientoSerializer_1(serializers.ModelSerializer):
    Formularios = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = operacionesFuncionamiento
        fields = '__all__'

class mediosVerificacionSerializer_OF_OG_1(serializers.ModelSerializer):
    idOperacion = operacionesFuncionamientoSerializer_1(read_only=True)
    ObjetivoPertence = objetivosGestionSerializer(read_only=True)
    class Meta:
        model = mediosVerificacion
        fields = '__all__'

