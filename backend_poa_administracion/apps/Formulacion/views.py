from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from django.db.models import FloatField
from django.db.models.functions import Cast
from rest_framework.parsers import FileUploadParser

from .models import formularios
from .serializers import formulariosSerializer
from .serializers import formulariosRespoCargoSerializer
from .serializers import formulariosRespoCargoSerializer_1

from .models import recursosHumanos
from .serializers import recursosHumanosSerializer
from .serializers import recursosHumanosFormularioSerializer
from .serializers import recursosHumanosFormularioSerializer1
from .serializers import recursosHumanosFormularioSerializer3

from .models import serviciosNoPersonales
from .serializers import serviciosNoPersonalesSerializer
from .serializers import serviciosNoPersonalesFormularioSerializer
from .serializers import serviciosNoPersonalesFormularioSerializer3

from .models import materialesSumunistros
from .serializers import materialesSumunistrosSerializer
from .serializers import materialesSumunistrosSerializer_D
from .serializers import materialesSumunistrosFormularioSerializer
from .serializers import materialesSumunistrosFormularioSerializer1
from .serializers import materialesSumunistrosFormularioSerializer2
from .serializers import materialesSumunistrosFormularioSerializer3
from .serializers import materialesSumunistrosFormularioSerializer4

from .models import activosFijos
from .serializers import activosFijosSerializer
from .serializers import activosFijosFormularioSerializer
from .serializers import activosFijosFormularioSerializer1
from .serializers import activosFijosFormularioSerializer2
from .serializers import activosFijosFormularioSerializer3
from .serializers import activosFijosFormularioSerializer4
from .serializers import activosFijosSerializer_D

from .serializers import formulariosPOAISerializer

from .models import objetivosGestion
from .serializers import objetivosGestionSerializer
from .serializers import objetivosGestionFormularioSerializer

from .models import operacionesFuncionamiento
from .serializers import operacionesFuncionamientoSerializer

from .models import mediosVerificacion
from .serializers import mediosVerificacionSerializer
from .serializers import mediosVerificacionSerializer_OF_OG
from .serializers import mediosVerificacionSerializer_OF_OG_1

from .models import CotizacionFile
from .serializers import CotizacionFileSerializer

from .serializers import activosFijosFormularioSerializer5

from django.db.models import Avg, Count, Min, Sum

class ListaFormularios(APIView):
    def get(self, request,format=None):
        Formulario = formularios.objects.all()
        serializer = formulariosRespoCargoSerializer_1(Formulario, many=True)
        return Response(serializer.data)

class ListaDetalleFormularios(APIView):
    def get_object(self, pk):
        try:
            return formularios.objects.get(pk=pk)
        except formularios.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = formulariosRespoCargoSerializer_1(snippet)
        return Response(serializer.data)


#sacamos formularios con una gestion, tipo y id POA dada
class ListaFormulariosGestion(APIView):
    def get(self, request, gestion,idP, type, format=None):        
        snippets= formularios.objects.filter(Gestion=gestion).filter(POAPertenece=idP).filter(tipo=type)
        serializer = formulariosSerializer(snippets, many=True)
        return Response(serializer.data)

#GUARDAR FORMULARIO 
class GuardarFormulario(APIView):
    def post(self, request, format=None):
        serializer = formulariosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# para cambiar de estado si se lleno todos los formularios
class ListaFormulariosGestionIdPOA(APIView):
    def get(self, request, gestion,idP, format=None):        
        snippets= formularios.objects.filter(Gestion=gestion).filter(POAPertenece=idP)
        serializer = formulariosSerializer(snippets, many=True)
        return Response(serializer.data)

''' Lista todos los Formularios de una gestion dada con su tipo'''
class ListaFormulariosGestionTipo(APIView):
    def get(self, request, gestion, type, format=None):        
        snippets= formularios.objects.filter(Gestion=gestion).filter(tipo=type)
        serializer = formulariosSerializer(snippets, many=True)
        return Response(serializer.data)  

''' ACTUALIZAR UN REGISTRO DE FORMULARIOS CAMBIO DE ESTADO A OBSERVADO'''
class actualizarFormularios(APIView):
    def get_object(self, pk):
        try:
            return formularios.objects.get(pk=pk)
        except formularios.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = formulariosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = formulariosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#-----------------------------------------------------
# PARA EL FORMULARIO 6 RRHH
#-----------------------------------------------------
''' GUARDA RRHH'''
class GuardarRecursosHumanos(APIView):
    def post(self, request, format=None):
        serializer = recursosHumanosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''LISTA TODOS LOS REGISTROS DE FORMULARIOS 'id' IGUAL AL PARAMETRO QUE LLEGA'''
class RecursosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= recursosHumanos.objects.filter(Formularios=id).order_by('Codigo')
        serializer = recursosHumanosSerializer(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE RRHH'''
class actualizarRecursoHumano(APIView):
    def get_object(self, pk):
        try:
            return recursosHumanos.objects.get(pk=pk)
        except recursosHumanos.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = recursosHumanosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = recursosHumanosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# ''' VISTAS PARA GENERAR LOS FORMULARIOS GENERALES ANTES DE LA REFORMULACION'''
# '''SACA TODOS LOS FORMULARIOS DE TIPO RRHH CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
# class ListaFormulariosPOAi(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=recursosHumanos.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = recursosHumanosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

# class ListaFormulariosPOAi_v1(APIView):
#     def get(self, request,gestion,unidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion).filter(POAPertenece__Unidad__nombre_unidad=unidad).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=recursosHumanos.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = recursosHumanosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)


''' VISTAS PARA GENERAR LOS FORMULARIOS GENERALES DESPUES DE LA REFORMULACION'''
'''SACA TODOS LOS FORMULARIOS DE TIPO RRHH CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
class ListaFormulariosPOAi(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=recursosHumanos.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = recursosHumanosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaFormulariosPOAi_v1(APIView):
    def get(self, request,gestion,unidad,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion).filter(POAPertenece__Unidad__nombre_unidad=unidad).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=recursosHumanos.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = recursosHumanosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

#----------------------------------------------------------
# PARA EL FORMULARIO 7 SNP
#----------------------------------------------------------
''' GUARDA SNP'''
class GuardarServiciosNoPersonales(APIView):
    def post(self, request, format=None):
        serializer = serviciosNoPersonalesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''LISTA TODOS LOS REGISTROS DE FORMULARIOS 'id' IGUAL AL PARAMETRO QUE LLEGA'''
class ServiciosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= serviciosNoPersonales.objects.filter(Formularios=id).order_by('Codigo')
        serializer = serviciosNoPersonalesSerializer(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE RRHH'''
class actualizarServiciosNoPersonales(APIView):
    def get_object(self, pk):
        try:
            return serviciosNoPersonales.objects.get(pk=pk)
        except serviciosNoPersonales.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = serviciosNoPersonalesSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = serviciosNoPersonalesSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    '''Elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# ''' VISTAS ANTES DE LA REFORMULACION '''
# '''SACA TODOS LOS FORMULARIOS DE TIPO SNP CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
# class ListaFormulariosPOAi1(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=serviciosNoPersonales.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = serviciosNoPersonalesFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

# class ListaFormulariosPOAi1_v1(APIView):
#     def get(self, request,gestion,unidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion).filter(POAPertenece__Unidad__nombre_unidad=unidad).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=serviciosNoPersonales.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = serviciosNoPersonalesFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

''' VISTAS DESPUES DE LA REFORMULACION '''
'''SACA TODOS LOS FORMULARIOS DE TIPO SNP CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
class ListaFormulariosPOAi1(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=serviciosNoPersonales.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = serviciosNoPersonalesFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaFormulariosPOAi1_v1(APIView):
    def get(self, request,gestion,unidad,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion).filter(POAPertenece__Unidad__nombre_unidad=unidad).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=serviciosNoPersonales.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = serviciosNoPersonalesFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

#----------------------------------------------------------
# PARA EL FORMULARIO 8 MS
#----------------------------------------------------------
''' GUARDA MS'''
class GuardarMaterialesSuministros(APIView):
    def post(self, request, format=None):
        serializer = materialesSumunistrosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''LISTA TODOS LOS REGISTROS DE FORMULARIOS 'id' IGUAL AL PARAMETRO QUE LLEGA'''
class MaterialesSuministrosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= materialesSumunistros.objects.filter(Formularios=id).order_by('Codigo')
        serializer = materialesSumunistrosSerializer(snippets, many=True)
        return Response(serializer.data)

''' LISTA TODOS LOS REGISTROS DE MATERIALES Y SUMINSITROS COENCIDENCIAS IGUAL ID MAS LA DESCRIPCION DE DETALLE'''
class MaterialesSuministrosDetalle(APIView):
    def get(self, request, id, format=None):
        snippets= materialesSumunistros.objects.filter(Formularios=id).order_by('Codigo')
        serializer = materialesSumunistrosSerializer_D(snippets, many=True)
        return Response(serializer.data)


''' ACTUALIZAR UN REGISTRO DE MS'''
class actualizarMaterialesySuministros(APIView):
    def get_object(self, pk):
        try:
            return materialesSumunistros.objects.get(pk=pk)
        except materialesSumunistros.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = materialesSumunistrosSerializer_D(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = materialesSumunistrosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    '''Elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# ''' VISTAR ANTES DE LA REFORMULACION'''
# '''SACA TODOS LOS FORMULARIOS DE TIPO MS CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
# class ListaFormulariosPOAi2(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).order_by('Detalle_CodigoPrioridad')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
#         serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

''' VISTAR DESPUES DE LA REFORMULACION'''
'''SACA TODOS LOS FORMULARIOS DE TIPO MS CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
class ListaFormulariosPOAi2(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
        serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

# .filter(POAPertenece__Estado_version= 'Activo')
#----------------------------------------------------------
# PARA EL FORMULARIO 9 AF
#----------------------------------------------------------
''' GUARDA AF'''
class GuardarActivosFijos(APIView):
    def post(self, request, format=None): 
        serializer = activosFijosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''LISTA TODOS LOS REGISTROS DE FORMULARIOS 'id' IGUAL AL PARAMETRO QUE LLEGA'''
class ActivosFijosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= activosFijos.objects.filter(Formularios=id).order_by('Codigo')
        serializer = activosFijosSerializer(snippets, many=True)
        return Response(serializer.data)

class ActivosFijosDetalle(APIView):
    def get(self, request, id, format=None):
        snippets= activosFijos.objects.filter(Formularios=id).order_by('Codigo')
        serializer = activosFijosSerializer_D(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE AF''' 
class actualizarActivosFijos(APIView):
    def get_object(self, pk):
        try:
            return activosFijos.objects.get(pk=pk)
        except activosFijos.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = activosFijosSerializer_D(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = activosFijosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' VISTAS ANTES DE LA REFORMULACION'''
'''SACA TODOS LOS FORMULARIOS DE TIPO AF CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
class ListaFormulariosPOAi3(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
        serializer = activosFijosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTAS DESPUES DE LA REFORMULACION'''
'''SACA TODOS LOS FORMULARIOS DE TIPO AF CON SU POA AL QUE PERTENECE EN ESTADO DE PROCESO DE REVISION Y GESTION COMO PARAMETRO'''
class ListaFormulariosPOAi3(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
        serializer = activosFijosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTAS ANTES DE LA REFORMULACION'''
# class ListaFormulariosPOAi3Total(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).order_by('Detalle_CodigoPrioridad')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = activosFijosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

''' VISTA DESPUES DE LA REFORMULACION'''
class ListaFormulariosPOAi3Total(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = activosFijosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

# # Nuevos para generar
# # uso de SUM y AVG para Materiales y Suministros -- ANTES DE LA REFORMULACION--
# #--------------------------------------------MATERIALES Y SUMINISTROS---------------------------#
# ''' Devuelve el total de CantidadNecesario de un MaterialSuminsitro'''
# class SumaCantidadNecesariaMS(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).filter(Detalle__id=fk)
#         serializer = materialesSumunistrosFormularioSerializer1(snippets, many=True)
#         return Response(serializer.data)

# ''' devuelve el promedio de los precios unitarios de un materialSuministro'''
# class PromedioPrecioUnitario(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(avg=Avg('CostoUnitario')).filter(Detalle__id=fk)
#         serializer = materialesSumunistrosFormularioSerializer2(snippets, many=True)
#         return Response(serializer.data)
        
# ''' devulve la suma de Monto total de un Material y Suminsitro'''
# class SumaMontoTotalMS(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Detalle__id=fk)
#         serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)
              
# ''' para sacar todos los materiales y suministros q estan en un poa de proceso de revision y de una gestion dada''' 
# class ListaFormulariosPOAi2TOTAL(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

# class SumaTotalMontoTotal(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Formularios__Gestion=gestion)
#         serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# class SumaTotalTipo(APIView):
#     def get(self, request,gestion,tipo,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.values('Detalle__Tipo').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Detalle__Tipo=tipo)
#         serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# '''devulve la suma de cantidad necesario mas el promedio de precios mas la suma total de cada material con el detalle_id'''
# class SumaPromedioSumatotalMS(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets= materialesSumunistros.objects.values('Detalle_id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).annotate(avg=Avg('CostoUnitario')).annotate(sum1=Sum('MontoTotal')).order_by('Detalle_id')
#         serializer = materialesSumunistrosFormularioSerializer4(snippets, many=True)
#         return Response(serializer.data)

# '''devulve laa unidad que solicitaron un determinado material '''
# class UnidadesSolicitantes_MS(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=materialesSumunistros.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
#         serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

# #--------------------------------------------RECURSOS HUMANOS---------------------------#
# ''' saca el total en monto de RRHH GENERAL'''
# class SumaTotalMontoTotalRRHH(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=recursosHumanos.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('MontoAnual')).filter(Formularios__Gestion=gestion)
#         serializer = recursosHumanosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# #--------------------------------------------SERVICIOS NO PERSONALES---------------------------#
# '''saca el total en monto de SNP General'''
# class SumaTotalMontoTotalSNP(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=serviciosNoPersonales.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('PFPrecioReferencial')).filter(Formularios__Gestion=gestion)
#         serializer = serviciosNoPersonalesFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# #--------------------------------------------ACTIVOS FIJOS---------------------------#
# # Nuevos para generar
# # uso de SUM y AVG para ACTIVOS FIJOS

# class SumaCantidadNecesariaAF(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).filter(Detalle__id=fk)
#         serializer = activosFijosFormularioSerializer1(snippets, many=True)
#         return Response(serializer.data)

# ''' devuelve el minimo de los precios unitarios de un ACTIVOS FIJOS'''
# class PromedioPrecioUnitarioAF(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(avg=Min('PrecioReferencial')).filter(Detalle__id=fk)
#         serializer = activosFijosFormularioSerializer2(snippets, many=True)
#         return Response(serializer.data)

# class SumaMontoTotalAF(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Detalle__id=fk)
#         serializer = activosFijosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# class SumaTotalTipoAF(APIView):
#     def get(self, request,gestion,tipo,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.values('Detalle__Tipo').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Detalle__Tipo=tipo)
#         serializer = activosFijosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# class SumaTotalMontoTotalAF(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Formularios__Gestion=gestion)
#         serializer = activosFijosFormularioSerializer3(snippets, many=True)
#         return Response(serializer.data)

# '''devulve la suma de cantidad necesario mas el promedio de precios mas la suma total de cada material con el detalle_id'''
# class SumaPromedioSumatotalAF(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion)
#         lista=respuesta.values_list('id',flat=True)
#         snippets= activosFijos.objects.values('Detalle_id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).annotate(avg=Min('PrecioReferencial')).annotate(sum1=Sum('CostoTotal')).order_by('Detalle_id')
#         serializer = activosFijosFormularioSerializer4(snippets, many=True)
#         return Response(serializer.data)

# '''devulve laa unidad que solicitaron un determinado material '''
# class UnidadesSolicitantes_AF(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
#         serializer = activosFijosFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

# class Cotizaciones_AF(APIView):
#     def get(self, request,gestion,fk,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=activosFijos.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
#         serializer = activosFijosFormularioSerializer5(snippets, many=True)
#         return Response(serializer.data)

#------------------*******--------------------------------------PARA REFORMULACION-----------------*****---------------------#
# Nuevos para generar
# uso de SUM y AVG para Materiales y Suministros -- DESPUES DE LA REFORMULACION--
#--------------------------------------------MATERIALES Y SUMINISTROS---------------------------#
''' Devuelve el total de CantidadNecesario de un MaterialSuminsitro'''
class SumaCantidadNecesariaMS(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).filter(Detalle__id=fk)
        serializer = materialesSumunistrosFormularioSerializer1(snippets, many=True)
        return Response(serializer.data)

''' devuelve el promedio de los precios unitarios de un materialSuministro'''
class PromedioPrecioUnitario(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(avg=Avg('CostoUnitario')).filter(Detalle__id=fk)
        serializer = materialesSumunistrosFormularioSerializer2(snippets, many=True)
        return Response(serializer.data)
        
''' devulve la suma de Monto total de un Material y Suminsitro'''
class SumaMontoTotalMS(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Detalle__id=fk)
        serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)
              
''' para sacar todos los materiales y suministros q estan en un poa de proceso de revision y de una gestion dada''' 
class ListaFormulariosPOAi2TOTAL(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

class SumaTotalMontoTotal(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Formularios__Gestion=gestion)
        serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

class SumaTotalTipo(APIView):
    def get(self, request,gestion,tipo,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.values('Detalle__Tipo').filter(Formularios__in=lista).annotate(sum=Sum('MontoTotal')).filter(Detalle__Tipo=tipo)
        serializer = materialesSumunistrosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

'''devulve la suma de cantidad necesario mas el promedio de precios mas la suma total de cada material con el detalle_id'''
class SumaPromedioSumatotalMS(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets= materialesSumunistros.objects.values('Detalle_id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).annotate(avg=Avg('CostoUnitario')).annotate(sum1=Sum('MontoTotal')).order_by('Detalle_id')
        serializer = materialesSumunistrosFormularioSerializer4(snippets, many=True)
        return Response(serializer.data)

'''devulve laa unidad que solicitaron un determinado material '''
class UnidadesSolicitantes_MS(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='materialeSuministro').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
        serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

#--------------------------------------------RECURSOS HUMANOS---------------------------#
''' saca el total en monto de RRHH GENERAL'''
class SumaTotalMontoTotalRRHH(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='recursosHumanos').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=recursosHumanos.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('MontoAnual')).filter(Formularios__Gestion=gestion)
        serializer = recursosHumanosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

#--------------------------------------------SERVICIOS NO PERSONALES---------------------------#
'''saca el total en monto de SNP General'''
class SumaTotalMontoTotalSNP(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='serviciosNoPersonales').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=serviciosNoPersonales.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('PFPrecioReferencial')).filter(Formularios__Gestion=gestion)
        serializer = serviciosNoPersonalesFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

#--------------------------------------------ACTIVOS FIJOS---------------------------#
# Nuevos para generar
# uso de SUM y AVG para ACTIVOS FIJOS

class SumaCantidadNecesariaAF(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).filter(Detalle__id=fk)
        serializer = activosFijosFormularioSerializer1(snippets, many=True)
        return Response(serializer.data)

''' devuelve el minimo de los precios unitarios de un ACTIVOS FIJOS'''
class PromedioPrecioUnitarioAF(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(avg=Min('PrecioReferencial')).filter(Detalle__id=fk)
        serializer = activosFijosFormularioSerializer2(snippets, many=True)
        return Response(serializer.data)

class SumaMontoTotalAF(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.values('Detalle__id').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Detalle__id=fk)
        serializer = activosFijosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

class SumaTotalTipoAF(APIView):
    def get(self, request,gestion,tipo,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.values('Detalle__Tipo').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Detalle__Tipo=tipo)
        serializer = activosFijosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

class SumaTotalMontoTotalAF(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.values('Formularios__Gestion').filter(Formularios__in=lista).annotate(sum=Sum('CostoTotal')).filter(Formularios__Gestion=gestion)
        serializer = activosFijosFormularioSerializer3(snippets, many=True)
        return Response(serializer.data)

'''devulve la suma de cantidad necesario mas el promedio de precios mas la suma total de cada material con el detalle_id'''
class SumaPromedioSumatotalAF(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo')
        lista=respuesta.values_list('id',flat=True)
        snippets= activosFijos.objects.values('Detalle_id').filter(Formularios__in=lista).annotate(sum=Sum('CantidadNecesario')).annotate(avg=Min('PrecioReferencial')).annotate(sum1=Sum('CostoTotal')).order_by('Detalle_id')
        serializer = activosFijosFormularioSerializer4(snippets, many=True)
        return Response(serializer.data)

'''devulve laa unidad que solicitaron un determinado material '''
class UnidadesSolicitantes_AF(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
        serializer = activosFijosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

class Cotizaciones_AF(APIView):
    def get(self, request,gestion,fk,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='activoFijo').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.all().filter(Formularios__in=lista).filter(Detalle__id=fk)
        serializer = activosFijosFormularioSerializer5(snippets, many=True)
        return Response(serializer.data)

#------------------*******--------------------------------------PARA REFORMULACION-----------------*****---------------------#
#----------------------------------------------------------
# PARA EL FORMULARIO 4 Objetivos de Gestion
#----------------------------------------------------------
'''PARA EL FORMULARIO 4 OBJETIVOS DE GESTION'''
class GuardarObjetivosGestion(APIView):
    def post(self, request, format=None):
        serializer = objetivosGestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''LISTA TODOS LOS REGISTROS DE FORMULARIOS 'id' IGUAL AL PARAMETRO QUE LLEGA'''

class ObjetivosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= objetivosGestion.objects.filter(idFormularios=id).order_by('OP','Codigo')
        serializer = objetivosGestionSerializer(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE OBJETIVOS DE GESTION'''

class actualizarObjetivoGestion(APIView):
    def get_object(self, pk):
        try:
            return objetivosGestion.objects.get(pk=pk)
        except objetiobjetivosGestionSerializer.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = objetivosGestionSerializer(snippet)
        return Response(serializer.data)

    """ACTUALIZA UN REGISTRO"""

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = objetivosGestionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' para el formulario de OPERACIONES 5'''
class ObjetivosGestionUnidadGestion(APIView):
    def get(self, request,gestion,Nunidad,idPOA,format=None):
        snippets=objetivosGestion.objects.all().filter(idFormularios__Gestion=gestion).filter(UnidadRes=Nunidad).filter(idFormularios__POAPertenece=idPOA).order_by('OP','Codigo')
        serializer = objetivosGestionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

# ''' ESTE ES EL CUAL CORRE EL SISTEMA ANTES DE LA REFORMULACION'''
# class ListaFormulariosPOAi_OG(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='ObjetivosGestion').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).order_by('OP','Codigo')
#         serializer = objetivosGestionFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

''' CAMBIAR A ESTE PARA QUE TOME EN CUENTA LA REFORMULACION'''
class ListaFormulariosPOAi_OG(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='ObjetivosGestion').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).order_by('OP','Codigo')
        serializer = objetivosGestionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

# ''' PRIMERA FORMA SIN REFORMULACION'''
# class ListaFormulariosPOAi_OG_v1(APIView):
#     def get(self, request,gestion,unidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='ObjetivosGestion').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).filter(UnidadRes=unidad).order_by('OP','Codigo')
#         serializer = objetivosGestionFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)

''' PRIMERA FORMA PARA LA  REFORMULACION'''
''' Lista los objetivos dfe gestion de la gestion y de una unidad en especifica'''
class ListaFormulariosPOAi_OG_v1(APIView):
    def get(self, request,gestion,unidad,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='ObjetivosGestion').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).filter(UnidadRes=unidad).order_by('OP','Codigo')
        serializer = objetivosGestionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)
#----------------------------------------------------------
# PARA EL FORMULARIO 5 Operaciones de Funcionamiento
#----------------------------------------------------------

'''GUARDAPARA EL FORMULARIO 5 OPERACIONES DE FUNCIONAMEINTO'''
class GuardaroperacionesFuncionamiento(APIView):
    def post(self, request, format=None):
        serializer = operacionesFuncionamientoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OperacionesDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= operacionesFuncionamiento.objects.filter(Formularios=id).order_by('CodigoOrd')
        serializer = operacionesFuncionamientoSerializer(snippets, many=True)
        return Response(serializer.data)

''' Actualiza un registro medio de verificacion''' 
class actualizaroperacionesFuncionamienton(APIView):
    def get_object(self, pk):
        try:
            return operacionesFuncionamiento.objects.get(pk=pk)
        except operacionesFuncionamiento.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = operacionesFuncionamientoSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = operacionesFuncionamientoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class MedioVerificacionDetalleID(APIView):
    def get(self, request, idO, format=None):
        snippets= mediosVerificacion.objects.filter(idOperacion__Formularios=idO).order_by('ObjetivoPertence__OP','ObjetivoPertence__Codigo')
        serializer = mediosVerificacionSerializer_OF_OG(snippets, many=True)
        return Response(serializer.data)

class GuardarmediosVerificacion(APIView):
    def post(self, request, format=None):
        serializer = mediosVerificacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Actualiza un registro medio de verificacion''' 
class actualizarmediosVerificacion(APIView):
    def get_object(self, pk):
        try:
            return mediosVerificacion.objects.get(pk=pk)
        except mediosVerificacion.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = mediosVerificacionSerializer_OF_OG(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = mediosVerificacionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMediosVerificacion(APIView):
    def get(self, request,format=None):
        MediosVerificacion = mediosVerificacion.objects.all()
        serializer = mediosVerificacionSerializer_OF_OG_1(MediosVerificacion, many=True)
        return Response(serializer.data)

class MedioVerificacionOperacion(APIView):
    def get(self, request, idO, format=None):
        snippets= mediosVerificacion.objects.filter(idOperacion_id=idO).order_by('ObjetivoPertence__OP','ObjetivoPertence__Codigo')
        serializer = mediosVerificacionSerializer_OF_OG(snippets, many=True)
        return Response(serializer.data)

''' para generar los formularios generales'''
# # VISTAS ANTES DE LA REFORMULACION

# class ListaFormulariosPOAi_OF(APIView):
#     def get(self, request,gestion,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista)
#         serializer = operacionesFuncionamientoSerializer(snippets, many=True)
#         return Response(serializer.data)

# class ListaFormulariosPOAi_OF_v1(APIView):
#     def get(self, request,gestion,unidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista).filter(UnidadRes=unidad).order_by('id')
#         serializer = operacionesFuncionamientoSerializer(snippets, many=True)
#         return Response(serializer.data)

'''VISTAS PARA LA REFORMULACION'''
class ListaFormulariosPOAi_OF(APIView):
    def get(self, request,gestion,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista)
        serializer = operacionesFuncionamientoSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaFormulariosPOAi_OF_v1(APIView):
    def get(self, request,gestion,unidad,format=None):
        respuesta= formularios.objects.filter(POAPertenece__Estado = 'Proceso Revision').filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).filter(POAPertenece__Estado_version= 'Activo').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista).filter(UnidadRes=unidad).order_by('id')
        serializer = operacionesFuncionamientoSerializer(snippets, many=True)
        return Response(serializer.data)

# VISTAS PARA EL ARCHIVO DE COTIZACION
class FileUploadView(APIView):
    parser_class = (FileUploadParser,)
    def post(self, request, *args, **kwargs):
      file_serializer = CotizacionFileSerializer(data=request.data)
      if file_serializer.is_valid():
          file_serializer.save()
          return Response(file_serializer.data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
          
class ListaArchivos(APIView):
    def get(self, request):
        datos = CotizacionFile.objects.all()
        data = CotizacionFileSerializer(datos, many=True).data
        return Response(data)

class ListaArchivos_id(APIView):
    def get(self, request, id ,format=None):        
        snippets= CotizacionFile.objects.filter(id=id)
        serializer = CotizacionFileSerializer(snippets, many=True)
        return Response(serializer.data)

class actualizarArchivoCotizacion(APIView):
    def get_object(self, pk):
        try:
            return CotizacionFile.objects.get(pk=pk)
        except CotizacionFile.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CotizacionFileSerializer(snippet)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#-----------------------------VISTAS PARA LA REFORMULACION---------------------------#
''' VISTA PARA EL OBJETIVO DE GESTIÓN'''
class ListaFormulariosPOAi_OG_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='ObjetivosGestion').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).order_by('OP','Codigo')
        serializer = objetivosGestionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTA PARA LAS OPERACIONES FUNCIONAMIENTO'''
class ListaFormulariosPOAi_OF_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='AccionesyOperaciones').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = operacionesFuncionamientoSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTA PARA RECURSOS HUMANOS'''
class ListaFormulariosPOAi_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='recursosHumanos').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=recursosHumanos.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = recursosHumanosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTA PARA SERVICIOS NO PERSONALES'''
class ListaFormulariosPOAi1_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='serviciosNoPersonales').order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=serviciosNoPersonales.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = serviciosNoPersonalesFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTA PARA MATERIALES Y SUMINISTROS'''
class ListaFormulariosPOAi2_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='materialeSuministro').order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=materialesSumunistros.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
        serializer = materialesSumunistrosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)

''' VISTAS PARA ACTIVOS FIJOS'''
class ListaFormulariosPOAi3_v2(APIView):
    def get(self, request,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='activoFijo').order_by('Detalle_CodigoPrioridad')
        lista=respuesta.values_list('id',flat=True)
        snippets=activosFijos.objects.distinct("Detalle_id").filter(Formularios__in=lista).order_by('Detalle_id')
        serializer = activosFijosFormularioSerializer(snippets, many=True)
        return Response(serializer.data)