from django.db import models
from ..responsablesInformacion.models import Personal
from ..Poai.models import POAi
from ..Listas_para_Formulacion.models import Material_y_Suministro
from ..Listas_para_Formulacion.models import Activo_y_Equipo

#Formularios
class formularios(models.Model):
    Gestion = models.IntegerField()
    Fecha=models.DateField()
    ESTADO_CHOICES= (
    ('Formulacion', 'En Proceso de Formulacion'),
    ('Aprobacion', 'En Proceso de Aprobacion'),
    ('Aprobado', 'Aprobado'),
    ('Observado', 'Observado'),
    ('Vigente', 'Vigente'),
    ('Cumplido', 'Cumplido'),
    ('Revision', 'En Proceso de Revision'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='En Proceso')
    TIPO_CHOICES= (
    ('ObjetivosGestion', 'Objetivos de Gestion'),
    ('AccionesyOperaciones', 'Acciones y Operaciones'),
    ('recursosHumanos', 'Recursos Humanos'),
    ('serviciosNoPersonales', 'Servicios No Personales'),
    ('materialeSuministro', 'Materiales y Suministros'),
    ('activoFijo', 'Activos Fijos'),
    )
    tipo = models.CharField(max_length=50, choices=TIPO_CHOICES)
    # tipo=models.CharField(max_length=500) #puede ser formulario de gestion, operaciones CAMBAIR PARA Q SE ESCOGE
    ResponsableInformacion=models.ForeignKey(Personal,on_delete=models.CASCADE)
    POAPertenece=models.ForeignKey(POAi,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios En General")

#Formulario para registrar ObjetviosGestion
class objetivosGestion(models.Model):
    ACP = models.CharField(max_length=500)
    AE = models.CharField(max_length=500)
    OP = models.CharField(max_length=500)
    Codigo = models.IntegerField()
    ObjetivoG = models.CharField(max_length=500)
    LineaBase = models.IntegerField(blank=True,null=True,default=0)
    LineaGestion = models.IntegerField(blank=True,null=True,default=0)
    IndicadorEficiencia = models.CharField(max_length=200)
    indicadorEficacia = models.CharField(max_length=200)
    UnidadRes = models.CharField(max_length=100)
    TurnoUnidad = models.CharField(max_length=100,blank=True,null=True)
    Observaciones = models.CharField(max_length=500,blank=True,null=True)
    CodigoOrd = models.IntegerField() #aumentado para ordenar
    idFormularios=models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formulario Objetivos de Gestión")

#Formulario para registrar Operaciones de Funcionamiento
class operacionesFuncionamiento(models.Model):
    Descripcion = models.TextField(max_length=500)
    UnidadRes = models.CharField(max_length=50)
    FechaInicio=models.DateField()
    FechaFin=models.DateField()
    CodigoOrd = models.IntegerField()
    Formularios=models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formulario De Operaciones de Funcionamiento")
        
#formulario de medio de verificacion pues una operacion puede tener varios medios de verificación

class mediosVerificacion(models.Model):
    descripcionMV = models.CharField(max_length=100)
    idOperacion=models.ForeignKey(operacionesFuncionamiento,on_delete=models.CASCADE)
    ObjetivoPertence=models.OneToOneField(objetivosGestion,on_delete=models.CASCADE) #verificar como se hace esto
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Medios de verificacion")

#Formulario para registrar RRHH
class recursosHumanos(models.Model):
    Denominacion = models.CharField(max_length=500)
    CargaHoraria_CHOICES= (
    ('t/c', 'Tiempo Completo'),
    ('m/t', 'Medio Tiempo'),
    ('','Otro')
    )
    CargaHoraria = models.CharField(max_length=20, choices=CargaHoraria_CHOICES, default='',blank=True)
    Profesional = models.CharField(max_length=500)
    NEPermanenteExistente = models.IntegerField(blank=True,null=True,default=0)
    NEPermanenteRequerido = models.IntegerField(blank=True,null=True,default=0)
    NENOPermanenteExistente = models.IntegerField(blank=True,null=True,default=0)
    NENOPermanenteRequerido = models.IntegerField(blank=True,null=True,default=0)
    EscalaSalarial = models.IntegerField(blank=True,null=True,default=0)
    TiempoDe = models.DateField()
    TiempoHasta = models.DateField()
    TiempoMeses = models.IntegerField()
    MontoMensual = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    MontoAnual = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    Observaciones = models.CharField(max_length=500,blank=True,null=True)
    Codigo = models.IntegerField() #aumentado para ordenar
    Formularios=models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios Recursos Humanos")

#formulario para registra Requerimiento de Servicios No personales
class serviciosNoPersonales(models.Model):
    Modalidad = models.CharField(max_length=500,blank=True,null=True)
    TipoContratacion = models.CharField(max_length=500,blank=True,null=True)
    DenominacionServicio=models.CharField(max_length=500)
    MesProgramado = models.CharField(max_length=100,blank=True,null=True)
    AñoProgramado = models.CharField(max_length=500,blank=True,null=True)  #cambiado a string
    NumeroMeses = models.IntegerField(blank=True,null=True,default=0)
    PrecioMensual = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    PFPrecioReferencial = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    FFCodigo = models.CharField(max_length=100,blank=True,null=True)
    FFDetalle = models.CharField(max_length=300,blank=True,null=True)
    Codigo = models.IntegerField()#aumentado para ordenar
    Formularios=models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios Servicios No Personales")

# formulario para registra Materiales y Suministros
class materialesSumunistros(models.Model):
    Detalle = models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    Unidad = models.CharField(max_length=100,blank=True,null=True)
    CantidadExistente = models.IntegerField(blank=True,null=True,default=0)
    CantidadNecesario = models.IntegerField(blank=True,null=True,default=0)
    CREnero = models.CharField(max_length=10,blank=True,null=True)
    CRFebrero = models.CharField(max_length=10,blank=True,null=True)
    CRMarzo = models.CharField(max_length=10,blank=True,null=True)
    CRAbril = models.CharField(max_length=10,blank=True,null=True)
    CRMayo = models.CharField(max_length=10,blank=True,null=True)
    CRJunio = models.CharField(max_length=10,blank=True,null=True)
    CRJulio = models.CharField(max_length=10,blank=True,null=True)
    CRAgosto = models.CharField(max_length=10,blank=True,null=True)
    CRSeptiembre = models.CharField(max_length=10,blank=True,null=True)
    CROctubre = models.CharField(max_length=10,blank=True,null=True)
    CRNoviembre = models.CharField(max_length=10,blank=True,null=True)
    CRDiciembre = models.CharField(max_length=10,blank=True,null=True)
    CostoUnitario = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #float 
    MontoTotal = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #float
    Codigo = models.IntegerField()#aumentado para ordenar
    Formularios = models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios Materiales y Suministros")

class CotizacionFile(models.Model):
    file = models.FileField(blank=False, null=False)
    def __str__(self):
        return self.file.name
        
# formulario para registra Requerimiento de Activos Fijos
class activosFijos(models.Model):
    Detalle = models.ForeignKey(Activo_y_Equipo,on_delete=models.CASCADE)
    CantidadExistente = models.IntegerField(blank=True,null=True,default=0)
    CantidadNecesario = models.IntegerField(blank=True,null=True,default=0)
    CREnero = models.CharField(max_length=10,blank=True,null=True)
    CRFebrero = models.CharField(max_length=10,blank=True,null=True)
    CRMarzo = models.CharField(max_length=10,blank=True,null=True)
    CRAbril = models.CharField(max_length=10,blank=True,null=True)
    CRMayo = models.CharField(max_length=10,blank=True,null=True)
    CRJunio = models.CharField(max_length=10,blank=True,null=True)
    CRJulio = models.CharField(max_length=10,blank=True,null=True)
    CRAgosto = models.CharField(max_length=10,blank=True,null=True)
    CRSeptiembre = models.CharField(max_length=10,blank=True,null=True)
    CROctubre = models.CharField(max_length=10,blank=True,null=True)
    CRNoviembre = models.CharField(max_length=10,blank=True,null=True)
    CRDiciembre = models.CharField(max_length=10,blank=True,null=True)
    PartidaPresupuestaria = models.IntegerField(blank=True,null=True,default=0)
    PrecioReferencial = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) 
    CostoTotal = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    Codigo = models.IntegerField()
    Cotizacion=models.ForeignKey(CotizacionFile,on_delete=models.CASCADE)
    Formularios = models.ForeignKey(formularios,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formularios Activos Fijos")


