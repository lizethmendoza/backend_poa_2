from django.urls import path
from .views import ListarEvaluciones
from .views import GuardarEvaluacion
from .views import ListaEvaluacionGestionPeriodo
from .views import GuardarFormularioEvaluacion
from .views import GuardaresEvaluadoObjGestion
from .views import ListaFormulariosEvaluacion
from .views import ListaEvaluacionObjGestion
from .views import ListaFormulariosEvaluacionOPE
from .views import GuardarFormularioEvaluacionOPE
from .views import GuardaresEvaluadoOperacionFuncionamiento
from .views import ListaEvaluacionOperacionesFuncionamiento
from .views import ListaEvaluacionObjGestionUnidad
from .views import ListaEvaluacionOperacionesFuncionamientoUnidad
from .views import ActualizarEvaluacion

from .views import ListarEvalucionesAprobado
# from .views import ListaMedioVerificacionId
# from .views import actualizarMedioVerificacionFile
# from .views import actualizarAvance_1
# from .views import SiexisteSeguimientoGestion
# from .views import SumaTotalObjetivosGestion
# from .views import ListarSeguimientoEstado
# from .views import ListarSeguimientoGestionPeriodo
# # from .views import ListarSeguimientoGestionEstado
# from .views import ListarSeguimientoGestionPeriodoUnidad
# from .views import ListarSeguimientoGestion
# from .views import ListarSeguimientoUnidad
# from .views import ListarSeguimientoGestionEstadoPeriodo

urlpatterns = [
    path('ListarEvaluciones/', ListarEvaluciones.as_view(),name="ListarEvaluciones"),
    path('GuardarEvaluacion/', GuardarEvaluacion.as_view(),name="GuardarEvaluacion"),
    path('ListaEvaluacionGestionPeriodo/<int:gestion>/<int:periodo>/<int:idPOA>/', ListaEvaluacionGestionPeriodo.as_view(),name="ListaEvaluacionGestionPeriodo"),
    path('GuardarFormularioEvaluacion/', GuardarFormularioEvaluacion.as_view(),name="GuardarFormularioEvaluacion"),
    path('GuardaresEvaluadoObjGestion/', GuardaresEvaluadoObjGestion.as_view(),name="GuardaresEvaluadoObjGestion"),
    path('ListaFormulariosEvaluacion/<int:gestion>/<int:periodo>/<int:idE>/', ListaFormulariosEvaluacion.as_view(),name="ListaFormulariosEvaluacion"),
    path('ListaEvaluacionObjGestion/<int:idFormEva>/', ListaEvaluacionObjGestion.as_view(),name="ListaEvaluacionObjGestion"),
    path('ListaFormulariosEvaluacionOPE/<int:gestion>/<int:periodo>/<int:idE>/', ListaFormulariosEvaluacionOPE.as_view(),name="ListaFormulariosEvaluacionOPE"),
    path('GuardarFormularioEvaluacionOPE/', GuardarFormularioEvaluacionOPE.as_view(),name="GuardarFormularioEvaluacionOPE"),
    path('GuardaresEvaluadoOperacionFuncionamiento/', GuardaresEvaluadoOperacionFuncionamiento.as_view(),name="GuardaresEvaluadoOperacionFuncionamiento"),
    path('ListaEvaluacionOperacionesFuncionamiento/<int:idFormEva>/', ListaEvaluacionOperacionesFuncionamiento.as_view(),name="ListaEvaluacionOperacionesFuncionamiento"),
    path('ListaEvaluacionObjGestionUnidad/<int:idFormEva>/<int:idUnidad>/', ListaEvaluacionObjGestionUnidad.as_view(),name="ListaEvaluacionObjGestionUnidad"),
    path('ListaEvaluacionOperacionesFuncionamientoUnidad/<int:idFormEva>/<int:idUnidad>/', ListaEvaluacionOperacionesFuncionamientoUnidad.as_view(),name="ListaEvaluacionOperacionesFuncionamientoUnidad"),
    path('ActualizarEvaluacion/<int:pk>/', ActualizarEvaluacion.as_view(),name="ActualizarEvaluacion"),
    path('ListarEvalucionesAprobado/', ListarEvalucionesAprobado.as_view(),name="ListarEvalucionesAprobado"),
    
    # path('ListaMedioVerificacionId/<int:id>/', ListaMedioVerificacionId.as_view(),name="ListaMedioVerificacionId"),
    # path('actualizarMedioVerificacionFile/<int:pk>/', actualizarMedioVerificacionFile.as_view(),name="actualizarMedioVerificacionFile"),
    # path('actualizarAvance_1/<int:pk>/', actualizarAvance_1.as_view(),name="actualizarAvance_1"),
    # path('SiexisteSeguimientoGestion/<int:gestion>/<int:unidad>/', SiexisteSeguimientoGestion.as_view(),name="SiexisteSeguimientoGestion"),
    # path('SumaTotalObjetivosGestion/<int:id>/', SumaTotalObjetivosGestion.as_view(),name="SumaTotalObjetivosGestion"),
    # path('ListarSeguimientoEstado/', ListarSeguimientoEstado.as_view(),name="ListarSeguimientoEstado"),
    # path('ListarSeguimientoGestionPeriodo/<int:gestion>/<str:periodo>/', ListarSeguimientoGestionPeriodo.as_view(),name="ListarSeguimientoGestionPeriodo"),
    # # path('ListarSeguimientoGestionEstado/<int:gestion>/<str:periodicidad>/<str:periodo>/', ListarSeguimientoGestionEstado.as_view(),name="ListarSeguimientoGestionEstado"),
    # path('ListarSeguimientoGestionPeriodoUnidad/<int:gestion>/<str:periodo>/<int:pkUnidad>/', ListarSeguimientoGestionPeriodoUnidad.as_view(),name="ListarSeguimientoGestionPeriodoUnidad"),
    # path('ListarSeguimientoGestion/<int:gestion>/', ListarSeguimientoGestion.as_view(),name="ListarSeguimientoGestion"),
    # path('ListarSeguimientoUnidad/<int:idUnidad>/', ListarSeguimientoUnidad.as_view(),name="ListarSeguimientoUnidad"),
    # path('ListarSeguimientoGestionEstadoPeriodo/<int:gestion>/<int:periodo>/', ListarSeguimientoGestionEstadoPeriodo.as_view(),name="ListarSeguimientoGestionEstadoPeriodo"),
    
    


]