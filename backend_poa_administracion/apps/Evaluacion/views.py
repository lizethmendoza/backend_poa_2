from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser

from .models import EvaluacionGeneral
from .serializers import EvaluacionGeneralSerializer
from .serializers import EvaluacionPeriodoSerializer
from .models import FormularioEvaluacion
from .serializers import FormularioEvaluacionSerializer
from .models import esEvaluadoObjGestion
from .serializers import esEvaluadoObjGestionSerializer
from .serializers import FormularioEvaluacionSerializer_1
from .serializers import esEvaluadoObjGestionSerializer_1

from .serializers import FormularioEvaluacionOPESerializer
from .models import FormularioEvaluacionOPE
from .serializers import esEvaluadoOperacionFuncionamientoSerializer
from .models import esEvaluadoOperacionFuncionamiento
from .serializers import FormularioEvaluacionOPESerializer_1
from .serializers import esEvaluadoOperacionesFuncSerializer_1
from .serializers import EvaluacionPeriodoSerializer_1
# Create your views here.
'''Lista todos las evaluaciones'''
class ListarEvaluciones(APIView):
    def get(self, request):
        datos = EvaluacionGeneral.objects.all().order_by('Gestion','id')
        data = EvaluacionPeriodoSerializer_1(datos, many=True).data
        return Response(data)

''' Guarda evaluacion'''
class GuardarEvaluacion(APIView):
    def post(self, request, format=None):
        serializer = EvaluacionGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista los evaluacion de una determinada gestion, periodo '''
class ListaEvaluacionGestionPeriodo(APIView):
   def get(self, request, gestion,periodo,idPOA,format=None):
        datos = EvaluacionGeneral.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(idPOAg=idPOA).order_by('-Gestion')
        data = EvaluacionGeneralSerializer(datos, many=True).data
        return Response(data)

''' Guarda Formulario de Evaluacion'''
class GuardarFormularioEvaluacion(APIView):
    def post(self, request, format=None):
        serializer = FormularioEvaluacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Guarda Formulario de Evaluacion'''
class GuardaresEvaluadoObjGestion(APIView):
    def post(self, request, format=None):
        serializer = esEvaluadoObjGestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista de formularios de evaluacion con los parametros de gestion,periodo, idEvaluacion'''
class ListaFormulariosEvaluacion(APIView):
   def get(self, request, gestion,periodo,idE,format=None):
        datos = FormularioEvaluacion.objects.all().filter(Gestion=gestion).filter(periodo=periodo).filter(idEvaluacion=idE).order_by('-Gestion')
        data = FormularioEvaluacionSerializer_1(datos, many=True).data
        return Response(data)

'''Lista de evaluaciones de un determinado idFormularioEvalyacion'''
class ListaEvaluacionObjGestion(APIView):
   def get(self, request,idFormEva,format=None):
        datos = esEvaluadoObjGestion.objects.all().filter(idFormularioEvaluacion=idFormEva).order_by('idAvance__idObjetivoGestion__OP','idAvance__idObjetivoGestion__Codigo')
        data = esEvaluadoObjGestionSerializer_1(datos, many=True).data
        return Response(data)

'''Lista de evaluaciones de un determinado idFormularioEvalyacion y IdUnidad'''
class ListaEvaluacionObjGestionUnidad(APIView):
   def get(self, request,idFormEva,idUnidad,format=None):
        datos = esEvaluadoObjGestion.objects.all().filter(idFormularioEvaluacion=idFormEva).filter(idAvance__idSeguimiento__Unidad__cod_unidad=idUnidad).order_by('idAvance__idObjetivoGestion__OP','idAvance__idObjetivoGestion__Codigo')
        data = esEvaluadoObjGestionSerializer_1(datos, many=True).data
        return Response(data)

''' Lista de formularios de evaluacion con los parametros de gestion,periodo, idEvaluacion'''
class ListaFormulariosEvaluacionOPE(APIView):
   def get(self, request, gestion,periodo,idE,format=None):
        datos = FormularioEvaluacionOPE.objects.all().filter(Gestion=gestion).filter(periodo=periodo).filter(idEvaluacion=idE).order_by('-Gestion')
        data = FormularioEvaluacionOPESerializer_1(datos, many=True).data
        return Response(data)

''' Guarda Formulario de Evaluacion OPE '''
class GuardarFormularioEvaluacionOPE(APIView):
    def post(self, request, format=None):
        serializer = FormularioEvaluacionOPESerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Guarda es_Evaluado OPE'''
class GuardaresEvaluadoOperacionFuncionamiento(APIView):
    def post(self, request, format=None):
        serializer = esEvaluadoOperacionFuncionamientoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''Lista de evaluaciones de un determinado idFormularioEvaluacion'''
class ListaEvaluacionOperacionesFuncionamiento(APIView):
   def get(self, request,idFormEva,format=None):
        datos = esEvaluadoOperacionFuncionamiento.objects.all().filter(idFormularioEvaluacionOPE=idFormEva).order_by('idAvanceOPE__idOperacion__id')
        data = esEvaluadoOperacionesFuncSerializer_1(datos, many=True).data
        return Response(data)

class ListaEvaluacionOperacionesFuncionamientoUnidad(APIView):
   def get(self, request,idFormEva,idUnidad,format=None):
        datos = esEvaluadoOperacionFuncionamiento.objects.all().filter(idFormularioEvaluacionOPE=idFormEva).filter(idAvanceOPE__idSeguimiento__Unidad__cod_unidad=idUnidad).order_by('idAvanceOPE__idOperacion__id')
        data = esEvaluadoOperacionesFuncSerializer_1(datos, many=True).data
        return Response(data)
        
class ActualizarEvaluacion(APIView):
    def get_object(self, pk):
        try:
            return EvaluacionGeneral.objects.get(pk=pk)
        except EvaluacionGeneral.DoesNotExist:
            raise Http404
    """Devuelve registros de acuerdo a al id"""
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EvaluacionGeneralSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN POA"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EvaluacionGeneralSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''Lista todos las evaluaciones'''
class ListarEvalucionesAprobado(APIView):
    def get(self, request,format=None):
        datos = EvaluacionGeneral.objects.all().filter(Estado="Aprobado PROMES").order_by('Gestion','id')
        data = EvaluacionPeriodoSerializer(datos, many=True).data
        return Response(data)
