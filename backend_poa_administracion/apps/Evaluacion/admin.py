from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import EvaluacionGeneral
from .models import FormularioEvaluacion
from .models import esEvaluadoObjGestion
from .models import FormularioEvaluacionOPE
from .models import esEvaluadoOperacionFuncionamiento
# Register your models here.

@admin.register(EvaluacionGeneral)
class EvaluacionGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","Estado","periodo","parametro","idPOAg"]

@admin.register(FormularioEvaluacion)
class FormularioEvaluacionAdmin(admin.ModelAdmin):
    list_display = ["pk","Gestion","Fecha","porcentajePromedio", "criterioPromedio","idEvaluacion","periodo"]

@admin.register(esEvaluadoObjGestion)
class esEvaluadoObjGestionAdmin(admin.ModelAdmin):
    list_display = ["pk","porcentaje","criterio","idFormularioEvaluacion", "idAvance"]

@admin.register(FormularioEvaluacionOPE)
class FormularioEvaluacionOPEAdmin(admin.ModelAdmin):
    list_display = ["pk","Gestion","Fecha","porcentajePromedio", "criterioPromedio","idEvaluacion","periodo"]

@admin.register(esEvaluadoOperacionFuncionamiento)
class esEvaluadoOperacionFuncionamientoAdmin(admin.ModelAdmin):
    list_display = ["pk","porcentaje","criterio","idFormularioEvaluacionOPE", "idAvanceOPE"]