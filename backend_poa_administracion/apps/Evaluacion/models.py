from django.db import models
from ..Formulacion.models import objetivosGestion
from ..periodicidad.models import Periodo
from ..ParametrosEvaluacion.models import ParametrosEvaluacion
from ..Seguimiento.models import AvanceEjecutadoTrimestral
from ..SeguimientoOperacion.models import AvanceEjecutadoOperaciones
from ..POA_General.models import POAGeneral

class EvaluacionGeneral(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
        ('Proceso evaluacion', 'Proceso evaluacion'),
        ('Proceso Revision', 'Proceso Revision'),
        ('Observado', 'Observado'),
        ('Aprobado PROMES', 'Aprobado PROMES'),
        ('Vigente', 'Vigente'),
        ('Aprobado SSU', 'Aprobado SSU'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso evaluacion')
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    parametro=models.ForeignKey(ParametrosEvaluacion,on_delete=models.CASCADE)
    idPOAg = models.ForeignKey(POAGeneral,on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de evaluaciones generales")

class FormularioEvaluacion(models.Model):
    Gestion = models.IntegerField()
    Fecha=models.DateField()
    porcentajePromedio=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    criterioPromedio=models.CharField(max_length=50)
    idEvaluacion=models.ForeignKey(EvaluacionGeneral,on_delete=models.CASCADE)
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OG - Formulario Evaluacion - Objetivos de Gestion")

class esEvaluadoObjGestion(models.Model):
    porcentaje=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    criterio=models.CharField(max_length=50)
    idFormularioEvaluacion=models.ForeignKey(FormularioEvaluacion,on_delete=models.CASCADE)
    idAvance=models.ForeignKey(AvanceEjecutadoTrimestral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OG - Evaluaciones - Objetivos de Gestion")

class FormularioEvaluacionOPE(models.Model):
    Gestion = models.IntegerField()
    Fecha=models.DateField()
    porcentajePromedio=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    criterioPromedio=models.CharField(max_length=50)
    idEvaluacion=models.ForeignKey(EvaluacionGeneral,on_delete=models.CASCADE)
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OF - Formulario Evaluacion - Operación de Funcionamiento")

class esEvaluadoOperacionFuncionamiento(models.Model):
    porcentaje=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    criterio=models.CharField(max_length=50)
    idFormularioEvaluacionOPE=models.ForeignKey(FormularioEvaluacionOPE,on_delete=models.CASCADE)
    idAvanceOPE=models.ForeignKey(AvanceEjecutadoOperaciones,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OF - Evaluaciones - Operación de Funcionamiento")