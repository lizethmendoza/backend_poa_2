from rest_framework import serializers
from .models import EvaluacionGeneral
from ..periodicidad.serializers import PeriodoSerializerPeriodicidad
from ..ParametrosEvaluacion.serializers import ParametrosEvaluacionSerializer
from .models import FormularioEvaluacion
from .models import esEvaluadoObjGestion
from ..Seguimiento.serializers import AvanceEjecutadoTrimestralSerializer_4
from ..POA_General.serializers import POAGeneralSerializer

from .models import FormularioEvaluacionOPE
from .models import esEvaluadoOperacionFuncionamiento
from ..SeguimientoOperacion.serializers import AvanceEjecutadoOperacionesSerializer_1

class EvaluacionGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = EvaluacionGeneral
        fields = '__all__'

class EvaluacionPeriodoSerializer(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    parametro=ParametrosEvaluacionSerializer(read_only=True)
    class Meta:
        model = EvaluacionGeneral
        fields = '__all__'

class EvaluacionPeriodoSerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    parametro=ParametrosEvaluacionSerializer(read_only=True)
    idPOAg=POAGeneralSerializer(read_only=True)
    class Meta:
        model = EvaluacionGeneral
        fields = '__all__'

class FormularioEvaluacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioEvaluacion
        fields = '__all__'

class esEvaluadoObjGestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = esEvaluadoObjGestion
        fields = '__all__'

class FormularioEvaluacionSerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idEvaluacion=EvaluacionPeriodoSerializer(read_only=True)
    class Meta:
        model = FormularioEvaluacion
        fields = '__all__'

class esEvaluadoObjGestionSerializer_1(serializers.ModelSerializer):
    idAvance=AvanceEjecutadoTrimestralSerializer_4(read_only=True)
    class Meta:
        model = esEvaluadoObjGestion
        fields = '__all__'

class FormularioEvaluacionOPESerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioEvaluacionOPE
        fields = '__all__'

class esEvaluadoOperacionFuncionamientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = esEvaluadoOperacionFuncionamiento
        fields = '__all__'

class FormularioEvaluacionOPESerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idEvaluacion=EvaluacionPeriodoSerializer(read_only=True)
    class Meta:
        model = FormularioEvaluacionOPE
        fields = '__all__'

class esEvaluadoOperacionesFuncSerializer_1(serializers.ModelSerializer):
    idAvanceOPE=AvanceEjecutadoOperacionesSerializer_1(read_only=True)
    class Meta:
        model = esEvaluadoOperacionFuncionamiento
        fields = '__all__'