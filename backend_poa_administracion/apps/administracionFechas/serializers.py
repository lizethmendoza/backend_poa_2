from rest_framework import serializers
from .models import FechasFormulacionPOA

class FechasFormulacionPOASerializer(serializers.ModelSerializer):
    class Meta:
        model = FechasFormulacionPOA
        fields = '__all__'

