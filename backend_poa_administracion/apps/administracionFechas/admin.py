from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import FechasFormulacionPOA
# Register your models here.

@admin.register(FechasFormulacionPOA)
class FechasFormulacionPOAAdmin(admin.ModelAdmin):
    list_display = ["pk","FechaInicio","FechaFin","Observacion","Gestion","Estado"]