from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .models import FechasFormulacionPOA
from .serializers import FechasFormulacionPOASerializer

# Create your views here.

class GuardarFechasDeFormulacion(APIView):
    def post(self, request, format=None):
        serializer = FechasFormulacionPOASerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaFechasDeFormulacion(APIView):
    def get(self, request,format=None):
        ListaObservacionFormulario = FechasFormulacionPOA.objects.all().order_by('-Gestion')
        serializer = FechasFormulacionPOASerializer(ListaObservacionFormulario, many=True)
        return Response(serializer.data)

class ListaFechasDeFormulacionGestion(APIView):
    def get(self, request,gestion,format=None):
        ListaObservacionFormulario = FechasFormulacionPOA.objects.all().filter(Gestion=gestion).order_by('Gestion')
        serializer = FechasFormulacionPOASerializer(ListaObservacionFormulario, many=True)
        return Response(serializer.data)

class ListaFechasDeFormulacionEstado(APIView):
    def get(self, request,format=None):
        ListaFormularioEstado = FechasFormulacionPOA.objects.all().filter(Estado='Vigente')
        serializer = FechasFormulacionPOASerializer(ListaFormularioEstado, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO''' 
class actualizarFechasFormulacionPOA(APIView):
    def get_object(self, pk):
        try:
            return FechasFormulacionPOA.objects.get(pk=pk)
        except FechasFormulacionPOA.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FechasFormulacionPOASerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FechasFormulacionPOASerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)