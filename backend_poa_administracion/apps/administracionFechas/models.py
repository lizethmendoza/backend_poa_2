from django.db import models

# Create your models here.

class FechasFormulacionPOA(models.Model):
    FechaInicio =  models.DateField()
    FechaFin =  models.DateField()
    Observacion = models.CharField(max_length=500,blank=True)
    Gestion = models.IntegerField()
    Estado_CHOICES= (
    ('Vigente', 'Vigente'),
    ('Expirado', 'Expirado')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='Expirado',blank=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Fechas de Formulacion")