from django.urls import path
from .views import GuardarFechasDeFormulacion
from .views import ListaFechasDeFormulacion
from .views import ListaFechasDeFormulacionGestion
from .views import actualizarFechasFormulacionPOA
from .views import ListaFechasDeFormulacionEstado
urlpatterns = [
    path('GuardarFechasDeFormulacion/', GuardarFechasDeFormulacion.as_view(),name="GuardarFechasDeFormulacion"),
    path('ListaFechasDeFormulacion/', ListaFechasDeFormulacion.as_view(),name="ListaFechasDeFormulacion"),
    path('ListaFechasDeFormulacionGestion/<int:gestion>', ListaFechasDeFormulacionGestion.as_view(),name="ListaFechasDeFormulacionGestion"),
    path('actualizarFechasFormulacionPOA/<int:pk>', actualizarFechasFormulacionPOA.as_view(),name="actualizarFechasFormulacionPOA"),
    path('ListaFechasDeFormulacionEstado/', ListaFechasDeFormulacionEstado.as_view(),name="ListaFechasDeFormulacionEstado"),
]