from import_export import resources
from .models import Material_y_Suministro

from .models import MaterialEscritorio
from .models import MaterialLimpieza
from .models import Papeleria
from .models import MedicamentosInsumos
from .models import MaterialElectricoVarios
from .models import ReactivosInsumosLaboratorio
from .models import InsumosMaterialOdontologia

from .models import Activo_y_Equipo

from .models import Activo_Fijo
from .models import Equipo_Medico
from .models import ServiciosBasicos
from .models import MedicosEspecialistas
from .models import AtencionMedicoQuirurgicas
from .models import ServiciosMedicosHospitalarios

class MaterialEscritorioResource(resources.ModelResource):
    class Meta:
        model = MaterialEscritorio

class Material_y_SuministroResource(resources.ModelResource):
    class Meta:
        model = Material_y_Suministro
    
class MaterialLimpiezaResource(resources.ModelResource):
    class Meta:
        model = MaterialLimpieza
 
class PapeleriaResource(resources.ModelResource):
    class Meta:
        model = Papeleria
 
class MedicamentosInsumosResource(resources.ModelResource):
    class Meta:
        model = MedicamentosInsumos

class MaterialElectricoVariosResource(resources.ModelResource):
    class Meta:
        model = MaterialElectricoVarios
 
class ReactivosInsumosLaboratorioResource(resources.ModelResource):
    class Meta:
        model = ReactivosInsumosLaboratorio
 
class InsumosMaterialOdontologiaResource(resources.ModelResource):
    class Meta:
        model = InsumosMaterialOdontologia
 
class Activo_y_EquipoResource(resources.ModelResource):
    class Meta:
        model = Activo_y_Equipo

class Activo_FijoResource(resources.ModelResource):
    class Meta:
        model = Activo_Fijo

class Equipo_MedicoResource(resources.ModelResource):
    class Meta:
        model = Equipo_Medico

class ServiciosBasicosResource(resources.ModelResource):
    class Meta:
        model = ServiciosBasicos

class MedicosEspecialistasResource(resources.ModelResource):
    class Meta:
        model = MedicosEspecialistas

class AtencionMedicoQuirurgicasResource(resources.ModelResource):
    class Meta:
        model = AtencionMedicoQuirurgicas

class ServiciosMedicosHospitalariosResource(resources.ModelResource):
    class Meta:
        model = ServiciosMedicosHospitalarios