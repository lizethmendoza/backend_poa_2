from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import  Profesiones
from .models import  Modalidades
from .models import  TipoContratacion

from .models import  ServiciosBasicos
from .models import  MedicosEspecialistas
from .models import  AtencionMedicoQuirurgicas
from .models import  ServiciosMedicosHospitalarios

from .models import  Material_y_Suministro
from .models import  MaterialEscritorio
from .models import  MaterialLimpieza
from .models import  Papeleria
from .models import  MedicamentosInsumos
from .models import  MaterialElectricoVarios
from .models import  ReactivosInsumosLaboratorio
from .models import  InsumosMaterialOdontologia

from .models import  Activo_y_Equipo
from .models import  Activo_Fijo
from .models import  Equipo_Medico

from .models import IndicadoresEficiencia
from .models import MediosVerificacionSelect

@admin.register(Profesiones)
class ProfesionesAdmin(admin.ModelAdmin):
    list_display = ["pk","profesion"]

@admin.register(Modalidades)
class ModalidadesAdmin(admin.ModelAdmin):
    list_display = ["pk","detalle"]

@admin.register(TipoContratacion)
class TipoContratacionAdmin(admin.ModelAdmin):
    list_display = ["pk","detalle"]

@admin.register(Material_y_Suministro)
class Material_y_SuministroAdmin(ImportExportModelAdmin):
    list_display = ["pk","Tipo","CodigoPrioridad"]


@admin.register(MaterialEscritorio)
class MaterialEscritorioAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(MaterialLimpieza)
class MaterialLimpiezaAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(Papeleria)
class PapeleriaAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(MedicamentosInsumos)
class MedicamentosInsumosAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(MaterialElectricoVarios)
class MaterialElectricoVariosAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(ReactivosInsumosLaboratorio)
class ReactivosInsumosLaboratorioAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(InsumosMaterialOdontologia)
class InsumosMaterialOdontologiaAdmin(ImportExportModelAdmin):
    list_display = ["pk","codigoUnico","nombre_generico","unidad","precioUnitario","idMaterialSuministro"]

@admin.register(ServiciosBasicos)
class ServiciosBasicosAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","descripcion"]

@admin.register(MedicosEspecialistas)
class MedicosEspecialistasAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","descripcion"]

@admin.register(AtencionMedicoQuirurgicas)
class AtencionMedicoQuirurgicasAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","descripcion"]

@admin.register(ServiciosMedicosHospitalarios)
class ServiciosMedicosHospitalariosAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","descripcion"]


@admin.register(Activo_y_Equipo)
class Activo_y_EquipoAdmin(ImportExportModelAdmin):
    list_display = ["pk","Tipo","CodigoPrioridad"]

@admin.register(Activo_Fijo)
class Activo_FijoAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","nombre_generico","idActivoEquipo"]

@admin.register(Equipo_Medico)
class Equipo_MedicoAdmin(ImportExportModelAdmin):
    list_display = ["pk", "codigoUnico","nombre_generico","idActivoEquipo"]

@admin.register(IndicadoresEficiencia)
class IndicadoresEficienciaAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion"]

@admin.register(MediosVerificacionSelect)
class MediosVerificacionSelectAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion"]