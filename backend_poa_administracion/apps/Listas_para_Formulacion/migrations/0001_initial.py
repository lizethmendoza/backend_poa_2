# Generated by Django 2.2.3 on 2019-07-24 04:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activo_y_Equipo',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('Tipo', models.CharField(choices=[('ActivoFijo', 'ActivoFijo'), ('EquipoMedico', 'EquipoMedico')], default='Otro', max_length=500)),
                ('CodigoPrioridad', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'AE- Activos Fijos y Equipos Medicos',
            },
        ),
        migrations.CreateModel(
            name='AtencionMedicoQuirurgicas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('descripcion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Atencion Medico Quirurgicas',
            },
        ),
        migrations.CreateModel(
            name='IndicadoresEficiencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Descripcion', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name_plural': 'OG - Indicadores de Eficiencia',
            },
        ),
        migrations.CreateModel(
            name='Material_y_Suministro',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('Tipo', models.CharField(choices=[('MaterialEscritorio', 'MaterialEscritorio'), ('MaterialLimpieza', 'MaterialLimpieza'), ('Papeleria', 'Papeleria'), ('MedicamentosInsumos', 'MedicamentosInsumos'), ('MaterialElectricoVarios', 'MaterialElectricoVarios'), ('ReactivosInsumosLaboratorio', 'ReactivosInsumosLaboratorio'), ('InsumosMaterialOdontologia', 'InsumosMaterialOdontologia')], default='Otro', max_length=500)),
                ('CodigoPrioridad', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'MS- Materiales y Suministros en General',
            },
        ),
        migrations.CreateModel(
            name='MedicosEspecialistas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('descripcion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Medicos Especialistas',
            },
        ),
        migrations.CreateModel(
            name='MediosVerificacionSelect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Descripcion', models.CharField(max_length=300)),
            ],
            options={
                'verbose_name_plural': 'OA - Lista de medios de verificación',
            },
        ),
        migrations.CreateModel(
            name='Modalidades',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('detalle', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Modalidades',
            },
        ),
        migrations.CreateModel(
            name='Profesiones',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('profesion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'RH- Profesiones u Oficios',
            },
        ),
        migrations.CreateModel(
            name='ServiciosBasicos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('descripcion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Servicios Basicos',
            },
        ),
        migrations.CreateModel(
            name='ServiciosMedicosHospitalarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('descripcion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Servicios Medicos Hospitalarios',
            },
        ),
        migrations.CreateModel(
            name='TipoContratacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('detalle', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'SNP- Tipos de Contrataciones',
            },
        ),
        migrations.CreateModel(
            name='ReactivosInsumosLaboratorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Reactivos Insumos Laboratorio',
            },
        ),
        migrations.CreateModel(
            name='Papeleria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Papeleria',
            },
        ),
        migrations.CreateModel(
            name='MedicamentosInsumos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Medicamentos e Suministros',
            },
        ),
        migrations.CreateModel(
            name='MaterialLimpieza',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Material Limpieza',
            },
        ),
        migrations.CreateModel(
            name='MaterialEscritorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Material Escritorio',
            },
        ),
        migrations.CreateModel(
            name='MaterialElectricoVarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Material Electrico Varios',
            },
        ),
        migrations.CreateModel(
            name='InsumosMaterialOdontologia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=200)),
                ('nombre_generico', models.CharField(default='', max_length=300)),
                ('unidad', models.CharField(max_length=100)),
                ('precioUnitario', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('idMaterialSuministro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Material_y_Suministro')),
            ],
            options={
                'verbose_name_plural': 'MS - Insumos Material Odontologia',
            },
        ),
        migrations.CreateModel(
            name='Equipo_Medico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('nombre_generico', models.CharField(max_length=500)),
                ('idActivoEquipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Activo_y_Equipo')),
            ],
            options={
                'verbose_name_plural': 'AE - Equipos Medicos',
            },
        ),
        migrations.CreateModel(
            name='Activo_Fijo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigoUnico', models.CharField(default='S/C', max_length=500)),
                ('nombre_generico', models.CharField(max_length=500)),
                ('idActivoEquipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Listas_para_Formulacion.Activo_y_Equipo')),
            ],
            options={
                'verbose_name_plural': 'AE - Activos Fijos',
            },
        ),
    ]
