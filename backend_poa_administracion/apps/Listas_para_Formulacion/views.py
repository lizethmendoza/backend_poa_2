from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from tablib import Dataset

from .models import Profesiones
from .models import Modalidades
from .models import TipoContratacion

from .serializers import ProfesionesSerializer
from .serializers import ModalidadesSerializer
from .serializers import TipoContratacionSerializer

from .models import  Material_y_Suministro
from .models import  MaterialEscritorio
from .models import  MaterialLimpieza
from .models import  Papeleria
from .models import  MedicamentosInsumos
from .models import  MaterialElectricoVarios
from .models import  ReactivosInsumosLaboratorio
from .models import  InsumosMaterialOdontologia

from .models import IndicadoresEficiencia

from .serializers import Material_y_SuministroSerializer
from .serializers import MaterialEscritorioSerializer
from .serializers import MaterialLimpiezaSerializer
from .serializers import PapeleriaSerializer
from .serializers import MedicamentosInsumosSerializer
from .serializers import MaterialElectricoVariosSerializer
from .serializers import ReactivosInsumosLaboratorioSerializer
from .serializers import InsumosMaterialOdontologiaSerializer

from .models import  ServiciosBasicos
from .models import  MedicosEspecialistas
from .models import  AtencionMedicoQuirurgicas
from .models import  ServiciosMedicosHospitalarios

from .models import  Activo_y_Equipo
from .models import  Activo_Fijo
from .models import  Equipo_Medico

from .serializers import ServiciosBasicosSerializer
from .serializers import MedicosEspecialistasSerializer
from .serializers import AtencionMedicoQuirurgicasSerializer
from .serializers import ServiciosMedicosHospitalariosSerializer

from .serializers import Activo_y_EquipoSerializer
from .serializers import Activo_FijoSerializer
from .serializers import Equipo_MedicoSerializer

from .serializers import IndicadoresEficienciaSerializer

from .models import MediosVerificacionSelect
from .serializers import MediosVerificacionSelectSerializer

class ListaProfesiones(APIView):
    def get(self, request):
        datos = Profesiones.objects.all()
        data = ProfesionesSerializer(datos, many=True).data
        return Response(data)

class GuardarProfesiones(APIView):
    def post(self, request, format=None):
        serializer = ProfesionesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaModalidades(APIView):
    def get(self, request):
        datos = Modalidades.objects.all()
        data = ModalidadesSerializer(datos, many=True).data
        return Response(data)

class ListaTipoContratacion(APIView):
    def get(self, request):
        datos = TipoContratacion.objects.all()
        data = TipoContratacionSerializer(datos, many=True).data
        return Response(data)

#Guardar Materiales Y suministros en general

class GuardarMaterial_y_Suministro(APIView):
    def post(self, request, format=None):
        serializer = Material_y_SuministroSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#devulve el ultimo de materialSuministro general
class DevuelveUltimoMaterialSuministro(APIView):
    """DEVUELVE LA ULTIMA TUPLA DEL MODELO materialySuministro"""
    def get(self, request, format=None):
        snippets = Material_y_Suministro.objects.order_by('-id')[:1]
        serializer = Material_y_SuministroSerializer(snippets, many=True)
        return Response(serializer.data)

#Material de Escritorio
class ListaMaterialEscritorio(APIView):
    def get(self, request):
        datos = MaterialEscritorio.objects.all().order_by('codigoUnico')
        data = MaterialEscritorioSerializer(datos, many=True).data
        return Response(data)

class actualizarMaterialEscritorio(APIView):
    def get_object(self, pk):
        try:
            return MaterialEscritorio.objects.get(pk=pk)
        except MaterialEscritorio.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MaterialEscritorioSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MaterialEscritorioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMaterialEscritorio_id(APIView):
    def get(self, request, pk, format=None):
        datos = MaterialEscritorio.objects.all().filter(id=pk)
        data = MaterialEscritorioSerializer(datos, many=True).data
        return Response(data)

class GuardarMaterialEscritorio(APIView):
    def post(self, request, format=None):
        serializer = MaterialEscritorioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Material de limpieza
class ListaMaterialLimpieza(APIView):
    def get(self, request):
        datos = MaterialLimpieza.objects.all().order_by('codigoUnico')
        data = MaterialLimpiezaSerializer(datos, many=True).data
        return Response(data)
    
class actualizarListaMaterialLimpieza(APIView):
    def get_object(self, pk):
        try:
            return MaterialLimpieza.objects.get(pk=pk)
        except MaterialLimpieza.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MaterialLimpiezaSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer =MaterialLimpiezaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaMaterialLimpieza_id(APIView):
    def get(self, request, pk, format=None):
        datos = MaterialLimpieza.objects.all().filter(id=pk)
        data = MaterialLimpiezaSerializer(datos, many=True).data
        return Response(data)

class GuardarMaterialLimpieza(APIView):
    def post(self, request, format=None):
        serializer = MaterialLimpiezaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#MATERIAL DE PAPELERIA

class ListaPapeleria(APIView):
    def get(self, request):
        datos = Papeleria.objects.all().order_by('codigoUnico')
        data = PapeleriaSerializer(datos, many=True).data
        return Response(data)

class actualizarListaPapeleria(APIView):
    def get_object(self, pk):
        try:
            return Papeleria.objects.get(pk=pk)
        except Papeleria.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PapeleriaSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PapeleriaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaPapeleria_id(APIView):
    def get(self, request, pk, format=None):
        datos = Papeleria.objects.all().filter(id=pk)
        data = PapeleriaSerializer(datos, many=True).data
        return Response(data)

class GuardarPapeleria(APIView):
    def post(self, request, format=None):
        serializer = PapeleriaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#MATERIAL ELECTRICO
class ListaMaterialElectricoVarios(APIView):
    def get(self, request):
        datos = MaterialElectricoVarios.objects.all().order_by('codigoUnico')
        data = MaterialElectricoVariosSerializer(datos, many=True).data
        return Response(data)

class actualizarListaMaterialElectricoVarios(APIView):
    def get_object(self, pk):
        try:
            return MaterialElectricoVarios.objects.get(pk=pk)
        except MaterialElectricoVarios.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MaterialElectricoVariosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MaterialElectricoVariosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMaterialElectricoVarios_id(APIView):
    def get(self, request, pk, format=None):
        datos = MaterialElectricoVarios.objects.all().filter(id=pk)
        data = MaterialElectricoVariosSerializer(datos, many=True).data
        return Response(data)

class GuardarMaterialElectricoVarios(APIView):
    def post(self, request, format=None):
        serializer = MaterialElectricoVariosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#LISTA DE MEDICAMENTOS
class ListaMedicamentosInsumos(APIView):
    def get(self, request):
        datos = MedicamentosInsumos.objects.all().order_by('codigoUnico')
        data = MedicamentosInsumosSerializer(datos, many=True).data
        return Response(data)

class actualizarListaMedicamentosInsumos(APIView):
    def get_object(self, pk):
        try:
            return MedicamentosInsumos.objects.get(pk=pk)
        except MedicamentosInsumos.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedicamentosInsumosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer =MedicamentosInsumosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMedicamentosInsumos_id(APIView):
    def get(self, request, pk, format=None):
        datos = MedicamentosInsumos.objects.all().filter(id=pk)
        data = MedicamentosInsumosSerializer(datos, many=True).data
        return Response(data)

class GuardarMedicamentosInsumos(APIView):
    def post(self, request, format=None):
        serializer = MedicamentosInsumosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#LISTA DE REACTIVOS LABORATORIO        
class ListaReactivosInsumosLaboratorio(APIView):
    def get(self, request):
        datos = ReactivosInsumosLaboratorio.objects.all().order_by('codigoUnico')
        data = ReactivosInsumosLaboratorioSerializer(datos, many=True).data
        return Response(data)

class actualizarListaReactivosInsumosLaboratorio(APIView):
    def get_object(self, pk):
        try:
            return ReactivosInsumosLaboratorio.objects.get(pk=pk)
        except ReactivosInsumosLaboratorio.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ReactivosInsumosLaboratorioSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ReactivosInsumosLaboratorioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaReactivosInsumosLaboratorio_id(APIView):
    def get(self, request, pk, format=None):
        datos = ReactivosInsumosLaboratorio.objects.all().filter(id=pk)
        data = ReactivosInsumosLaboratorioSerializer(datos, many=True).data
        return Response(data)

class GuardarReactivosInsumosLaboratorio(APIView):
    def post(self, request, format=None):
        serializer = ReactivosInsumosLaboratorioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#MATERIAL DE ODONTOLOGIA
class ListaInsumosMaterialOdontologia(APIView):
    def get(self, request):
        datos = InsumosMaterialOdontologia.objects.all().order_by('codigoUnico')
        data = InsumosMaterialOdontologiaSerializer(datos, many=True).data
        return Response(data)

class actualizarListaInsumosMaterialOdontologias(APIView):
    def get_object(self, pk):
        try:
            return InsumosMaterialOdontologia.objects.get(pk=pk)
        except InsumosMaterialOdontologia.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = InsumosMaterialOdontologiaSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = InsumosMaterialOdontologiaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaInsumosMaterialOdontologia_id(APIView):
    def get(self, request, pk, format=None):
        datos = InsumosMaterialOdontologia.objects.all().filter(id=pk)
        data = InsumosMaterialOdontologiaSerializer(datos, many=True).data
        return Response(data)

class GuardarInsumosMaterialOdontologia(APIView):
    def post(self, request, format=None):
        serializer = InsumosMaterialOdontologiaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# SERVICIOS BASICOS      
class ListaServiciosBasicos(APIView):
    def get(self, request):
        datos = ServiciosBasicos.objects.all()
        data = ServiciosBasicosSerializer(datos, many=True).data
        return Response(data)

class actualizarListaServiciosBasicos(APIView):
    def get_object(self, pk):
        try:
            return ServiciosBasicos.objects.get(pk=pk)
        except ServiciosBasicos.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ServiciosBasicosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ServiciosBasicosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaServiciosBasicos_id(APIView):
    def get(self, request, pk, format=None):
        datos = ServiciosBasicos.objects.all().filter(id=pk)
        data = ServiciosBasicosSerializer(datos, many=True).data
        return Response(data)

class GuardarServiciosBasicos(APIView):
    def post(self, request, format=None):
        serializer = ServiciosBasicosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#MEDICOS ESPECIALISTAS
class ListaMedicosEspecialistas(APIView):
    def get(self, request):
        datos = MedicosEspecialistas.objects.all()
        data = MedicosEspecialistasSerializer(datos, many=True).data
        return Response(data)

class actualizarListaMedicosEspecialistas(APIView):
    def get_object(self, pk):
        try:
            return MedicosEspecialistas.objects.get(pk=pk)
        except MedicosEspecialistas.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedicosEspecialistasSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedicosEspecialistasSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMedicosEspecialistas_id(APIView):
    def get(self, request, pk, format=None):
        datos = MedicosEspecialistas.objects.all().filter(id=pk)
        data = MedicosEspecialistasSerializer(datos, many=True).data
        return Response(data)

class GuardarMedicosEspecialistas(APIView):
    def post(self, request, format=None):
        serializer = MedicosEspecialistasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#ATENCION MEDICO QUIRURGICAS
class ListaAtencionMedicoQuirurgicas(APIView):
    def get(self, request):
        datos = AtencionMedicoQuirurgicas.objects.all()
        data = AtencionMedicoQuirurgicasSerializer(datos, many=True).data
        return Response(data)

class actualizarListaAtencionMedicoQuirurgicas(APIView):
    def get_object(self, pk):
        try:
            return AtencionMedicoQuirurgicas.objects.get(pk=pk)
        except AtencionMedicoQuirurgicas.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AtencionMedicoQuirurgicasSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AtencionMedicoQuirurgicasSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaAtencionMedicoQuirurgicas_id(APIView):
    def get(self, request, pk, format=None):
        datos = AtencionMedicoQuirurgicas.objects.all().filter(id=pk)
        data = AtencionMedicoQuirurgicasSerializer(datos, many=True).data
        return Response(data)

class GuardarAtencionMedicoQuirurgicas(APIView):
    def post(self, request, format=None):
        serializer = AtencionMedicoQuirurgicasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#SERVICIOS MEDICOS HOSPITALARIOS
class ListaServiciosMedicosHospitalarios(APIView):
    def get(self, request):
        datos = ServiciosMedicosHospitalarios.objects.all()
        data = ServiciosMedicosHospitalariosSerializer(datos, many=True).data
        return Response(data)

class actualizarListaServiciosMedicosHospitalarios(APIView):
    def get_object(self, pk):
        try:
            return ServiciosMedicosHospitalarios.objects.get(pk=pk)
        except ServiciosMedicosHospitalarios.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ServiciosMedicosHospitalariosSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ServiciosMedicosHospitalariosSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaServiciosMedicosHospitalario_id(APIView):
    def get(self, request, pk, format=None):
        datos = ServiciosMedicosHospitalarios.objects.all().filter(id=pk)
        data = ServiciosMedicosHospitalariosSerializer(datos, many=True).data
        return Response(data)

class GuardarServiciosMedicosHospitalarios(APIView):
    def post(self, request, format=None):
        serializer = ServiciosMedicosHospitalariosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#Guardar ActivoEquipos en general
class GuardarActivo_y_Equipo(APIView):
    def post(self, request, format=None):
        serializer = Activo_y_EquipoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#devulve el ultimo de ActivoEquipos general
class DevuelveUltimoActivosEquipos(APIView):
    """DEVUELVE LA ULTIMA TUPLA DEL MODELO materialySuministro"""
    def get(self, request, format=None):
        snippets = Activo_y_Equipo.objects.order_by('-id')[:1]
        serializer = Activo_y_EquipoSerializer(snippets, many=True)
        return Response(serializer.data)


#PARA ACTIVOS FIJOS
class ListaActivo_Fijo(APIView):
    def get(self, request):
        datos = Activo_Fijo.objects.all()
        data = Activo_FijoSerializer(datos, many=True).data
        return Response(data)

class actualizarListaActivo_Fijo(APIView):
    def get_object(self, pk):
        try:
            return Activo_Fijo.objects.get(pk=pk)
        except Activo_Fijo.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = Activo_FijoSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = Activo_FijoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaActivo_Fijo_id(APIView):
    def get(self, request, pk, format=None):
        datos = Activo_Fijo.objects.all().filter(id=pk)
        data = Activo_FijoSerializer(datos, many=True).data
        return Response(data)

class GuardarActivo_Fijo(APIView):
    def post(self, request, format=None):
        serializer = Activo_FijoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#PARA EQUIPOS MEDICOS
class ListaEquipo_Medico(APIView):
    def get(self, request):
        datos = Equipo_Medico.objects.all()
        data = Equipo_MedicoSerializer(datos, many=True).data
        return Response(data)

class actualizarListaEquipo_Medico(APIView):
    def get_object(self, pk):
        try:
            return Equipo_Medico.objects.get(pk=pk)
        except Equipo_Medico.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = Equipo_MedicoSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = Equipo_MedicoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaEquipo_Medico_id(APIView):
    def get(self, request, pk, format=None):
        datos = Equipo_Medico.objects.all().filter(id=pk)
        data = Equipo_MedicoSerializer(datos, many=True).data
        return Response(data)

class GuardarEquipo_Medico(APIView):
    def post(self, request, format=None):
        serializer = Equipo_MedicoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' para sacar que material es si es una de las 7 opcioness '''
class MaterialSUministroDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= MaterialEscritorio.objects.filter(idMaterialSuministro=id)
        serializer = MaterialEscritorioSerializer(snippets, many=True)
        return Response(serializer.data)

class MaterialLimpiezaDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= MaterialLimpieza.objects.filter(idMaterialSuministro=id)
        serializer = MaterialLimpiezaSerializer(snippets, many=True)
        return Response(serializer.data)

class PapeleriaDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= Papeleria.objects.filter(idMaterialSuministro=id)
        serializer = PapeleriaSerializer(snippets, many=True)
        return Response(serializer.data)

class MedicamentosInsumosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= MedicamentosInsumos.objects.filter(idMaterialSuministro=id)
        serializer = MedicamentosInsumosSerializer(snippets, many=True)
        return Response(serializer.data)

class MaterialElectricoVariosDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= MaterialElectricoVarios.objects.filter(idMaterialSuministro=id)
        serializer = MaterialElectricoVariosSerializer(snippets, many=True)
        return Response(serializer.data)

class ReactivosInsumosLaboratorioDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= ReactivosInsumosLaboratorio.objects.filter(idMaterialSuministro=id)
        serializer = ReactivosInsumosLaboratorioSerializer(snippets, many=True)
        return Response(serializer.data)

class InsumosMaterialOdontologiaDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= InsumosMaterialOdontologia.objects.filter(idMaterialSuministro=id)
        serializer = InsumosMaterialOdontologiaSerializer(snippets, many=True)
        return Response(serializer.data)

class ActivoFijoDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= Activo_Fijo.objects.filter(idActivoEquipo=id)
        serializer = Activo_FijoSerializer(snippets, many=True)
        return Response(serializer.data)

class EquipoMedicoDetalleID(APIView):
    def get(self, request, id, format=None):
        snippets= Equipo_Medico.objects.filter(idActivoEquipo=id)
        serializer = Equipo_MedicoSerializer(snippets, many=True)
        return Response(serializer.data)

#indicadores de eficiencia formulario 4
class ListaIndicadoresEficiencia(APIView):
    def get(self, request):
        datos = IndicadoresEficiencia.objects.all()
        data = IndicadoresEficienciaSerializer(datos, many=True).data
        return Response(data)

class IndicadoresEficienciaBusca(APIView):
    def get(self, request, descripcion, format=None):
        snippets= IndicadoresEficiencia.objects.filter(Descripcion=descripcion)
        serializer = IndicadoresEficienciaSerializer(snippets, many=True)
        return Response(serializer.data)

class GuardarIndicadoresEficiencia(APIView):
    def post(self, request, format=None):
        serializer = IndicadoresEficienciaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Operaciones de funcionamiento-medios de verificación formulario 5

class ListaMediosVerificacionSelect(APIView):
    def get(self, request):
        datos = MediosVerificacionSelect.objects.all()
        data = MediosVerificacionSelectSerializer(datos, many=True).data
        return Response(data)

class MediosVerificacionSelectBusca(APIView):
    def get(self, request, descripcion, format=None):
        snippets= MediosVerificacionSelect.objects.filter(Descripcion=descripcion)
        serializer = MediosVerificacionSelectSerializer(snippets, many=True)
        return Response(serializer.data)

class GuardarMediosVerificacionSelect(APIView):
    def post(self, request, format=None):
        serializer = MediosVerificacionSelectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProfesionesSelectBusca(APIView):
    def get(self, request, descripcion, format=None):
        snippets= Profesiones.objects.filter(profesion=descripcion)
        serializer = ProfesionesSerializer(snippets, many=True)
        return Response(serializer.data)

# para subir material de escritorio 
def simple_upload(request):
    if request.method == 'POST':
        MaterialEscritorio_resource = MaterialEscritorioResource()
        dataset = Dataset()
        new_MaterialEscritorio = request.FILES['myfile']

        imported_data = dataset.load(new_MaterialEscritorio.read())
        result = MaterialEscritorio_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            MaterialEscritorio_resource.import_data(dataset, dry_run=False)  # Actually import now
    return render(request, 'core/simple_upload.html')


# para subir material de escritorio 
def simple_upload1(request):
    if request.method == 'POST':
        Material_y_Suministro_resource = Material_y_SuministroResource()
        dataset = Dataset()
        new_Material_y_Suministro = request.FILES['myfile']
        imported_data = dataset.load(new_Material_y_Suministro.read())
        result = Material_y_Suministro_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Material_y_Suministro_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload1.html')


def simple_upload2(request):
    if request.method == 'POST':
        MaterialLimpieza_resource = MaterialLimpiezaResource()
        dataset = Dataset()
        new_MaterialLimpieza = request.FILES['myfile']

        imported_data = dataset.load(new_MaterialLimpieza.read())
        result = MaterialLimpieza_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            MaterialLimpieza_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload2.html')

def simple_upload3(request):
    if request.method == 'POST':
        Papeleria_resource = PapeleriaResourceResource()
        dataset = Dataset()
        new_PapeleriaResource = request.FILES['myfile']

        imported_data = dataset.load(new_PapeleriaResource.read())
        result = Papeleria_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Papeleria_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload3.html')

def simple_upload4(request):
    if request.method == 'POST':
        MedicamentosInsumos_resource = MedicamentosInsumosResource()
        dataset = Dataset()
        new_MedicamentosInsumosResource = request.FILES['myfile']

        imported_data = dataset.load(new_MedicamentosInsumosResource.read())
        result = MedicamentosInsumos_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            MedicamentosInsumos_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload4.html')



def simple_upload5(request):
    if request.method == 'POST':
        MaterialElectricoVarios_resource = MaterialElectricoVariosResource()
        dataset = Dataset()
        new_MaterialElectricoVariosResource = request.FILES['myfile']

        imported_data = dataset.load(new_MaterialElectricoVariosResource.read())
        result = MaterialElectricoVarios_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            MaterialElectricoVarios_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload5.html')

def simple_upload6(request):
    if request.method == 'POST':
        ReactivosInsumosLaboratorio_resource = ReactivosInsumosLaboratorioResource()
        dataset = Dataset()
        new_ReactivosInsumosLaboratorioResource = request.FILES['myfile']

        imported_data = dataset.load(new_ReactivosInsumosLaboratorioResource.read())
        result = ReactivosInsumosLaboratorio_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            ReactivosInsumosLaboratorio_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload6.html')


def simple_upload7(request):
    if request.method == 'POST':
        InsumosMaterialOdontologia_resource = InsumosMaterialOdontologiaResource()
        dataset = Dataset()
        new_InsumosMaterialOdontologiaResource = request.FILES['myfile']

        imported_data = dataset.load(new_InsumosMaterialOdontologiaResource.read())
        result = InsumosMaterialOdontologia_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            InsumosMaterialOdontologia_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload7.html')

''' PARA IMPORTAR LAS LISTAS DE ACTIVOS FIJOS Y EQUIPOS MEDICOS'''

def simple_upload8(request):
    if request.method == 'POST':
        Activo_y_Equipo_resource = Activo_y_EquipoResource()
        dataset = Dataset()
        new_Activo_y_EquipoResource = request.FILES['myfile']
        imported_data = dataset.load(new_Activo_y_EquipoResource.read())
        result = Activo_y_Equipo_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Activo_y_Equipo_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload8.html')

def simple_upload9(request):
    if request.method == 'POST':
        Activo_Fijo_resource = Activo_FijoResource()
        dataset = Dataset()
        new_Activo_FijoResource = request.FILES['myfile']
        imported_data = dataset.load(new_Activo_FijoResource.read())
        result = Activo_Fijo_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Activo_Fijo_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload9.html')

def simple_upload10(request):
    if request.method == 'POST':
        Equipo_Medico_resource = Equipo_MedicoResource()
        dataset = Dataset()
        new_Equipo_MedicoResource = request.FILES['myfile']
        imported_data = dataset.load(new_Equipo_MedicoResource.read())
        result = Equipo_Medico_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Equipo_Medico_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload10.html')

def simple_upload11(request):
    if request.method == 'POST':
        ServiciosBasicos_resource = ServiciosBasicosResource()
        dataset = Dataset()
        new_ServiciosBasicosResource = request.FILES['myfile']
        imported_data = dataset.load(new_ServiciosBasicosResource.read())
        result = ServiciosBasicos_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            ServiciosBasicos_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload11.html')


def simple_upload12(request):
    if request.method == 'POST':
        MedicosEspecialistas_resource = MedicosEspecialistasResource()
        dataset = Dataset()
        new_MedicosEspecialistasResource = request.FILES['myfile']
        imported_data = dataset.load(new_MedicosEspecialistasResource.read())
        result = MedicosEspecialistas_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            MedicosEspecialistas_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload12.html')


def simple_upload13(request):
    if request.method == 'POST':
        AtencionMedicoQuirurgicas_resource = AtencionMedicoQuirurgicasResource()
        dataset = Dataset()
        new_AtencionMedicoQuirurgicasResource = request.FILES['myfile']
        imported_data = dataset.load(new_AtencionMedicoQuirurgicasResource.read())
        result = AtencionMedicoQuirurgicas_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            AtencionMedicoQuirurgicas_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload13.html')


def simple_upload14(request):
    if request.method == 'POST':
        ServiciosMedicosHospitalarios_resource = ServiciosMedicosHospitalariosResource()
        dataset = Dataset()
        new_ServiciosMedicosHospitalariosResource = request.FILES['myfile']
        imported_data = dataset.load(new_ServiciosMedicosHospitalariosResource.read())
        result = ServiciosMedicosHospitalarios_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            ServiciosMedicosHospitalarios_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload14.html')