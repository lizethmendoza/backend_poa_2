from django.urls import path
from .views import ListaProfesiones
from .views import GuardarProfesiones
from .views import ListaModalidades
from .views import ListaTipoContratacion

from .views import GuardarMaterial_y_Suministro
from .views import  DevuelveUltimoMaterialSuministro

from .views import ListaMaterialEscritorio
from .views import actualizarMaterialEscritorio
from .views import ListaMaterialEscritorio_id
from .views import GuardarMaterialEscritorio

from .views import ListaMaterialLimpieza
from .views import actualizarListaMaterialLimpieza
from .views import ListaMaterialLimpieza_id
from .views import GuardarMaterialLimpieza

from .views import ListaPapeleria
from .views import actualizarListaPapeleria
from .views import ListaPapeleria_id
from .views import GuardarPapeleria

from .views import ListaMaterialElectricoVarios
from .views import actualizarListaMaterialElectricoVarios
from .views import ListaMaterialElectricoVarios_id
from .views import GuardarMaterialElectricoVarios

from .views import ListaMedicamentosInsumos
from .views import actualizarListaMedicamentosInsumos
from .views import ListaMedicamentosInsumos_id
from .views import GuardarMedicamentosInsumos

from .views import ListaReactivosInsumosLaboratorio
from .views import actualizarListaReactivosInsumosLaboratorio
from .views import ListaReactivosInsumosLaboratorio_id
from .views import GuardarReactivosInsumosLaboratorio

from .views import ListaInsumosMaterialOdontologia
from .views import actualizarListaInsumosMaterialOdontologias
from .views import ListaInsumosMaterialOdontologia_id
from .views import GuardarInsumosMaterialOdontologia

from .views import ListaServiciosBasicos
from .views import actualizarListaServiciosBasicos
from .views import ListaServiciosBasicos_id
from .views import GuardarServiciosBasicos

from .views import ListaMedicosEspecialistas
from .views import actualizarListaMedicosEspecialistas
from .views import ListaMedicosEspecialistas_id
from .views import GuardarMedicosEspecialistas


from .views import ListaAtencionMedicoQuirurgicas
from .views import actualizarListaAtencionMedicoQuirurgicas
from .views import ListaAtencionMedicoQuirurgicas_id
from .views import GuardarAtencionMedicoQuirurgicas


from .views import ListaServiciosMedicosHospitalarios
from .views import actualizarListaServiciosMedicosHospitalarios
from .views import ListaServiciosMedicosHospitalario_id
from .views import GuardarServiciosMedicosHospitalarios

from .views import GuardarActivo_y_Equipo
from .views import DevuelveUltimoActivosEquipos
from .views import ListaActivo_Fijo
from .views import ActivoFijoDetalleID
from .views import actualizarListaEquipo_Medico
from .views import ListaEquipo_Medico_id
from .views import GuardarEquipo_Medico

from .views import ListaEquipo_Medico
from .views import EquipoMedicoDetalleID
from .views import actualizarListaActivo_Fijo
from .views import ListaActivo_Fijo_id
from .views import GuardarActivo_Fijo

from .views import MaterialSUministroDetalleID
from .views import MaterialLimpiezaDetalleID
from .views import PapeleriaDetalleID
from .views import MedicamentosInsumosDetalleID
from .views import MaterialElectricoVariosDetalleID
from .views import ReactivosInsumosLaboratorioDetalleID
from .views import InsumosMaterialOdontologiaDetalleID

from .views import ListaIndicadoresEficiencia
from .views import IndicadoresEficienciaBusca
from .views import GuardarIndicadoresEficiencia

from .views import ListaMediosVerificacionSelect
from .views import MediosVerificacionSelectBusca
from .views import GuardarMediosVerificacionSelect
from .views import ProfesionesSelectBusca

urlpatterns = [
    path('ListaProfesiones/', ListaProfesiones.as_view(),name="ListaProfesiones"),
    path('GuardarProfesiones/', GuardarProfesiones.as_view(),name="GuardarProfesiones"),
    path('ListaModalidades/', ListaModalidades.as_view(),name="ListaModalidades"),
    path('ListaTipoContratacion/', ListaTipoContratacion.as_view(),name="ListaTipoContratacion"),

    path('GuardarMaterial_y_Suministro/', GuardarMaterial_y_Suministro.as_view(),name="GuardarMaterial_y_Suministro"),
     path('DevuelveUltimoMaterialSuministro/', DevuelveUltimoMaterialSuministro.as_view(),name="DevuelveUltimoMaterialSuministro"),

    path('ListaMaterialEscritorio/', ListaMaterialEscritorio.as_view(),name="ListaMaterialEscritorio"),
    path('actualizarMaterialEscritorio/<int:pk>/', actualizarMaterialEscritorio.as_view(), name="actualizarMaterialEscritorio"),
    path('ListaMaterialEscritorio_id/<int:pk>/', ListaMaterialEscritorio_id.as_view(), name="ListaMaterialEscritorio_id"),
    path('GuardarMaterialEscritorio/', GuardarMaterialEscritorio.as_view(), name="GuardarMaterialEscritorio"),

    path('ListaMaterialLimpieza/', ListaMaterialLimpieza.as_view(),name="ListaMaterialLimpieza"),
    path('actualizarListaMaterialLimpieza/<int:pk>/', actualizarListaMaterialLimpieza.as_view(), name="actualizarListaMaterialLimpieza"),
    path('ListaMaterialLimpieza_id/<int:pk>/', ListaMaterialLimpieza_id.as_view(), name="ListaMaterialLimpieza_id"),
    path('GuardarMaterialLimpieza/', GuardarMaterialLimpieza.as_view(), name="GuardarMaterialLimpieza"),

    path('ListaPapeleria/', ListaPapeleria.as_view(),name="ListaPapeleria"),
    path('actualizarListaPapeleria/<int:pk>/', actualizarListaPapeleria.as_view(), name="actualizarListaPapeleria"),
    path('ListaPapeleria_id/<int:pk>/', ListaPapeleria_id.as_view(), name="ListaPapeleria_id"),
    path('GuardarPapeleria/', GuardarPapeleria.as_view(), name="GuardarPapeleria"),

    path('ListaMaterialElectricoVarios/', ListaMaterialElectricoVarios.as_view(),name="ListaMaterialElectricoVarios"),
    path('actualizarListaMaterialElectricoVarios/<int:pk>/', actualizarListaMaterialElectricoVarios.as_view(), name="actualizarListaMaterialElectricoVarios"),
    path('ListaMaterialElectricoVarios_id/<int:pk>/', ListaMaterialElectricoVarios_id.as_view(), name="ListaMaterialElectricoVarios_id"),
    path('GuardarMaterialElectricoVarios/', GuardarMaterialElectricoVarios.as_view(), name="GuardarMaterialElectricoVarios"),

    path('ListaMedicamentosInsumos/', ListaMedicamentosInsumos.as_view(),name="ListaMedicamentosInsumos"),
    path('actualizarListaMedicamentosInsumos/<int:pk>/', actualizarListaMedicamentosInsumos.as_view(), name="actualizarListaMedicamentosInsumos"),
    path('ListaMedicamentosInsumos_id/<int:pk>/', ListaMedicamentosInsumos_id.as_view(), name="ListaMedicamentosInsumos_id"),
    path('GuardarMedicamentosInsumos/', GuardarMedicamentosInsumos.as_view(), name="GuardarMedicamentosInsumos"),

    path('ListaReactivosInsumosLaboratorio/', ListaReactivosInsumosLaboratorio.as_view(),name="ListaReactivosInsumosLaboratorio"),
    path('actualizarListaReactivosInsumosLaboratorio/<int:pk>/', actualizarListaReactivosInsumosLaboratorio.as_view(), name="actualizarListaReactivosInsumosLaboratorio"),
    path('ListaReactivosInsumosLaboratorio_id/<int:pk>/', ListaReactivosInsumosLaboratorio_id.as_view(), name="ListaReactivosInsumosLaboratorio_id"),
    path('GuardarReactivosInsumosLaboratorio/', GuardarReactivosInsumosLaboratorio.as_view(), name="GuardarReactivosInsumosLaboratorio"),

    path('ListaInsumosMaterialOdontologia/', ListaInsumosMaterialOdontologia.as_view(),name="ListaInsumosMaterialOdontologia"),
    path('actualizarListaInsumosMaterialOdontologias/<int:pk>/', actualizarListaInsumosMaterialOdontologias.as_view(), name="actualizarListaInsumosMaterialOdontologias"),
    path('ListaInsumosMaterialOdontologia_id/<int:pk>/', ListaInsumosMaterialOdontologia_id.as_view(), name="ListaInsumosMaterialOdontologia_id"),
    path('GuardarInsumosMaterialOdontologia/', GuardarInsumosMaterialOdontologia.as_view(), name="GuardarInsumosMaterialOdontologia"),
    
    path('ListaServiciosBasicos/', ListaServiciosBasicos.as_view(),name="ListaServiciosBasicos"),
    path('actualizarListaServiciosBasicos/<int:pk>/', actualizarListaServiciosBasicos.as_view(), name="actualizarListaServiciosBasicos"),
    path('ListaServiciosBasicos_id/<int:pk>/', ListaServiciosBasicos_id.as_view(), name="ListaServiciosBasicos_id"),
    path('GuardarServiciosBasicos/', GuardarServiciosBasicos.as_view(), name="GuardarServiciosBasicos"),

    path('ListaMedicosEspecialistas/', ListaMedicosEspecialistas.as_view(),name="ListaMedicosEspecialistas"),
    path('actualizarListaMedicosEspecialistas/<int:pk>/', actualizarListaMedicosEspecialistas.as_view(), name="actualizarListaMedicosEspecialistas"),
    path('ListaMedicosEspecialistas_id/<int:pk>/', ListaMedicosEspecialistas_id.as_view(), name="ListaMedicosEspecialistas_id"),
    path('GuardarMedicosEspecialistas/', GuardarMedicosEspecialistas.as_view(), name="GuardarMedicosEspecialistas"),
    
    path('ListaAtencionMedicoQuirurgicas/', ListaAtencionMedicoQuirurgicas.as_view(),name="ListaAtencionMedicoQuirurgicas"),
    path('actualizarListaAtencionMedicoQuirurgicas/<int:pk>/', actualizarListaAtencionMedicoQuirurgicas.as_view(), name="actualizarListaAtencionMedicoQuirurgicas"),
    path('ListaAtencionMedicoQuirurgicas_id/<int:pk>/', ListaAtencionMedicoQuirurgicas_id.as_view(), name="ListaAtencionMedicoQuirurgicas_id"),
    path('GuardarAtencionMedicoQuirurgicas/', GuardarAtencionMedicoQuirurgicas.as_view(), name="GuardarAtencionMedicoQuirurgicas"),
    
    path('ListaServiciosMedicosHospitalarios/', ListaServiciosMedicosHospitalarios.as_view(),name="ListaServiciosMedicosHospitalarios"),
    path('actualizarListaServiciosMedicosHospitalarios/<int:pk>/', actualizarListaServiciosMedicosHospitalarios.as_view(), name="actualizarListaServiciosMedicosHospitalarios"),
    path('ListaServiciosMedicosHospitalario_id/<int:pk>/', ListaServiciosMedicosHospitalario_id.as_view(), name="ListaServiciosMedicosHospitalario_id"),
    path('GuardarServiciosMedicosHospitalarios/', GuardarServiciosMedicosHospitalarios.as_view(), name="GuardarServiciosMedicosHospitalarios"),
    
    

    
    path('GuardarActivo_y_Equipo/', GuardarActivo_y_Equipo.as_view(),name="GuardarActivo_y_Equipo"),
    path('DevuelveUltimoActivosEquipos/', DevuelveUltimoActivosEquipos.as_view(),name="DevuelveUltimoActivosEquipos"),

    path('ListaActivo_Fijo/', ListaActivo_Fijo.as_view(),name="ListaActivo_Fijo"),
    path('actualizarListaEquipo_Medico/<int:pk>/', actualizarListaEquipo_Medico.as_view(), name="actualizarListaEquipo_Medico"),
    path('ListaEquipo_Medico_id/<int:pk>/', ListaEquipo_Medico_id.as_view(), name="ListaEquipo_Medico_id"),
    path('GuardarEquipo_Medico/', GuardarEquipo_Medico.as_view(), name="GuardarEquipo_Medico"),

    path('ListaEquipo_Medico/', ListaEquipo_Medico.as_view(),name="ListaEquipo_Medico"),
    path('actualizarListaActivo_Fijo/<int:pk>/', actualizarListaActivo_Fijo.as_view(), name="actualizarListaActivo_Fijo"),
    path('ListaActivo_Fijo_id/<int:pk>/', ListaActivo_Fijo_id.as_view(), name="ListaActivo_Fijo_id"),
    path('GuardarActivo_Fijo/', GuardarActivo_Fijo.as_view(), name="GuardarActivo_Fijo"),
    
    path('MaterialSUministroDetalleID/<int:id>/', MaterialSUministroDetalleID.as_view(),name="MaterialSUministroDetalleID"),
    path('MaterialLimpiezaDetalleID/<int:id>/', MaterialLimpiezaDetalleID.as_view(),name="MaterialLimpiezaDetalleID"),
    path('PapeleriaDetalleID/<int:id>/', PapeleriaDetalleID.as_view(),name="PapeleriaDetalleID"),
    path('MedicamentosInsumosDetalleID/<int:id>/', MedicamentosInsumosDetalleID.as_view(),name="MedicamentosInsumosDetalleID"),
    path('MaterialElectricoVariosDetalleID/<int:id>/', MaterialElectricoVariosDetalleID.as_view(),name="MaterialElectricoVariosDetalleID"),
    path('ReactivosInsumosLaboratorioDetalleID/<int:id>/', ReactivosInsumosLaboratorioDetalleID.as_view(),name="ReactivosInsumosLaboratorioDetalleID"),
    path('InsumosMaterialOdontologiaDetalleID/<int:id>/', InsumosMaterialOdontologiaDetalleID.as_view(),name="InsumosMaterialOdontologiaDetalleID"),

    path('ActivoFijoDetalleID/<int:id>/', ActivoFijoDetalleID.as_view(),name="ActivoFijoDetalleID"),
    path('EquipoMedicoDetalleID/<int:id>/', EquipoMedicoDetalleID.as_view(),name="EquipoMedicoDetalleID"),

    path('ListaIndicadoresEficiencia/', ListaIndicadoresEficiencia.as_view(),name="ListaIndicadoresEficiencia"),
    path('IndicadoresEficienciaBusca/<str:descripcion>/', IndicadoresEficienciaBusca.as_view(),name="IndicadoresEficienciaBusca"),
    path('GuardarIndicadoresEficiencia/', GuardarIndicadoresEficiencia.as_view(),name="GuardarIndicadoresEficiencia"),

    path('ListaMediosVerificacionSelect/', ListaMediosVerificacionSelect.as_view(),name="ListaMediosVerificacionSelect"),
    path('MediosVerificacionSelectBusca/<str:descripcion>/', MediosVerificacionSelectBusca.as_view(),name="MediosVerificacionSelectBusca"),
    path('GuardarMediosVerificacionSelect/', GuardarMediosVerificacionSelect.as_view(),name="GuardarMediosVerificacionSelect"),
    path('ProfesionesSelectBusca/<str:descripcion>/', ProfesionesSelectBusca.as_view(),name="ProfesionesSelectBusca"),

]