from django.db import models

#------------PARA FORMULARIO 6 RRHH-----------#
class Profesiones(models.Model):
    profesion=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("RH- Profesiones u Oficios")

#------------PARA FORMULARIO 7 SNP-----------#

class Modalidades(models.Model):  
    detalle=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Modalidades")

class TipoContratacion(models.Model):
    detalle=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Tipos de Contrataciones")


class ServiciosBasicos(models.Model):
    codigoUnico=models.CharField(max_length=500,default="S/C")
    descripcion=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Servicios Basicos")

class MedicosEspecialistas(models.Model):
    codigoUnico=models.CharField(max_length=500,default="S/C")
    descripcion=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Medicos Especialistas")
    
class AtencionMedicoQuirurgicas(models.Model):
    codigoUnico=models.CharField(max_length=500,default="S/C")
    descripcion=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Atencion Medico Quirurgicas")

class ServiciosMedicosHospitalarios(models.Model):
    codigoUnico=models.CharField(max_length=500,default="S/C")
    descripcion=models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("SNP- Servicios Medicos Hospitalarios")

#------------PARA FORMULARIO 8 MS-----------#

class Material_y_Suministro(models.Model):
    id = models.IntegerField(primary_key=True) 
    TIPO_CHOICES= (
    ('MaterialEscritorio','MaterialEscritorio'),
    ('MaterialLimpieza', 'MaterialLimpieza'),
    ('Papeleria', 'Papeleria'),
    ('MedicamentosInsumos', 'MedicamentosInsumos'),
    ('MaterialElectricoVarios', 'MaterialElectricoVarios'),
    ('ReactivosInsumosLaboratorio', 'ReactivosInsumosLaboratorio'),
    ('InsumosMaterialOdontologia', 'InsumosMaterialOdontologia'),
    )
    Tipo = models.CharField(max_length=500, choices=TIPO_CHOICES, default='Otro')
    CodigoPrioridad = models.IntegerField()
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name_plural = ("MS- Materiales y Suministros en General")

class MaterialEscritorio(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Material Escritorio")

class MaterialLimpieza(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Material Limpieza")       

class Papeleria(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Papeleria") 

class MedicamentosInsumos(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Medicamentos e Suministros")

class MaterialElectricoVarios(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Material Electrico Varios") 

class ReactivosInsumosLaboratorio(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300)
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Reactivos Insumos Laboratorio") 

class InsumosMaterialOdontologia(models.Model):
    codigoUnico=models.CharField(max_length=200,default="S/C")
    nombre_generico=models.CharField(max_length=300,default="")
    unidad=models.CharField(max_length=100)
    precioUnitario = models.DecimalField(blank=True,null=True,max_digits=10, decimal_places=2)
    idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("MS - Insumos Material Odontologia")

# ------------PARA FORMULARIO 9 AF-----------#
# idMaterialSuministro=models.ForeignKey(Material_y_Suministro,on_delete=models.CASCADE)

class Activo_y_Equipo(models.Model):
    id = models.IntegerField(primary_key=True) 
    TIPO_CHOICES= (
    ('ActivoFijo','ActivoFijo'),
    ('EquipoMedico', 'EquipoMedico'),
    )
    Tipo = models.CharField(max_length=500, choices=TIPO_CHOICES, default='Otro')
    CodigoPrioridad = models.IntegerField()
    def __str__(self):
        return str(self.id)
    class Meta:
        verbose_name_plural = ("AE- Activos Fijos y Equipos Medicos")

class Activo_Fijo(models.Model):
    codigoUnico=models.CharField(max_length=500,default="S/C")
    nombre_generico=models.CharField(max_length=500)
    idActivoEquipo=models.ForeignKey(Activo_y_Equipo,on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("AE - Activos Fijos")

class Equipo_Medico(models.Model):
    codigoUnico = models.CharField(max_length=500,default="S/C")
    nombre_generico = models.CharField(max_length=500)
    idActivoEquipo=models.ForeignKey(Activo_y_Equipo,on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("AE - Equipos Medicos")

# ------------PARA FORMULARIO 4 Objetivos de gestions-----------#
''' Lista de Indicadores de Eficiencia'''
class IndicadoresEficiencia(models.Model):
    Descripcion = models.CharField(max_length=200)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OG - Indicadores de Eficiencia")

# ------------PARA FORMULARIO 5 OPERACIONES Y ACTIVIDADES-----------#
''' Lista de Indicadores de Eficiencia'''
class MediosVerificacionSelect(models.Model):
    Descripcion = models.CharField(max_length=300)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("OA - Lista de medios de verificación")