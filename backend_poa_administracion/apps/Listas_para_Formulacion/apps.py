from django.apps import AppConfig


class ListasParaFormulacionConfig(AppConfig):
    name = 'Listas_para_Formulacion'
