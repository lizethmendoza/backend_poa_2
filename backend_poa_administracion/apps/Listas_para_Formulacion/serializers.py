from rest_framework import serializers

from .models import Profesiones
from .models import Modalidades
from .models import TipoContratacion

from .models import  ServiciosBasicos
from .models import  MedicosEspecialistas
from .models import  AtencionMedicoQuirurgicas
from .models import  ServiciosMedicosHospitalarios

from .models import  Material_y_Suministro
from .models import  MaterialEscritorio
from .models import  MaterialLimpieza
from .models import  Papeleria
from .models import  MedicamentosInsumos
from .models import  MaterialElectricoVarios
from .models import  ReactivosInsumosLaboratorio
from .models import  InsumosMaterialOdontologia

from .models import  Activo_y_Equipo
from .models import  Activo_Fijo
from .models import  Equipo_Medico
from .models import IndicadoresEficiencia
from .models import MediosVerificacionSelect


class ProfesionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profesiones
        fields = '__all__'

class ModalidadesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modalidades
        fields = '__all__'

class TipoContratacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoContratacion
        fields = '__all__'


class Material_y_SuministroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material_y_Suministro
        fields = '__all__'

class MaterialEscritorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaterialEscritorio
        fields = '__all__'
    
class MaterialLimpiezaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaterialLimpieza
        fields = '__all__'
class PapeleriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Papeleria
        fields = '__all__'

class MedicamentosInsumosSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicamentosInsumos
        fields = '__all__'

class MaterialElectricoVariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaterialElectricoVarios
        fields = '__all__'

class ReactivosInsumosLaboratorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReactivosInsumosLaboratorio
        fields = '__all__'

class InsumosMaterialOdontologiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = InsumosMaterialOdontologia
        fields = '__all__'

class ServiciosBasicosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiciosBasicos
        fields = '__all__'

class MedicosEspecialistasSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialistas
        fields = '__all__'

class AtencionMedicoQuirurgicasSerializer(serializers.ModelSerializer):
    class Meta:
        model = AtencionMedicoQuirurgicas
        fields = '__all__'

class ServiciosMedicosHospitalariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiciosMedicosHospitalarios
        fields = '__all__'

class Activo_y_EquipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activo_y_Equipo
        fields = '__all__'
        
class Activo_FijoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activo_Fijo
        fields = '__all__'

class Equipo_MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipo_Medico
        fields = '__all__'

class IndicadoresEficienciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndicadoresEficiencia
        fields = '__all__'  

class MediosVerificacionSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediosVerificacionSelect
        fields = '__all__'              
        