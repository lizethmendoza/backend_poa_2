from django.contrib import admin
from .models import  PersonalRevisor
from .models import  PersonalAprobacion
from .models import  RevisadoPor
from .models import  AprobadoPor
from .models import  RevisadoPorGeneral
from .models import  AprobadoPorGeneral
from django.utils.translation import ugettext_lazy as _
# Register your models here.

@admin.register(PersonalRevisor)
class PersonalRevisorAdmin(admin.ModelAdmin):
    list_display = ["AbreviacionProf","Nombre", "Apellidos", "Cargo","Estado"]

@admin.register(PersonalAprobacion)
class PersonalAprobacionAdmin(admin.ModelAdmin):
    list_display = ["AbreviacionProf","Nombre", "Apellidos", "Cargo","Estado"]

@admin.register(RevisadoPor)
class RevisadoPorAdmin(admin.ModelAdmin):
    list_display = ["formulario","personalR", "Gestion", "FechaElaboracion"]

@admin.register(AprobadoPor)
class AprobadoPorAdmin(admin.ModelAdmin):
    list_display = ["formulario","personalA", "Gestion", "FechaElaboracion"]

@admin.register(RevisadoPorGeneral)
class RevisadoPorGeneralAdmin(admin.ModelAdmin):
    list_display = ["formulario","personalR", "Gestion", "FechaElaboracion"]

@admin.register(AprobadoPorGeneral)
class AprobadoPorGeneralAdmin(admin.ModelAdmin):
    list_display = ["formulario","personalA", "Gestion", "FechaElaboracion"]