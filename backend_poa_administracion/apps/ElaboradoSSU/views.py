from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from .models import PersonalRevisor
from .serializers import PersonalRevisorSerializer

from .models import PersonalAprobacion
from .serializers import PersonalAprobacionSerializer

from .models import RevisadoPor
from .serializers import RevisadoPorSerializer
from .serializers import RevisadoPorSerializer_1

from .models import AprobadoPor
from .serializers import AprobadoPorSerializer
from .serializers import AprobadoPorSerializer_1

from .models import RevisadoPorGeneral
from .serializers import RevisadoPorGeneralSerializer
from .serializers import RevisadoPorGeneralSerializer_1

from .models import AprobadoPorGeneral
from .serializers import AprobadoPorGeneralSerializer
from .serializers import AprobadoPorGeneralSerializer_1


''' Lista de personal de revision'''
class ListaRevisadoPor(APIView):
    def get(self, request):
        datos = PersonalRevisor.objects.all().order_by('prioridadM')
        data = PersonalRevisorSerializer(datos, many=True).data
        return Response(data)

''' ACTUALIZAR ESTADO DEL PERSONAL REVISOR'''
class actualizarEstadoRevisado(APIView):
    def get_object(self, pk):
        try:
            return PersonalRevisor.objects.get(pk=pk)
        except PersonalRevisor.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalRevisorSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalRevisorSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' GUARDA Personal Revision'''
class GuardarPRevisor(APIView):
    def post(self, request, format=None):
        serializer = PersonalRevisorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SiPersonalActivoRevision(APIView):
    def get(self, request,estado,format=None):
        Formulario = PersonalRevisor.objects.all().filter(Estado=estado).order_by('prioridadM')
        serializer = PersonalRevisorSerializer(Formulario, many=True)
        return Response(serializer.data)

#-------------------------------------------------------------------

''' Lista de personal de Aprobacion'''
class ListaAprobadoPor(APIView):
    def get(self, request):
        datos = PersonalAprobacion.objects.all().order_by('id')
        data = PersonalAprobacionSerializer(datos, many=True).data
        return Response(data)

''' ACTUALIZAR ESTADO DEL PERSONAL QUE APRUEBA'''
class actualizarEstadoAprobado(APIView):
    def get_object(self, pk):
        try:
            return PersonalAprobacion.objects.get(pk=pk)
        except PersonalAprobacion.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalAprobacionSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalAprobacionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' GUARDA Personal Aprobacion'''
class GuardarPAprobacion(APIView):
    def post(self, request, format=None):
        serializer = PersonalAprobacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SiPersonalActivoAprobacion(APIView):
    def get(self, request,estado,format=None):
        Formulario = PersonalAprobacion.objects.all().filter(Estado=estado)
        serializer = PersonalAprobacionSerializer(Formulario, many=True)
        return Response(serializer.data)

#------------------------------------------
''' Relacion de n a m para guardar los formularios revision'''
class GuardarPersonalRevision(APIView):
    def post(self, request, format=None):
        serializer = RevisadoPorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista el registro de un formulario'''
class ListarPersonalxUnidad(APIView):
    def get(self, request,pk,format=None):
        Formulario = RevisadoPor.objects.all().filter(formulario=pk).order_by('personalR__prioridadM')
        serializer = RevisadoPorSerializer_1(Formulario, many=True)
        return Response(serializer.data)

#--------------------------------------------------------

''' Relacion de n a m para guardar los formularios aprobacion'''
class GuardarPersonalAprobacion(APIView):
    def post(self, request, format=None):
        serializer = AprobadoPorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListarPersonalxUnidad_1(APIView):
    def get(self, request,pk,format=None):
        Formulario = AprobadoPor.objects.all().filter(formulario=pk)
        serializer = AprobadoPorSerializer_1(Formulario, many=True)
        return Response(serializer.data)
#-------------------------------------------------------------------------------
#RESPONSABLES PARA FORMULARIOS GENERALES
class GuardarPersonalRevicionGeneral(APIView):
    def post(self, request, format=None):
        serializer = RevisadoPorGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListarPersonalxUnidadG(APIView):
    def get(self, request,pk,format=None):
        Formulario = RevisadoPorGeneral.objects.all().filter(formulario=pk).order_by('personalR__prioridadM')
        serializer = RevisadoPorGeneralSerializer_1(Formulario, many=True)
        return Response(serializer.data)
#---------------------------------------------------------------------------------------------------------------------------
class GuardarPersonalAprobacionGeneral(APIView):
    def post(self, request, format=None):
        serializer = AprobadoPorGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListarPersonalxUnidad_1G(APIView):
    def get(self, request,pk,format=None):
        Formulario = AprobadoPorGeneral.objects.all().filter(formulario=pk)
        serializer = AprobadoPorGeneralSerializer_1(Formulario, many=True)
        return Response(serializer.data)
