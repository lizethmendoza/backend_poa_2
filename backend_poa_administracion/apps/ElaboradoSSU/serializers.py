from rest_framework import serializers
from .models import PersonalRevisor
from .models import PersonalAprobacion
from .models import RevisadoPor
from .models import AprobadoPor
from .models import RevisadoPorGeneral
from .models import AprobadoPorGeneral

class PersonalRevisorSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalRevisor
        fields = '__all__'

class PersonalAprobacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalAprobacion
        fields = '__all__'

class RevisadoPorSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevisadoPor
        fields = '__all__'

class RevisadoPorSerializer_1(serializers.ModelSerializer):
    personalR=PersonalRevisorSerializer(read_only=True)
    class Meta:
        model = RevisadoPor
        fields = '__all__'

class AprobadoPorSerializer(serializers.ModelSerializer):
    class Meta:
        model = AprobadoPor
        fields = '__all__'

class AprobadoPorSerializer_1(serializers.ModelSerializer):
    personalA=PersonalAprobacionSerializer(read_only=True)
    class Meta:
        model = AprobadoPor
        fields = '__all__'

class RevisadoPorGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevisadoPorGeneral
        fields = '__all__'

class RevisadoPorGeneralSerializer_1(serializers.ModelSerializer):
    personalR=PersonalRevisorSerializer(read_only=True)
    class Meta:
        model = RevisadoPorGeneral
        fields = '__all__'

class AprobadoPorGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = AprobadoPorGeneral
        fields = '__all__'

class AprobadoPorGeneralSerializer_1(serializers.ModelSerializer):
    personalA=PersonalAprobacionSerializer(read_only=True)
    class Meta:
        model = AprobadoPorGeneral
        fields = '__all__'







