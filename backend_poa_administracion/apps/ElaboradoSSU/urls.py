from django.urls import path
from .views import ListaRevisadoPor
from .views import actualizarEstadoRevisado
from .views import GuardarPRevisor
from .views import SiPersonalActivoRevision

from .views import ListaAprobadoPor
from .views import actualizarEstadoAprobado
from .views import GuardarPAprobacion
from .views import SiPersonalActivoAprobacion

from .views import GuardarPersonalRevision
from .views import GuardarPersonalAprobacion
from .views import ListarPersonalxUnidad
from .views import ListarPersonalxUnidad_1
from .views import GuardarPersonalRevicionGeneral
from .views import GuardarPersonalAprobacionGeneral
from .views import ListarPersonalxUnidadG
from .views import ListarPersonalxUnidad_1G


urlpatterns = [
    path('ListaRevisadoPor/', ListaRevisadoPor.as_view(),name="ListaRevisadoPor"),
    path('actualizarEstadoRevisado/<int:pk>/', actualizarEstadoRevisado.as_view(),name="actualizarEstadoRevisado"),  
    path('GuardarPRevisor/', GuardarPRevisor.as_view(),name="GuardarPRevisor"),
    path('SiPersonalActivoRevision/<str:estado>/', SiPersonalActivoRevision.as_view(),name="SiPersonalActivoRevision"),
    path('ListaAprobadoPor/', ListaAprobadoPor.as_view(),name="ListaAprobadoPor"),
    path('actualizarEstadoAprobado/<int:pk>/', actualizarEstadoAprobado.as_view(),name="actualizarEstadoAprobado"),
    path('SiPersonalActivoAprobacion/<str:estado>/', SiPersonalActivoAprobacion.as_view(),name="SiPersonalActivoAprobacion"),
    path('GuardarPAprobacion/', GuardarPAprobacion.as_view(),name="GuardarPAprobacion"),
    path('GuardarPersonalRevision/', GuardarPersonalRevision.as_view(),name="GuardarPersonalRevision"),
    path('GuardarPersonalAprobacion/', GuardarPersonalAprobacion.as_view(),name="GuardarPersonalAprobacion"),
    path('ListarPersonalxUnidad/<int:pk>/', ListarPersonalxUnidad.as_view(),name="ListarPersonalxUnidad"),  
    path('ListarPersonalxUnidad_1/<int:pk>/', ListarPersonalxUnidad_1.as_view(),name="ListarPersonalxUnidad_1"),
    path('GuardarPersonalRevicionGeneral/', GuardarPersonalRevicionGeneral.as_view(),name="GuardarPersonalRevicionGeneral"),
    path('GuardarPersonalAprobacionGeneral/', GuardarPersonalAprobacionGeneral.as_view(),name="GuardarPersonalAprobacionGeneral"),
    path('ListarPersonalxUnidadG/<int:pk>/', ListarPersonalxUnidadG.as_view(),name="ListarPersonalxUnidadG"),  
    path('ListarPersonalxUnidad_1G/<int:pk>/', ListarPersonalxUnidad_1G.as_view(),name="ListarPersonalxUnidad_1G"),  
]
