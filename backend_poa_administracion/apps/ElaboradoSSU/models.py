from django.db import models
from ..Formulacion.models import formularios
from ..POA_General.models import FormulariosGenerales
# # Create your models here.

class PersonalRevisor(models.Model):
    AbreviacionProf= models.CharField(max_length=30)
    Nombre = models.CharField(max_length=50)
    Apellidos=models.CharField(max_length=200)
    Cargo=models.CharField(max_length=100)
    Estado_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='',blank=True)
    prioridadM=models.IntegerField()
    def __str__(self):
        return str(self.pk)
    class Meta:
        ordering = ('Apellidos')
    class Meta:
        verbose_name_plural = ("Personal Revisor - SSU")

class PersonalAprobacion(models.Model):
    AbreviacionProf= models.CharField(max_length=30)
    Nombre = models.CharField(max_length=50)
    Apellidos=models.CharField(max_length=200)
    Cargo=models.CharField(max_length=100)
    Estado_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='',blank=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        ordering = ('Apellidos')
    class Meta:
        verbose_name_plural = ("Personal Aprobacion - SSU")

#tabla para personal revisado n:m    
class RevisadoPor(models.Model):
    formulario=models.ForeignKey(formularios,on_delete=models.CASCADE)
    personalR=models.ForeignKey(PersonalRevisor,on_delete=models.CASCADE)
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("FN-Revisado por - Formulario")

# tabla para personal aprobado n:m 
class AprobadoPor(models.Model):
    formulario=models.ForeignKey(formularios,on_delete=models.CASCADE)
    personalA=models.ForeignKey(PersonalAprobacion,on_delete=models.CASCADE)
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("FN-Aprobado por - Formulario")

#tabla para personal revisado n:m GENERAL   
class RevisadoPorGeneral(models.Model):
    formulario=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    personalR=models.ForeignKey(PersonalRevisor,on_delete=models.CASCADE)
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("FG-Revisado por - Formulario General")

# tabla para personal aprobado n:m GENERAL
class AprobadoPorGeneral(models.Model):
    formulario=models.ForeignKey(FormulariosGenerales,on_delete=models.CASCADE)
    personalA=models.ForeignKey(PersonalAprobacion,on_delete=models.CASCADE)
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("FG-Aprobado por - Formulario General")