from rest_framework import serializers
from .models import SeguimientoOperacion
from ..responsablesInformacion.serializers import UnidadSerializer
from ..periodicidad.serializers import PeriodoSerializerPeriodicidad
from ..Formulacion.serializers import operacionesFuncionamientoSerializer
from ..Poai.serializers import POAiSerializer

from .models import AvanceEjecutadoOperaciones

class SeguimientoOperacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeguimientoOperacion
        fields = '__all__'
        
class SeguimientoOperacionUnidadSerializer(serializers.ModelSerializer):
    Unidad=UnidadSerializer(read_only=True)
    class Meta:
        model = SeguimientoOperacion
        fields = '__all__'

class SeguimientoOperacionPeriodoSerializer(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = SeguimientoOperacion
        fields = '__all__'

class AvanceEjecutadoOperacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvanceEjecutadoOperaciones
        fields = '__all__'

class AvanceEjecutadoOperacionesSerializer_1(serializers.ModelSerializer):
    idOperacion=operacionesFuncionamientoSerializer(read_only=True)
    idSeguimiento=SeguimientoOperacionUnidadSerializer(read_only=True)
    class Meta:
        model = AvanceEjecutadoOperaciones
        fields = '__all__'

class SeguimientoOperacionPeriodoSerializer_2(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idPOAi=POAiSerializer(read_only=True)
    class Meta:
        model = SeguimientoOperacion
        fields = '__all__'