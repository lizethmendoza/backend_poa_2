from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import SeguimientoOperacion
from .models import AvanceEjecutadoOperaciones

# Register your models here.
@admin.register(SeguimientoOperacion)
class SeguimientoOperacionAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","Estado","Unidad","periodo","idPOAi"]

@admin.register(AvanceEjecutadoOperaciones)
class AvanceEjecutadoOperacionesAdmin(admin.ModelAdmin):
    list_display = ["pk", "CantidadEjecutado", "PonderacionPorc","Observacion","idOperacion","idSeguimiento"]