from django.db import models
from ..responsablesInformacion.models import Unidad
from .. periodicidad.models import Periodo
from ..Formulacion.models import operacionesFuncionamiento
from ..Poai.models import POAi

# Create your models here.
class SeguimientoOperacion(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
        ('Proceso seguimiento', 'Proceso seguimiento'),
        ('Proceso Revision', 'Proceso Revision'),
        ('Observado', 'Observado'),
        ('Aprobado PROMES', 'Aprobado PROMES'),
        ('Vigente', 'Vigente'),
        ('Aprobado SSU', 'Aprobado SSU'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso seguimiento')
    Unidad=models.ForeignKey(Unidad,on_delete=models.CASCADE)
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    idPOAi=models.ForeignKey(POAi,on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de seguimientos Acciones-Operaciones")

class AvanceEjecutadoOperaciones(models.Model):
    CantidadEjecutado = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    PonderacionPorc=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    Observacion=models.TextField(max_length=500,null=True, blank=True)
    idOperacion= models.ForeignKey(operacionesFuncionamiento,on_delete=models.CASCADE)
    idSeguimiento = models.ForeignKey(SeguimientoOperacion,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Avence - Operciones de Funcionamiento")