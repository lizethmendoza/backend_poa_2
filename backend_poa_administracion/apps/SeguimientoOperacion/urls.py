from django.urls import path
from .views import ListarSeguimientoOperacion
from .views import GuardarSeguimientoOperacion
from .views import ListarSeguimientoOperacionesE
from .views import ListarSeguimientosOperacionUnidad
from .views import ListaFormulariosPOAi_OF_S
from .views import GuardarSeguimientoOperacionAvance
from .views import actualizarSeguimientoOperacion
from .views import ListarSeguimientoAcciones
from .views import ListarSeguimientoGeneralPeriodo
from .views import ListarSeguimientoOperacionPeriodoUnidad
from .views import actualizarAvanceOperacion
from .views import ListaSeguimientoEstadoPeriodoOPE

urlpatterns = [
    path('ListarSeguimientoOperacion/', ListarSeguimientoOperacion.as_view(),name="ListarSeguimientoOperacion"),
    path('GuardarSeguimientoOperacion/', GuardarSeguimientoOperacion.as_view(),name="GuardarSeguimientoOperacion"),
    path('ListarSeguimientoOperacionesE/<int:gestion>/<int:idUnidad>/<int:periodo>/<int:idPOA>/', ListarSeguimientoOperacionesE.as_view(),name="ListarSeguimientoOperacionesE"),
    path('ListarSeguimientosOperacionUnidad/<int:idUnidad>/', ListarSeguimientosOperacionUnidad.as_view(),name="ListarSeguimientosOperacionUnidad"),
    path('ListaFormulariosPOAi_OF_S/<int:gestion>/<int:idUnidad>/<int:idPOA>/', ListaFormulariosPOAi_OF_S.as_view(),name="ListaFormulariosPOAi_OF_S"),
    path('GuardarSeguimientoOperacionAvance/', GuardarSeguimientoOperacionAvance.as_view(),name="GuardarSeguimientoOperacionAvance"),
    path('actualizarSeguimientoOperacion/<int:pk>/', actualizarSeguimientoOperacion.as_view(),name="actualizarSeguimientoOperacion"),
    path('ListarSeguimientoAcciones/<int:idSeg>/', ListarSeguimientoAcciones.as_view(),name="ListarSeguimientoAcciones"),
    path('ListarSeguimientoGeneralPeriodo/<int:gestion>/<str:periodo>/', ListarSeguimientoGeneralPeriodo.as_view(),name="ListarSeguimientoGeneralPeriodo"),
    path('ListarSeguimientoOperacionPeriodoUnidad/<int:gestion>/<str:periodo>/<int:pkUnidad>/', ListarSeguimientoOperacionPeriodoUnidad.as_view(),name="ListarSeguimientoOperacionPeriodoUnidad"),
    path('actualizarAvanceOperacion/<int:pk>/', actualizarAvanceOperacion.as_view(),name="actualizarAvanceOperacion"),
    path('ListaSeguimientoEstadoPeriodoOPE/<int:gestion>/<int:periodo>/', ListaSeguimientoEstadoPeriodoOPE.as_view(),name="ListaSeguimientoEstadoPeriodoOPE"),
    
    # path('actualizarSeguimiento/<int:pk>/', actualizarSeguimiento.as_view(),name="actualizarSeguimiento"),
    # path('ListadeAvancePeriodoIdSeguimiento/<int:idSeg>/', ListadeAvancePeriodoIdSeguimiento.as_view(),name="ListadeAvancePeriodoIdSeguimiento"),
    # path('actualizarAvance/<int:pk>/', actualizarAvance.as_view(),name="actualizarAvance"),
    # path('ListaMedioVerificacionId/<int:id>/', ListaMedioVerificacionId.as_view(),name="ListaMedioVerificacionId"),
    # path('actualizarMedioVerificacionFile/<int:pk>/', actualizarMedioVerificacionFile.as_view(),name="actualizarMedioVerificacionFile"),
    # path('actualizarAvance_1/<int:pk>/', actualizarAvance_1.as_view(),name="actualizarAvance_1"),
    # path('SiexisteSeguimientoGestion/<int:gestion>/<int:unidad>/', SiexisteSeguimientoGestion.as_view(),name="SiexisteSeguimientoGestion"),
]