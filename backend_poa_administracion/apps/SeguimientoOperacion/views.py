from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser

from .models import SeguimientoOperacion
from .serializers import SeguimientoOperacionSerializer
from ..Formulacion.models import formularios
from ..Formulacion.serializers import operacionesFuncionamiento
from ..Formulacion.serializers import operacionesFuncionamientoSerializer_1
from .serializers import SeguimientoOperacionPeriodoSerializer

from .models import AvanceEjecutadoOperaciones
from .serializers import AvanceEjecutadoOperacionesSerializer
from .serializers import AvanceEjecutadoOperacionesSerializer_1
from .serializers import SeguimientoOperacionPeriodoSerializer_2
# Create your views here.

'''Lista todos los seguimientos'''
class ListarSeguimientoOperacion(APIView):
    def get(self, request):
        datos = SeguimientoOperacion.objects.all().order_by('Gestion')
        data = SeguimientoOperacionSerializer(datos, many=True).data
        return Response(data)

''' Guarda seguimiento'''
class GuardarSeguimientoOperacion(APIView):
    def post(self, request, format=None):
        serializer = SeguimientoOperacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# ''' Lista los seguimientos de operaciones de una determinada gestion, periodo , unidad ANTES DE REFORMULACION'''
# class ListarSeguimientoOperacionesE(APIView):
#    def get(self, request, gestion,idUnidad,periodo,format=None):        
#         datos = SeguimientoOperacion.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(Unidad=idUnidad).order_by('-Gestion')
#         data = SeguimientoOperacionPeriodoSerializer(datos, many=True).data
#         return Response(data)

''' Lista los seguimientos de operaciones de una determinada gestion, periodo , unidad DEPUES DE LA REFORMULACION'''
class ListarSeguimientoOperacionesE(APIView):
   def get(self, request, gestion,idUnidad,periodo,idPOA,format=None):        
        datos = SeguimientoOperacion.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(Unidad=idUnidad).filter(idPOAi=idPOA).order_by('-Gestion')
        data = SeguimientoOperacionPeriodoSerializer(datos, many=True).data
        return Response(data)

# ''' Lista los seguimientos de una determinada gestion, periodo , unidad antes de la reformulacion'''
# class ListarSeguimientosOperacionUnidad(APIView):
#    def get(self, request,idUnidad,format=None):        
#         datos = SeguimientoOperacion.objects.all().filter(Unidad=idUnidad).order_by('-Gestion')
#         data = SeguimientoOperacionPeriodoSerializer(datos, many=True).data
#         return Response(data)

''' Lista los seguimientos de una determinada gestion, periodo , unidad despues de la reformulacion'''
class ListarSeguimientosOperacionUnidad(APIView):
   def get(self, request,idUnidad,format=None):        
        datos = SeguimientoOperacion.objects.all().filter(Unidad=idUnidad).order_by('-Gestion')
        data = SeguimientoOperacionPeriodoSerializer_2(datos, many=True).data
        return Response(data)

''' Operaciones de funcionamiento de una determinada unidad gestion  ANTES DE LA REFORMULACION'''
# class ListaFormulariosPOAi_OF_S(APIView):
#     def get(self, request,gestion,idUnidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Aprobado SSU').filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).filter(POAPertenece__Unidad=idUnidad).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista).order_by('id')
#         serializer = operacionesFuncionamientoSerializer_1(snippets, many=True)
#         return Response(serializer.data)

''' Operaciones de funcionamiento de una determinada unidad gestion  DESPUES DE LA REFORMULACION'''
class ListaFormulariosPOAi_OF_S(APIView):
    def get(self, request,gestion,idUnidad,idPOA,format=None):
        respuesta= formularios.objects.filter(POAPertenece__id = idPOA).filter(tipo='AccionesyOperaciones').filter(Gestion=gestion).filter(POAPertenece__Unidad=idUnidad).order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=operacionesFuncionamiento.objects.all().filter(Formularios__in=lista).order_by('id')
        serializer = operacionesFuncionamientoSerializer_1(snippets, many=True)
        return Response(serializer.data)

''' Guarda Avance de operaciones - acciones'''
class GuardarSeguimientoOperacionAvance(APIView):
    def post(self, request, format=None):
        serializer = AvanceEjecutadoOperacionesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Actualiza un registro de seguimiento de acciones - operacion''' 
class actualizarSeguimientoOperacion(APIView):
    def get_object(self, pk):
        try:
            return SeguimientoOperacion.objects.get(pk=pk)
        except SeguimientoOperacion.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoOperacionSerializer(snippet)
        return Response(serializer.data)
    """actualiza el registro"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoOperacionSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' Lista Seguimiento de acciones y operaciones segun idSeguimiento'''
class ListarSeguimientoAcciones(APIView):
   def get(self, request,idSeg,format=None):        
        datos = AvanceEjecutadoOperaciones.objects.all().filter(idSeguimiento=idSeg)
        data = AvanceEjecutadoOperacionesSerializer_1(datos, many=True).data
        return Response(data)

# '''Lista todos los seguimientos con un estado de proceso de revision, Gestion , periodo ANTES DE LA REFORMULACION'''
# class ListarSeguimientoGeneralPeriodo(APIView):
#     def get(self, request,gestion,periodo,format=None):
#         datos = SeguimientoOperacion.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).order_by('FechaElaboracion')
#         lista=datos.values_list('id',flat=True)
#         snippets=AvanceEjecutadoOperaciones.objects.all().filter(idSeguimiento__in=lista).order_by('id')
#         data = AvanceEjecutadoOperacionesSerializer_1(snippets, many=True).data
#         return Response(data)

'''Lista todos los seguimientos con un estado de proceso de revision, Gestion , periodo DESPUES DE LA REFORMULACION'''
class ListarSeguimientoGeneralPeriodo(APIView):
    def get(self, request,gestion,periodo,format=None):
        datos = SeguimientoOperacion.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        lista=datos.values_list('id',flat=True)
        snippets=AvanceEjecutadoOperaciones.objects.all().filter(idSeguimiento__in=lista).order_by('id')
        data = AvanceEjecutadoOperacionesSerializer_1(snippets, many=True).data
        return Response(data)

'''Lista todos los seguimientos con un estado de proceso de revision, Gestion ,Periodo y Unidad '''
class ListarSeguimientoOperacionPeriodoUnidad(APIView):
    def get(self, request,gestion,periodo,pkUnidad,format=None):
        datos = SeguimientoOperacion.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).filter(Unidad=pkUnidad).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        lista=datos.values_list('id',flat=True)
        snippets=AvanceEjecutadoOperaciones.objects.all().filter(idSeguimiento__in=lista).order_by('id')
        data = AvanceEjecutadoOperacionesSerializer_1(snippets, many=True).data
        return Response(data)

''' Actualiza un registro de avance del seguimiento de acciones - operacion''' 
class actualizarAvanceOperacion(APIView):
    def get_object(self, pk):
        try:
            return AvanceEjecutadoOperaciones.objects.get(pk=pk)
        except AvanceEjecutadoOperaciones.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoOperacionesSerializer(snippet)
        return Response(serializer.data)
    """actualiza el registro"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoOperacionesSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaSeguimientoEstadoPeriodoOPE(APIView):
    def get(self, request,gestion,periodo,format=None):
        datos = SeguimientoOperacion.objects.filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo_id=periodo).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        data = SeguimientoOperacionSerializer(datos, many=True).data
        return Response(data)