from rest_framework import serializers
from .models import AprobacionReformulacion
from ..responsablesInformacion.serializers import PersonalSerializer_1
from ..POA_General.serializers import POAGeneralSerializer

class AprobacionReformulacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = AprobacionReformulacion
        fields = '__all__'
        
#serializador para que te muestre los datos de la llave forea mas
class AprobacionReformulacionPSerializer(serializers.ModelSerializer):
    PersonalResposanble=PersonalSerializer_1(read_only=True)
    class Meta:
        model = AprobacionReformulacion
        fields = '__all__'

class AprobacionReformulacionGSerializer(serializers.ModelSerializer):
    PoaGeneral=POAGeneralSerializer(read_only=True)
    class Meta:
        model = AprobacionReformulacion
        fields = '__all__'