from django.contrib import admin
from .models import AprobacionReformulacion
# Register your models here.

@admin.register(AprobacionReformulacion)
class AprobacionReformulacionAdmin(admin.ModelAdmin):
    list_display = ["pk", "FechaElaboracion", "Descripcion","PoaGeneral","PersonalResposanble","adjunto"]