from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from .models import AprobacionReformulacion
from .serializers import AprobacionReformulacionSerializer
from .serializers import AprobacionReformulacionPSerializer
from .serializers import AprobacionReformulacionGSerializer

'''VISTA PARA GUARDAR EL DETALLE DE LA REFORMULACION'''
class GuardarAprobacionReformulacion(APIView):
    def post(self, request, format=None):
        serializer = AprobacionReformulacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#FILTRO PARA VERIFICAR SI EXISTE YA LA APROBACION PARA LA REFORMULACION DE UN POA 
class ListaAprobacionReformulacion(APIView):
    def get(self, request, idPOA ,format=None):        
        snippets= AprobacionReformulacion.objects.filter(PoaGeneral=idPOA).order_by('id')
        serializer = AprobacionReformulacionPSerializer(snippets, many=True)
        return Response(serializer.data)

#FILTRO PARA VERIFICAR SI EXISTE YA LA APROBACION PARA LA REFORMULACION DE UN POA 
class ListaAprobacionReformulacionGestion(APIView):
    def get(self, request, gestion ,format=None):       
        snippets= AprobacionReformulacion.objects.filter(PoaGeneral__Gestion=gestion).filter(PoaGeneral__Estado_version="Activo").order_by('id')
        serializer = AprobacionReformulacionGSerializer(snippets, many=True)
        return Response(serializer.data)