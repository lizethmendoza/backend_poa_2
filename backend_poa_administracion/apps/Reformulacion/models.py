from django.db import models
from ..responsablesInformacion.models import Personal
from ..POA_General.models import POAGeneral
# Create your models here.

class AprobacionReformulacion(models.Model):
    FechaElaboracion=models.DateField(auto_now=True)
    Descripcion = models.CharField(max_length=500)
    PoaGeneral=models.ForeignKey(POAGeneral,on_delete=models.CASCADE)
    PersonalResposanble=models.ForeignKey(Personal,on_delete=models.CASCADE)
    adjunto=models.FileField(blank=True, null=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Detalle Aprobacion Reformulacion")
