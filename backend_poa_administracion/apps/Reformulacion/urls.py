from django.urls import path
from .views import GuardarAprobacionReformulacion
from .views import ListaAprobacionReformulacion
from .views import ListaAprobacionReformulacionGestion
urlpatterns = [
    path('GuardarAprobacionReformulacion/', GuardarAprobacionReformulacion.as_view(),name="DatosPOAi"),
    path('ListaAprobacionReformulacion/<int:idPOA>/', ListaAprobacionReformulacion.as_view(),name="ListaAprobacionReformulacion"),
    path('ListaAprobacionReformulacionGestion/<int:gestion>/', ListaAprobacionReformulacionGestion.as_view(),name="ListaAprobacionReformulacionGestion"),
]