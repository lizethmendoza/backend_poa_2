from django.urls import path
from .views import ListaParametrosEvaluacion
from .views import ListaPorcentajesParametro
from .views import GuardarParametrosEvaluacion
from .views import GuardarPorcentajeParametro
from .views import ListaPorcentajesParametroId
from .views import ListaPorcentajesParametroActivo
from .views import ListaPorcentajesParametroId_1
from .views import actualizarParametro
from .views import actualizarCriteriosParametros
from .views import ListaPorcentajesParametroPk
# from .views import actualizarPeriodicidadGestion
# from .views import ListaPeriodicidadGestion
# from .views import VerificaPeriodicidadGestionHab

urlpatterns = [
    path('ListaParametrosEvaluacion/', ListaParametrosEvaluacion.as_view(),name="ListaParametrosEvaluacion"),
    path('ListaPorcentajesParametro/', ListaPorcentajesParametro.as_view(),name="ListaPorcentajesParametro"),
    path('GuardarParametrosEvaluacion/', GuardarParametrosEvaluacion.as_view(),name="GuardarParametrosEvaluacion"),
    path('GuardarPorcentajeParametro/', GuardarPorcentajeParametro.as_view(),name="GuardarPorcentajeParametro"),
    path('ListaPorcentajesParametroId/<int:idP>/', ListaPorcentajesParametroId.as_view(),name="ListaPorcentajesParametroId"),
    path('ListaPorcentajesParametroActivo/', ListaPorcentajesParametroActivo.as_view(),name="ListaPorcentajesParametroActivo"),
    path('ListaPorcentajesParametroId_1/<int:idP>/', ListaPorcentajesParametroId_1.as_view(),name="ListaPorcentajesParametroId_1"),
    path('actualizarParametro/<int:pk>/', actualizarParametro.as_view(),name="actualizarParametro"),
    path('actualizarCriteriosParametros/<int:pk>/', actualizarCriteriosParametros.as_view(),name="actualizarCriteriosParametros"),
    path('ListaPorcentajesParametroPk/<int:idP>/', ListaPorcentajesParametroPk.as_view(),name="ListaPorcentajesParametroPk"),
    # path('actualizarPeriodicidadGestion/<int:pk>/', actualizarPeriodicidadGestion.as_view(),name="actualizarPeriodicidadGestion"),
    # path('ListaPeriodicidadGestion/', ListaPeriodicidadGestion.as_view(),name="ListaPeriodicidadGestion"),
    # path('VerificaPeriodicidadGestionHab/<int:gestion>/', VerificaPeriodicidadGestionHab.as_view(),name="VerificaPeriodicidadGestionHab"),
]