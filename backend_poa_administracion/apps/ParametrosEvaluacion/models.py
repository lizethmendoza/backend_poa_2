from django.db import models

# Create your models here.

class ParametrosEvaluacion(models.Model):
    FechaElaboracion=models.DateField(auto_now=True)
    Estado_CHOICES= (
    ('Vigente', 'Vigente'),
    ('Expirado', 'Expirado')
    )
    Estado = models.CharField(max_length=50, choices=Estado_CHOICES, default='Expirado')
    PorcentajeAl = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Parametros de Evaluvación")

class PorcentajeParametro(models.Model):
    RangoInicio = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    RangoFin = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    Descripcion = models.CharField(max_length=100)
    Secuencia=models.IntegerField()
    idParametroEvaluacion = models.ForeignKey(ParametrosEvaluacion,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Porcentajes de Parametro")