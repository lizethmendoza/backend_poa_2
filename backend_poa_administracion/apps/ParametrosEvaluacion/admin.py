from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin
from .models import ParametrosEvaluacion
from .models import PorcentajeParametro

# Register your models here.

@admin.register(ParametrosEvaluacion)
class ParametrosEvaluacionAdmin(admin.ModelAdmin):
    list_display = ["pk","FechaElaboracion","Estado","PorcentajeAl"]

@admin.register(PorcentajeParametro)
class PorcentajeParametroAdmin(ImportExportModelAdmin):
    list_display = ["pk", "RangoInicio", "RangoFin","Descripcion","idParametroEvaluacion","Secuencia"]

