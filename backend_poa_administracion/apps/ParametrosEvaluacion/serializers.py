from rest_framework import serializers
from .models import ParametrosEvaluacion
from .models import PorcentajeParametro

class ParametrosEvaluacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParametrosEvaluacion
        fields = '__all__'

class PorcentajeParametroSerializer(serializers.ModelSerializer):
    class Meta:
        model = PorcentajeParametro
        fields = '__all__'

class PorcentajeParametroSerializer_1(serializers.ModelSerializer):
    idParametroEvaluacion = ParametrosEvaluacionSerializer(read_only=True)
    class Meta:
        model = PorcentajeParametro
        fields = '__all__'
