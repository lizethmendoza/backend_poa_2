from django.apps import AppConfig


class ParametrosevaluacionConfig(AppConfig):
    name = 'ParametrosEvaluacion'
