from django.http import HttpResponseRedirect
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer, UserSerializerWithToken


@api_view(['GET'])
def current_user(request):
    """
    Determine el usuario actual por su token y devuelva sus datos
    """

    serializer = UserSerializer(request.user)
    return Response(serializer.data)


class UserList(APIView):
    """
    Crea un nuevo usuario. Se llama 'UserList'
    porque normalmente tendríamos un get
    Método aquí también, para recuperar una lista de todos los objetos de Usuario.
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaUsuarios(APIView):
    def get(self, request, usr, format=None):
        Formulario = User.objects.all().filter(username=usr)
        serializer = UserSerializer(Formulario, many=True)
        return Response(serializer.data)