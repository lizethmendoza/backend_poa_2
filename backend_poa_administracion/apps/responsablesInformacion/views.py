from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from tablib import Dataset
# Create your views here.

from .serializers import UnidadSerializer
from .serializers import CargoSerializer
from .serializers import PersonalSerializer
from .serializers import personal_Cargo_UnidadSerializer
from .serializers import PersonalSerializer_1

from .models import Unidad
from .models import Cargo
from .models import Personal

'''Lista a todo el personal del PROMES'''
class ListaPersonal(APIView):
    def get(self, request,format=None):
        personal = Personal.objects.all()
        data = personal_Cargo_UnidadSerializer(personal, many=True).data
        return Response(data)

''' Lista todos los cargos que existen el el PROMES'''
class ListaCargos(APIView):
    def get(self, request,format=None):
        Cargos = Cargo.objects.all()
        data = CargoSerializer(Cargos, many=True).data
        return Response(data)

''' Vista para guardar cargos desde el frontend'''
class GuardarCargos(APIView):
    def post(self, request, format=None):
        serializer = CargoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista de unidades dentro del PROMES'''
class ListaUnidades(APIView):
    def get(self, request,format=None):
        unidad = Unidad.objects.all()
        data = UnidadSerializer(unidad, many=True).data
        return Response(data)

''' Despliega la unidad con el id igual al codigo de unidad'''
class ListaUnidadesID(APIView):
    def get(self, request, id, format=None):
        snippets= Unidad.objects.filter(cod_unidad=id)
        serializer = UnidadSerializer(snippets, many=True)
        return Response(serializer.data)

#Vistas para el login
''' Devuelve los datos del personal del usuario que llega como paramentro de usr'''
class ListaPersonalusuario(APIView):
    def get(self, request, usr, format=None):
        Formulario = Personal.objects.all().filter(usuario__username=usr)
        serializer = personal_Cargo_UnidadSerializer(Formulario, many=True)
        return Response(serializer.data)

''' Lista los usuarios responsables de cada unidad y un estado de activo'''
class ListaPersonalusuario_1(APIView):
    def get(self, request, format=None):
        Formulario = Personal.objects.all().filter(responsable="True").filter(estado="Activo")
        serializer = PersonalSerializer(Formulario, many=True)
        return Response(serializer.data)

''' Lista al personal con el ci=pk (parametro que llega)'''
class ListaPersonalusuario2(APIView):
    def get(self, request, pk, format=None):
        Formulario = Personal.objects.all().filter(ci=pk)
        serializer = PersonalSerializer(Formulario, many=True)
        return Response(serializer.data)

''' Busca a la maxima autoridad ejecutiva del PROMES en este caso la directora'''
class BuscaMAE(APIView):
    def get(self, request, format=None):
        snippets= Personal.objects.filter(responsable="True").filter(estado="Activo").filter(unidad__nombre_unidad="DIRECCIÓN GENERAL")
        serializer = personal_Cargo_UnidadSerializer(snippets, many=True)
        return Response(serializer.data)

''' Busca un cargo de acuerdo a la descripcion que llega p.ej. descripcion=Administracio'''
class CargosSelectBusca(APIView):
    def get(self, request, descripcion, format=None):
        snippets= Cargo.objects.filter(descripcion=descripcion)
        serializer = CargoSerializer(snippets, many=True)
        return Response(serializer.data)

''' vista para importar personal pero existe un error pues la llave primaria es diferente a id'''
def simple_upload(request):
    if request.method == 'POST':
        personal_resource = PersonalResource()
        dataset = Dataset()
        new_persons = request.FILES['myfile']
        imported_data = dataset.load(new_persons.read())
        result = personal_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            personal_resource.import_data(dataset, dry_run=False)  # Actually import now
    return render(request, 'core/simple_upload.html')

''' vista para importar cargos desde un archivo excel'''
def simple_upload1(request):
    if request.method == 'POST':
        Cargo_resource = CargoResource()
        dataset = Dataset()
        new_CargoResource = request.FILES['myfile']
        imported_data = dataset.load(new_CargoResource.read())
        result = Cargo_resource.import_data(dataset, dry_run=True)  # Test the data import
        if not result.has_errors():
            Cargo_resource.import_data(dataset, dry_run=False)  # Actually import now
    return render(request, 'core/simple_upload1.html')
