from django.contrib import admin
from .models import Unidad
from .models import Cargo
from .models import T_Especialidad
from .models import Personal
from import_export.admin import ImportExportModelAdmin
from django.utils.translation import ugettext_lazy as _

@admin.register(Unidad)
class UnidadAdmin(admin.ModelAdmin):
    list_display = ["cod_unidad","nombre_unidad","cod_area_organizacional"]

@admin.register(Cargo)
class CargoAdmin(ImportExportModelAdmin):
    list_display = ["pk","descripcion"]

@admin.register(T_Especialidad)
class T_EspecialidadAdmin(admin.ModelAdmin):
    list_display = ["pk","especialidad"]

@admin.register(Personal)
class PersonalAdmin(ImportExportModelAdmin):
    list_display = ["ci","nombres","primer_apellido","segundo_apellido","genero","item","sector","responsable","cargo","carga_horaria","estado","foto_perfil","unidad","usuario","especialidad"]













