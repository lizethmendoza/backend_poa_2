from rest_framework import serializers
from .models import Cargo
from .models import Unidad
from .models import Personal
from ..Usuarios_Login.serializers import UserSerializer

class CargoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cargo
        fields = '__all__'

class UnidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unidad
        fields = '__all__'

#para login
class PersonalSerializer(serializers.ModelSerializer):
    Usuario = UserSerializer(read_only=True)
    unidad = UnidadSerializer(read_only=True)
    class Meta:
        model = Personal
        fields = '__all__'

class personal_Cargo_UnidadSerializer(serializers.ModelSerializer):
    unidad = UnidadSerializer(read_only=True)
    class Meta:
        model = Personal
        fields = '__all__'

class PersonalSerializer_1(serializers.ModelSerializer):
    class Meta:
        model = Personal
        fields = '__all__'
