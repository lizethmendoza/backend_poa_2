from import_export import resources
from .models import Personal
from .models import Cargo

#para subir desde archivo excel a el modelo
class PersonalResource(resources.ModelResource):
    class Meta:
        model = Personal

class CargoResource(resources.ModelResource):
    class Meta:
        model = Cargo
#prueba para que suba archivos con una llave primaria distinta a id             
# class Meta:
# #  model = PromoCode
# #  exclude = ('pc_used','pc_date',)
# #  import_id_fields = ['pc_code']