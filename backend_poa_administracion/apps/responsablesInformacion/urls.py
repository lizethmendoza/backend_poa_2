from django.urls import path

from .views import ListaPersonal
from .views import ListaCargos
from .views import ListaUnidades
from .views import ListaUnidadesID
from .views import GuardarCargos
from .views import ListaPersonalusuario
from .views import ListaPersonalusuario_1
from .views import ListaPersonalusuario2
from .views import CargosSelectBusca
from .views import BuscaMAE

urlpatterns = [
    path('ListaPersonal/', ListaPersonal.as_view(),name="ListaPersonal"),
    path('ListaCargos/', ListaCargos.as_view(),name="ListaCargos"),
    path('ListaUnidades/', ListaUnidades.as_view(),name="ListaUnidades"),
    path('ListaUnidadesID/<int:id>/', ListaUnidadesID.as_view(),name="ListaUnidadesID"),
    path('GuardarCargos/', GuardarCargos.as_view(),name="GuardarCargos"),
    path('ListaPersonalusuario/<str:usr>/', ListaPersonalusuario.as_view(),name="ListaPersonalusuario"),
    path('ListaPersonalusuario_1/', ListaPersonalusuario_1.as_view(),name="ListaPersonalusuario_1"),
    path('ListaPersonalusuario2/<int:pk>', ListaPersonalusuario2.as_view(),name="ListaPersonalusuario2"),
    path('CargosSelectBusca/<str:descripcion>/', CargosSelectBusca.as_view(),name="CargosSelectBusca"),
    path('BuscaMAE/', BuscaMAE.as_view(),name="BuscaMAE"),
]