from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Seguimiento
from .models import MedioVerificacionFile
from .models import AvanceEjecutadoTrimestral
# Register your models here.

@admin.register(Seguimiento)
class SeguimientoAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","Estado","Unidad","periodo","idPOAi"]

@admin.register(MedioVerificacionFile)
class MedioVerificacionFileAdmin(admin.ModelAdmin):
    list_display = ["pk", "ruta"]

@admin.register(AvanceEjecutadoTrimestral)
class AvanceEjecutadoTrimestralAdmin(admin.ModelAdmin):
    list_display = ["pk","CantidadEjecutado","CantidadEjecutadoParcial","RindicadorEficacia", "PonderacionPorc","Observacion_Conclusion","MedioVerificacionAdjunto","Descripcion","idObjetivoGestion","idSeguimiento"]