from rest_framework import serializers
from .models import Seguimiento
from ..responsablesInformacion.serializers import UnidadSerializer
from .models import AvanceEjecutadoTrimestral
from .models import MedioVerificacionFile
from ..Formulacion.serializers import objetivosGestionSerializer
from ..periodicidad.serializers import PeriodoSerializerPeriodicidad
from ..Poai.serializers import POAiSerializer

class SeguimientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seguimiento
        fields = '__all__'
        
#serializador para que te muestre los datos de la llave forea mas
class SeguimientoUnidadSerializer(serializers.ModelSerializer):
    Unidad=UnidadSerializer(read_only=True)
    class Meta:
        model = Seguimiento
        fields = '__all__'

class SeguimientoPeriodoSerializer(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = Seguimiento
        fields = '__all__'

class SeguimientoPeriodoSerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    Unidad=UnidadSerializer(read_only=True)
    class Meta:
        model = Seguimiento
        fields = '__all__'
        
class AvanceEjecutadoTrimestralSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvanceEjecutadoTrimestral
        fields = '__all__'

class MedioVerificacionFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedioVerificacionFile
        fields = "__all__"

class AvanceEjecutadoTrimestralSerializer_1(serializers.ModelSerializer):
    idSeguimiento=SeguimientoPeriodoSerializer(read_only=True)
    class Meta:
        model = AvanceEjecutadoTrimestral
        fields = '__all__'

class AvanceEjecutadoTrimestralSerializer_2(serializers.ModelSerializer):
    MedioVerificacionAdjunto=MedioVerificacionFileSerializer(read_only=True)
    class Meta:
        model = AvanceEjecutadoTrimestral
        fields = '__all__'
    
class AvanceTrimestralSumaSerializer(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    class Meta:
        model = AvanceEjecutadoTrimestral
        fields = ('idSeguimiento_id','sum')

#aqui
class SeguimientoUnidadSerializer_1(serializers.ModelSerializer):
    Unidad=UnidadSerializer(read_only=True)
    idPOAi=POAiSerializer(read_only=True)
    class Meta:
        model = Seguimiento
        fields = '__all__'

class AvanceEjecutadoTrimestralSerializer_4(serializers.ModelSerializer):
    idSeguimiento=SeguimientoUnidadSerializer_1(read_only=True)
    idObjetivoGestion=objetivosGestionSerializer(read_only=True)
    class Meta:
        model = AvanceEjecutadoTrimestral
        fields = '__all__'

class SeguimientoPeriodoSerializer_2(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idPOAi=POAiSerializer(read_only=True)
    
    class Meta:
        model = Seguimiento
        fields = '__all__'

class SeguimientoPeriodoSerializer_3(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idPOAi=POAiSerializer(read_only=True)
    Unidad=UnidadSerializer(read_only=True)
    class Meta:
        model = Seguimiento
        fields = '__all__'