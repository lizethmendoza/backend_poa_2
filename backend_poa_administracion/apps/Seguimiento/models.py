from django.db import models
from ..responsablesInformacion.models import Unidad
from ..Formulacion.models import objetivosGestion
from .. periodicidad.models import Periodo
from ..Poai.models import POAi

class Seguimiento(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
        ('Proceso seguimiento', 'Proceso seguimiento'),
        ('Proceso Revision', 'Proceso Revision'),
        ('Observado', 'Observado'),
        ('Aprobado PROMES', 'Aprobado PROMES'),
        ('Vigente', 'Vigente'),
        ('Aprobado SSU', 'Aprobado SSU'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso seguimiento')
    Unidad=models.ForeignKey(Unidad,on_delete=models.CASCADE)
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    idPOAi=models.ForeignKey(POAi,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de seguimientos")

class MedioVerificacionFile(models.Model):
    ruta = models.FileField(blank=False, null=False)
    def __str__(self):
        return self.ruta.name

class AvanceEjecutadoTrimestral(models.Model):
    CantidadEjecutado = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=1)
    CantidadEjecutadoParcial = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=1)
    PonderacionPorc=models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2) #%
    RindicadorEficacia = models.DecimalField(blank=True,null=True,max_digits=19, decimal_places=2)
    Observacion_Conclusion = models.TextField(max_length=500,null=True, blank=True)
    MedioVerificacionAdjunto=models.OneToOneField(MedioVerificacionFile,on_delete=models.SET_NULL,null=True, blank=True)
    Descripcion=models.TextField(max_length=500,null=True, blank=True)
    idObjetivoGestion = models.ForeignKey(objetivosGestion,on_delete=models.CASCADE)
    idSeguimiento = models.ForeignKey(Seguimiento,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Avence - OG")