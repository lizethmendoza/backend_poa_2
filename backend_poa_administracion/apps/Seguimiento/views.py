from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser

from .models import Seguimiento
from .serializers import SeguimientoSerializer
from .serializers import SeguimientoUnidadSerializer
from ..Formulacion.models import formularios
from ..Formulacion.serializers import objetivosGestion
from ..Formulacion.serializers import objetivosGestionFormularioSerializer
from ..Formulacion.models import mediosVerificacion
from ..Formulacion.serializers import mediosVerificacionSerializer_OF_OG
from .models import AvanceEjecutadoTrimestral
from .serializers import AvanceEjecutadoTrimestralSerializer
from .serializers import MedioVerificacionFileSerializer
from .serializers import AvanceEjecutadoTrimestralSerializer_1
from .serializers import AvanceEjecutadoTrimestralSerializer_2
from .models import MedioVerificacionFile
from django.db.models import Avg, Count, Min, Sum
from .serializers import AvanceTrimestralSumaSerializer
from .serializers import AvanceEjecutadoTrimestralSerializer_4
from .serializers import SeguimientoPeriodoSerializer
from .serializers import SeguimientoPeriodoSerializer_1
from .serializers import SeguimientoPeriodoSerializer_2
from .serializers import SeguimientoPeriodoSerializer_3
from django.db.models import Q
# Create your views here.

'''Lista todos los seguimientos'''
class ListarSeguimiento(APIView):
    def get(self, request):
        datos = Seguimiento.objects.all().order_by('Gestion')
        data = SeguimientoSerializer(datos, many=True).data
        return Response(data)

''' Guarda seguimiento'''
class GuardarSeguimiento(APIView):
    def post(self, request, format=None):
        serializer = SeguimientoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# ''' Lista los seguimientos de una determinada gestion, periodo , unidad ANTES DE LA REFORMULACION'''
# class ListarSeguimientoGPU(APIView):
#    def get(self, request, gestion,idUnidad,periodo,format=None):
#         datos = Seguimiento.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(Unidad=idUnidad).order_by('-Gestion')
#         data = SeguimientoPeriodoSerializer(datos, many=True).data
#         return Response(data)

''' Lista los seguimientos de una determinada gestion, periodo , unidad DESPUES DE LA REFORMULACION'''
class ListarSeguimientoGPU(APIView):
   def get(self, request, gestion,idUnidad,periodo,idPOA,format=None):
        datos = Seguimiento.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(Unidad=idUnidad).filter(idPOAi=idPOA).order_by('-Gestion')
        data = SeguimientoPeriodoSerializer(datos, many=True).data
        return Response(data)

''' Lista los seguimientos de una determinada gestion, periodo , unidad'''
class ListarSeguimientosUnidad(APIView):
   def get(self, request,idUnidad,format=None):        
        datos = Seguimiento.objects.all().filter(Unidad=idUnidad).order_by('-Gestion')
        data = SeguimientoPeriodoSerializer_2(datos, many=True).data
        return Response(data)

''' Lista los objetivos de gestion de una determinada unidad para el seguimiento antes de la reformulacion''' 
# class ListaFormulariosPOAi_OG_S(APIView):
#     def get(self, request,gestion,idUnidad,format=None):
#         respuesta= formularios.objects.filter(POAPertenece__Estado = 'Aprobado SSU').filter(tipo='ObjetivosGestion').filter(Gestion=gestion).filter(POAPertenece__Unidad=idUnidad).order_by('id')
#         lista=respuesta.values_list('id',flat=True)
#         snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).order_by('OP','Codigo')
#         serializer = objetivosGestionFormularioSerializer(snippets, many=True)
#         return Response(serializer.data)
''' Lista los objetivos de gestion de una determinada unidad para el seguimiento despues de la reformulacion''' 
class ListaFormulariosPOAi_OG_S(APIView):
    def get(self, request,gestion,idUnidad,idPOA,format=None):
        respuesta= formularios.objects.filter(tipo='ObjetivosGestion').filter(Gestion=gestion).filter(POAPertenece__Unidad=idUnidad).filter(POAPertenece__id = idPOA).order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets=objetivosGestion.objects.all().filter(idFormularios__in=lista).order_by('OP','Codigo')
        serializer = objetivosGestionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)
    
class MedioVerificacionDetalleIdO(APIView):
    def get(self, request, idO, format=None):
        snippets= mediosVerificacion.objects.filter(ObjetivoPertence=idO)
        serializer = mediosVerificacionSerializer_OF_OG(snippets, many=True)
        return Response(serializer.data)

''' Guarda avance'''
class GuardarAvanceEjecutadoTrimestral(APIView):
    def post(self, request, format=None):
        serializer = AvanceEjecutadoTrimestralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
class ListadeAvanceIdO(APIView):
    def get(self, request, idO, format=None):
        snippets= AvanceEjecutadoTrimestral.objects.filter(idObjetivoGestion=idO).order_by('idSeguimiento')
        serializer = AvanceEjecutadoTrimestralSerializer_1(snippets, many=True)
        
        return Response(serializer.data)

''' Guardar el archivo adjunto de medio de verificacion'''
class GuardarFileMedioVerificacion(APIView):
    parser_class = (FileUploadParser,)
    def post(self, request, *args, **kwargs):
      file_serializer = MedioVerificacionFileSerializer(data=request.data)
      if file_serializer.is_valid():
          file_serializer.save()
          return Response(file_serializer.data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListadeAvanceIdOIdSe(APIView):
    def get(self, request, idO,idSeg, format=None):
        snippets= AvanceEjecutadoTrimestral.objects.filter(idObjetivoGestion=idO).filter(idSeguimiento=idSeg)
        serializer = AvanceEjecutadoTrimestralSerializer(snippets, many=True)
        return Response(serializer.data)

class ListadeAvancePeriodo(APIView):
    def get(self, request, idO, pag,format=None):
        snippets= AvanceEjecutadoTrimestral.objects.filter(idObjetivoGestion=idO).filter(idSeguimiento__periodo__NumeroPerido=pag)
        serializer = AvanceEjecutadoTrimestralSerializer_1(snippets, many=True)
        return Response(serializer.data)

class ListadeAvancePeriodo_1(APIView):
    def get(self, request, idO, idSeg,pag,format=None):
        snippets= AvanceEjecutadoTrimestral.objects.filter(idObjetivoGestion=idO).filter(idSeguimiento=idSeg).filter(idSeguimiento__periodo__NumeroPerido=pag)
        serializer = AvanceEjecutadoTrimestralSerializer_1(snippets, many=True)
        return Response(serializer.data)

class ListadeAvancePeriodoIdSeguimiento(APIView):
    def get(self, request,idSeg,format=None):
        snippets= AvanceEjecutadoTrimestral.objects.filter(idSeguimiento=idSeg)
        serializer = AvanceEjecutadoTrimestralSerializer(snippets, many=True)
        return Response(serializer.data)

''' Lista, Actualiza  y elimina un registro seguimiento''' 
class actualizarSeguimiento(APIView):
    def get_object(self, pk):
        try:
            return Seguimiento.objects.get(pk=pk)
        except Seguimiento.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Actualiza un registro de  avance de ejecución''' 
class actualizarAvance(APIView):
    def get_object(self, pk):
        try:
            return AvanceEjecutadoTrimestral.objects.get(pk=pk)
        except AvanceEjecutadoTrimestral.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoTrimestralSerializer_2(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoTrimestralSerializer_2(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' Actualiza un registro de  avance de ejecución''' 
class actualizarAvance_1(APIView):
    def get_object(self, pk):
        try:
            return AvanceEjecutadoTrimestral.objects.get(pk=pk)
        except AvanceEjecutadoTrimestral.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoTrimestralSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AvanceEjecutadoTrimestralSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaMedioVerificacionId(APIView):
    def get(self, request, id ,format=None):        
        snippets= MedioVerificacionFile.objects.filter(id=id)
        serializer = MedioVerificacionFileSerializer(snippets, many=True)
        return Response(serializer.data)


''' Actualiza un registro de  avance de ejecución''' 
class actualizarMedioVerificacionFile(APIView):
    def get_object(self, pk):
        try:
            return MedioVerificacionFile.objects.get(pk=pk)
        except MedioVerificacionFile.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedioVerificacionFileSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedioVerificacionFileSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class SiexisteSeguimientoGestion(APIView):
    def get(self, request, gestion, unidad,format=None):        
        snippets= Seguimiento.objects.filter(Gestion=gestion).filter(Unidad=unidad)
        serializer = SeguimientoSerializer(snippets, many=True)
        return Response(serializer.data)
    
class SumaTotalObjetivosGestion(APIView):
    def get(self, request,id,format=None):
        snippets=AvanceEjecutadoTrimestral.objects.values('idSeguimiento_id').annotate(sum=Sum('PonderacionPorc')).filter(idSeguimiento_id=id)
        serializer = AvanceTrimestralSumaSerializer(snippets, many=True)
        return Response(serializer.data)
    
'''Lista todos los seguimientos con un estado de proceso de revision para consolidar poa  arreglar para q te muestre los observados y aprobados'''
# class ListarSeguimientoEstado(APIView):
#     def get(self, request):
#         datos = Seguimiento.objects.all().filter(Q(Estado="Proceso Revision") | Q(Estado="Observado") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES")).order_by('FechaElaboracion')
#         data = SeguimientoPeriodoSerializer_1(datos, many=True).data
#         return Response(data)

class ListarSeguimientoEstado(APIView):
    def get(self, request):
        datos = Seguimiento.objects.all().filter(Q(Estado="Proceso Revision") | Q(Estado="Observado") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES")).order_by('FechaElaboracion')
        data = SeguimientoPeriodoSerializer_3(datos, many=True).data
        return Response(data)

class ListarSeguimientoGestion(APIView):
    def get(self, request,gestion,format=None):
        datos = Seguimiento.objects.all().filter(Estado="Proceso Revision").filter(Gestion=gestion).order_by('FechaElaboracion')
        data = SeguimientoPeriodoSerializer_3(datos, many=True).data
        return Response(data)
    
class ListarSeguimientoUnidad(APIView):
    def get(self, request,idUnidad,format=None):
        datos = Seguimiento.objects.all().filter(Estado="Proceso Revision").filter(Unidad_id=idUnidad).order_by('FechaElaboracion')
        data = SeguimientoPeriodoSerializer_3(datos, many=True).data
        return Response(data)

# class ListarSeguimientoGestionEstado(APIView):
#     def get(self, request,gestion,periodicidad,periodo,format=None):
#         datos = Seguimiento.objects.all().filter(Estado="Proceso Revision").filter(TipoPeriodo=periodicidad).filter(Periodo=periodo).order_by('FechaElaboracion')
#         data = SeguimientoSerializer(datos, many=True).data
#         return Response(data)

# '''Lista todos los seguimientos con un estado de proceso de revision, Gestion , periodo  ANTES DE LA REFORMULACION'''
# class ListarSeguimientoGestionPeriodo(APIView):
#     def get(self, request,gestion,periodo,format=None):
#         datos = Seguimiento.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).order_by('FechaElaboracion')
#         lista=datos.values_list('id',flat=True)
#         snippets=AvanceEjecutadoTrimestral.objects.all().filter(idSeguimiento__in=lista).order_by('id')
#         data = AvanceEjecutadoTrimestralSerializer_4(snippets, many=True).data
#         return Response(data)

'''Lista todos los seguimientos con un estado de proceso de revision, Gestion , periodo DESPUES DE LA REFORMULACION'''
class ListarSeguimientoGestionPeriodo(APIView):
    def get(self, request,gestion,periodo,format=None):
        datos = Seguimiento.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        lista=datos.values_list('id',flat=True)
        snippets=AvanceEjecutadoTrimestral.objects.all().filter(idSeguimiento__in=lista).order_by('id')
        data = AvanceEjecutadoTrimestralSerializer_4(snippets, many=True).data
        return Response(data)

'''Lista todos los seguimientos con un estado de proceso de revision, Gestion ,Periodo y Unidad '''
class ListarSeguimientoGestionPeriodoUnidad(APIView):
    def get(self, request,gestion,periodo,pkUnidad,format=None):
        datos = Seguimiento.objects.all().filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo=periodo).filter(Unidad__cod_unidad=pkUnidad).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        lista=datos.values_list('id',flat=True)
        snippets=AvanceEjecutadoTrimestral.objects.all().filter(idSeguimiento__in=lista).order_by('id')
        data = AvanceEjecutadoTrimestralSerializer_4(snippets, many=True).data
        return Response(data)

class ListarSeguimientoGestionEstadoPeriodo(APIView):
    def get(self, request,gestion,periodo,format=None):
        datos = Seguimiento.objects.filter(Gestion=gestion).filter(Estado="Proceso Revision").filter(periodo_id=periodo).filter(idPOAi__Estado_version="Activo").order_by('FechaElaboracion')
        data = SeguimientoSerializer(datos, many=True).data
        return Response(data)


class ListarSeguimientoNotificacion(APIView):
    def get(self, request):
        datos = Seguimiento.objects.all().filter(Estado="Proceso Revision")
        data = SeguimientoPeriodoSerializer_3(datos, many=True).data
        return Response(data)