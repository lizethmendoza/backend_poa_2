from django.urls import path
from .views import ListarSeguimiento
from .views import GuardarSeguimiento
from .views import ListarSeguimientoGPU
from .views import ListarSeguimientosUnidad
from .views import ListaFormulariosPOAi_OG_S
from .views import MedioVerificacionDetalleIdO
from .views import GuardarAvanceEjecutadoTrimestral
from .views import ListadeAvanceIdO
from .views import GuardarFileMedioVerificacion
from .views import ListadeAvanceIdOIdSe
from .views import ListadeAvancePeriodo
from .views import ListadeAvancePeriodo_1
from .views import actualizarSeguimiento
from .views import ListadeAvancePeriodoIdSeguimiento
from .views import actualizarAvance
from .views import ListaMedioVerificacionId
from .views import actualizarMedioVerificacionFile
from .views import actualizarAvance_1
from .views import SiexisteSeguimientoGestion
from .views import SumaTotalObjetivosGestion
from .views import ListarSeguimientoEstado
from .views import ListarSeguimientoGestionPeriodo
# from .views import ListarSeguimientoNotificacion
from .views import ListarSeguimientoGestionPeriodoUnidad
from .views import ListarSeguimientoGestion
from .views import ListarSeguimientoUnidad
from .views import ListarSeguimientoGestionEstadoPeriodo
from .views import ListarSeguimientoNotificacion

urlpatterns = [
    path('ListarSeguimiento/', ListarSeguimiento.as_view(),name="ListarSeguimiento"),
    path('GuardarSeguimiento/', GuardarSeguimiento.as_view(),name="GuardarSeguimiento"),
    path('ListarSeguimientoGPU/<int:gestion>/<int:idUnidad>/<int:periodo>/<int:idPOA>/', ListarSeguimientoGPU.as_view(),name="ListarSeguimientoGPU"),
    path('ListarSeguimientosUnidad/<int:idUnidad>/', ListarSeguimientosUnidad.as_view(),name="ListarSeguimientosUnidad"),
    path('ListaFormulariosPOAi_OG_S/<int:gestion>/<int:idUnidad>/<int:idPOA>/', ListaFormulariosPOAi_OG_S.as_view(),name="ListaFormulariosPOAi_OG_S"),
    path('MedioVerificacionDetalleIdO/<int:idO>/', MedioVerificacionDetalleIdO.as_view(),name="MedioVerificacionDetalleIdO"),
    path('GuardarAvanceEjecutadoTrimestral/', GuardarAvanceEjecutadoTrimestral.as_view(),name="GuardarAvanceEjecutadoTrimestral"),
    path('ListadeAvanceIdO/<int:idO>/', ListadeAvanceIdO.as_view(),name="ListadeAvanceIdO"),
    path('GuardarFileMedioVerificacion/', GuardarFileMedioVerificacion.as_view(),name="GuardarFileMedioVerificacion"),
    path('ListadeAvanceIdOIdSe/<int:idO>/<int:idSeg>/', ListadeAvanceIdOIdSe.as_view(),name="ListadeAvanceIdOIdSe"),
    path('ListadeAvancePeriodo/<int:idO>/<int:pag>/', ListadeAvancePeriodo.as_view(),name="ListadeAvancePeriodo"),
    path('ListadeAvancePeriodo_1/<int:idO>/<int:pag>/<int:idSeg>/', ListadeAvancePeriodo_1.as_view(),name="ListadeAvancePeriodo_1"),
    path('actualizarSeguimiento/<int:pk>/', actualizarSeguimiento.as_view(),name="actualizarSeguimiento"),
    path('ListadeAvancePeriodoIdSeguimiento/<int:idSeg>/', ListadeAvancePeriodoIdSeguimiento.as_view(),name="ListadeAvancePeriodoIdSeguimiento"),
    path('actualizarAvance/<int:pk>/', actualizarAvance.as_view(),name="actualizarAvance"),
    path('ListaMedioVerificacionId/<int:id>/', ListaMedioVerificacionId.as_view(),name="ListaMedioVerificacionId"),
    path('actualizarMedioVerificacionFile/<int:pk>/', actualizarMedioVerificacionFile.as_view(),name="actualizarMedioVerificacionFile"),
    path('actualizarAvance_1/<int:pk>/', actualizarAvance_1.as_view(),name="actualizarAvance_1"),
    path('SiexisteSeguimientoGestion/<int:gestion>/<int:unidad>/', SiexisteSeguimientoGestion.as_view(),name="SiexisteSeguimientoGestion"),
    path('SumaTotalObjetivosGestion/<int:id>/', SumaTotalObjetivosGestion.as_view(),name="SumaTotalObjetivosGestion"),
    path('ListarSeguimientoEstado/', ListarSeguimientoEstado.as_view(),name="ListarSeguimientoEstado"),
    path('ListarSeguimientoGestionPeriodo/<int:gestion>/<str:periodo>/', ListarSeguimientoGestionPeriodo.as_view(),name="ListarSeguimientoGestionPeriodo"),
    # path('ListarSeguimientoGestionEstado/<int:gestion>/<str:periodicidad>/<str:periodo>/', ListarSeguimientoGestionEstado.as_view(),name="ListarSeguimientoGestionEstado"),
    path('ListarSeguimientoGestionPeriodoUnidad/<int:gestion>/<str:periodo>/<int:pkUnidad>/', ListarSeguimientoGestionPeriodoUnidad.as_view(),name="ListarSeguimientoGestionPeriodoUnidad"),
    path('ListarSeguimientoGestion/<int:gestion>/', ListarSeguimientoGestion.as_view(),name="ListarSeguimientoGestion"),
    path('ListarSeguimientoUnidad/<int:idUnidad>/', ListarSeguimientoUnidad.as_view(),name="ListarSeguimientoUnidad"),
    path('ListarSeguimientoGestionEstadoPeriodo/<int:gestion>/<int:periodo>/', ListarSeguimientoGestionEstadoPeriodo.as_view(),name="ListarSeguimientoGestionEstadoPeriodo"),
    path('ListarSeguimientoNotificacion/', ListarSeguimientoNotificacion.as_view(),name="ListarSeguimientoNotificacion"),
]