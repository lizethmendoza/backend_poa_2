from django.urls import path
from .views import DatosPOAi,GuardarPOAi,DatosPOAiDetalle,DatosPOAiDetalleUnidad,cambioEstadoPOA,ListaPOAiGestionEstado
from .views import ListaPOAiEstado
from .views import ListaPOAiEstadoUnidad
from .views import ListaPOAGestionEstado
from .views import ListaFormularioPoai
from .views import ListaPoasGestionUnidad
from .views import ListaPoasGestionUnidad_1
from .views import DatosPOAiDetalleUnidadReform
from .views import ListaPOAGestionVersion
from .views import ListaPOAiEstadoNotificacion
from .views import ListaPoasGestionUnidad_v1

urlpatterns = [
    path('DatosPOAi/', DatosPOAi.as_view(),name="DatosPOAi"),
    path('GuardarPOAi/', GuardarPOAi.as_view(),name="GuardarPOAi"),
    path('DatosPOAiDetalle/<int:gestion>/<int:unidad>/', DatosPOAiDetalle.as_view(),name="DatosPOAiDetalle"),
    path('DatosPOAiDetalleUnidad/<int:unidad>/', DatosPOAiDetalleUnidad.as_view(),name="DatosPOAiDetalleUnidad"),
    path('cambioEstadoPOA/<int:pk>/', cambioEstadoPOA.as_view(), name="cambioEstadoPOA"),
    path('ListaPOAiGestionEstado/<int:gestion>/', ListaPOAiGestionEstado.as_view(),name="ListaPOAiGestionEstado"),
    path('ListaPOAiEstado/', ListaPOAiEstado.as_view(),name="ListaPOAiEstado"),
    path('ListaPOAiEstadoUnidad/<int:idunidad>/', ListaPOAiEstadoUnidad.as_view(),name="ListaPOAiEstadoUnidad"),
    path('ListaPOAGestionEstado/<str:estado>/<int:gestion>/', ListaPOAGestionEstado.as_view(),name="ListaPOAGestionEstado"),
    path('ListaFormularioPoai/<int:id>/', ListaFormularioPoai.as_view(),name="ListaFormularioPoai"),
    path('ListaPoasGestionUnidad/<int:gestion>/<int:unidad>/<int:version>/', ListaPoasGestionUnidad.as_view(),name="ListaPoasGestionUnidad"),
    path('ListaPoasGestionUnidad_1/<int:unidad>/<int:gestion>/', ListaPoasGestionUnidad_1.as_view(),name="ListaPoasGestionUnidad_1"),
    path('DatosPOAiDetalleUnidadReform/<int:unidad>/', DatosPOAiDetalleUnidadReform.as_view(),name="DatosPOAiDetalleUnidadReform"),
    path('ListaPOAGestionVersion/<str:estado>/<int:gestion>/<int:version>/', ListaPOAGestionVersion.as_view(),name="ListaPOAGestionVersion"),
    path('ListaPOAiEstadoNotificacion/', ListaPOAiEstadoNotificacion.as_view(),name="ListaPOAiEstadoNotificacion"),
    path('ListaPoasGestionUnidad_v1/<int:unidad>/', ListaPoasGestionUnidad_v1.as_view(),name="ListaPoasGestionUnidad_v1"),
]