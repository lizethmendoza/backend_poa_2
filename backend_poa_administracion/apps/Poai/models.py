from django.db import models
from ..responsablesInformacion.models import Unidad

#POAsI por unidad
class POAi(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
    ('Proceso Formulacion', 'Proceso Formulacion'),
    ('Proceso Revision', 'Proceso Revision'),
    ('Observado', 'Observado'),
    ('Aprobado PROMES', 'Aprobado PROMES'),
    ('Vigente', 'Vigente'),
    ('Aprobado SSU', 'Aprobado SSU'),
    ('Cumplido', 'Cumplido'),
    ('Cumplido Reformulado', 'Cumplido Reformulado'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso Formulacion')
    ESTADOV_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo'),
    )
    VersionPOA = models.CharField(max_length=50)#campos para la reformulacion
    Estado_version = models.CharField(max_length=50, choices=ESTADOV_CHOICES, default='Inactivo')#campos para la reformulacion
    Unidad=models.ForeignKey(Unidad,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de POAs")
