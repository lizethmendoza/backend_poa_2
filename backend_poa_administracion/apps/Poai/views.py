from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .models import POAi
from .serializers import POAiSerializer,POAiUnidadSerializer
#descomentar para que funcione lo demas
from ..Formulacion.models import formularios
from ..Formulacion.serializers import formulariosSerializer
from django.db.models import Q

# Create your views here.
class DatosPOAi(APIView):
    def get(self, request):
        datos = POAi.objects.all().order_by('Gestion')
        data = POAiSerializer(datos, many=True).data
        return Response(data)

#sacamos el POA de una gestion dada y unidad 
class DatosPOAiDetalle(APIView):
    def get(self, request, gestion , unidad ,format=None):        
        snippets= POAi.objects.filter(Gestion=gestion).filter(Unidad=unidad).order_by('Gestion')
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)

# #Lista de POAs de una unidad especifica ANTES DE LA REFORMULACION
# class DatosPOAiDetalleUnidad(APIView):
#     def get(self, request, unidad , format=None):        
#         snippets= POAi.objects.filter(Unidad=unidad).order_by('-Gestion')
#         serializer = POAiSerializer(snippets, many=True)
#         return Response(serializer.data)

#Lista de POAs de una unidad especifica DESPUES DE LA REFORMULACION arreglar para que muestre los poa de las demas versiones pero solo aprobado por SSU
class DatosPOAiDetalleUnidad(APIView):
    def get(self, request, unidad , format=None):        
        snippets= POAi.objects.filter(Unidad=unidad).filter(VersionPOA=0).order_by('-Gestion')
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)

'''VISTA PARA GUARDAR EL POA INDIVUDIAL DE UNA UNIDAD'''
class GuardarPOAi(APIView):
    def post(self, request, format=None):
        serializer = POAiSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class cambioEstadoPOA(APIView):
    def get_object(self, pk):
        try:
            return POAi.objects.get(pk=pk)
        except POAi.DoesNotExist:
            raise Http404
    """Devuelve registros de acuerdo a al id"""
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = POAiSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN POA"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = POAiSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Listamos todos los POAi de la gestion y estado en ProcesoRevision 
class ListaPOAiGestionEstado(APIView):
    def get(self, request, gestion ,format=None):        
        snippets= POAi.objects.filter(Q(Estado="Proceso Revision") | Q(Estado="Observado") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") | Q(Estado="Cumplido")).filter(Gestion=gestion).order_by('Gestion')
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaPOAiEstado(APIView):
    def get(self, request ,format=None):        
        snippets= POAi.objects.filter(Q(Estado="Proceso Revision") | Q(Estado="Observado") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") | Q(Estado="Cumplido")).order_by('-Gestion')
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)
# snippets= POAGeneral.objects.filter(Q(Estado="Proceso Aprobacion") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") ).order_by('-Gestion')

class ListaPOAiEstadoUnidad(APIView):
    def get(self, request ,idunidad,format=None):        
        snippets= POAi.objects.filter(Q(Estado="Proceso Revision") | Q(Estado="Observado") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES")| Q(Estado="Cumplido")).filter(Unidad_id=idunidad).order_by('-Gestion')
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaPOAGestionEstado(APIView):
    def get(self, request , estado ,gestion,format=None):        
        snippets= POAi.objects.filter(Estado=estado).filter(Gestion=gestion).order_by('-Gestion')
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)

''' Lista los formularios con el id del poa individual'''
class ListaFormularioPoai(APIView):
    def get(self, request , id,format=None):        
        snippets= formularios.objects.filter(POAPertenece=id).filter(Estado='Observado').order_by('id')
        serializer = formulariosSerializer(snippets, many=True)
        return Response(serializer.data)

#sacamos el POA de una gestion dada y unidad y orden por la version --> para obtener el ultima versión
class ListaPoasGestionUnidad(APIView):
    def get(self, request, gestion , unidad ,version,format=None):        
        snippets= POAi.objects.filter(Gestion=gestion).filter(Unidad=unidad).filter(VersionPOA=version)
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)

#sacamos el POA de una gestion dada y unidad y orden por la version --> para obtener el ultima versión
class ListaPoasGestionUnidad_1(APIView):
    def get(self, request , unidad , gestion, format=None):        
        snippets= POAi.objects.filter(Unidad=unidad).filter(Estado_version='Activo').filter(Gestion = gestion)
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaPoasGestionUnidad_v1(APIView):
    def get(self, request , unidad , format=None):        
        snippets= POAi.objects.filter(Unidad=unidad).filter(Estado_version='Activo').filter(Estado= 'Aprobado SSU')
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)

#Lista de POAs de una unidad especifica
class DatosPOAiDetalleUnidadReform(APIView):
    def get(self, request, unidad , format=None): 
        snippets= POAi.objects.filter(Unidad=unidad).filter(Q(VersionPOA=1) | Q(VersionPOA=2) | Q(VersionPOA=3) | Q(Estado=4)).order_by('-Gestion')
        serializer = POAiSerializer(snippets, many=True)
        return Response(serializer.data)
       
''' saca todos los poa de la gestion y version dada'''
class ListaPOAGestionVersion(APIView):
    def get(self, request , estado ,gestion,version,format=None):        
        snippets= POAi.objects.filter(Estado=estado).filter(Gestion=gestion).filter(VersionPOA=version).order_by('-Gestion')
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaPOAiEstadoNotificacion(APIView):
    def get(self, request ,format=None):        
        snippets= POAi.objects.filter(Estado="Proceso Revision")
        serializer = POAiUnidadSerializer(snippets, many=True)
        return Response(serializer.data)

