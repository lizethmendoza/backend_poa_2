from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import POAi
# Register your models here.

@admin.register(POAi)
class POAiAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","Estado","Unidad","VersionPOA","Estado_version"]