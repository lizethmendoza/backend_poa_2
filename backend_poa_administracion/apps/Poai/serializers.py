from rest_framework import serializers
from .models import POAi
from ..responsablesInformacion.serializers import UnidadSerializer

class POAiSerializer(serializers.ModelSerializer):
    class Meta:
        model = POAi
        fields = '__all__'
        
#serializador para que te muestre los datos de la llave forea mas
class POAiUnidadSerializer(serializers.ModelSerializer):
    Unidad=UnidadSerializer(read_only=True)
    class Meta:
        model = POAi
        fields = '__all__'



