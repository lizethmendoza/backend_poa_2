from rest_framework import serializers
from .models import ObservacionFormulario
from ..Formulacion.serializers import formulariosPOAISerializer
from ..Poai.serializers import POAiSerializer
from .models import ObservacionAvance

class ObservacionFormularioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObservacionFormulario
        fields = '__all__'

class ObservacionFormularioSerializerPOA(serializers.ModelSerializer):
    idFormulario = formulariosPOAISerializer(read_only=True)
    class Meta:
        model = ObservacionFormulario
        fields = '__all__'
    
class ObservacionFormularioSerializerUnidad(serializers.ModelSerializer):
    idPOA= POAiSerializer(read_only=True)
    class Meta:
        model = ObservacionFormulario
        fields = '__all__'

class ObservacionAvanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObservacionAvance
        fields = '__all__'