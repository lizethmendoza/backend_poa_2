from django.urls import path
from .views import GuardarObservacionFormulario
from .views import ListaObservacionFormulario
from .views import ListaObservacionesId
from .views import ListaObservacionesIdPOA
from .views import actualizarObservacionFormulario
from .views import ListaObservacionesIdEs
from .views import ListaObservacionesIdPOA_1
from .views import ListaObservacionesIdActivos
from .views import GuardarObservacionAvance
from .views import ListaObservacionesIdAvance
from .views import actualizarObservacionAvance
from .views import ListaObservacionesIdObjetivo
from .views import ListaObservacionesIdSeguimiento
urlpatterns = [
    path('GuardarObservacionFormulario/', GuardarObservacionFormulario.as_view(),name="GuardarObservacionFormulario"),
    path('ListaObservacionFormulario/', ListaObservacionFormulario.as_view(),name="ListaObservacionFormulario"),
    path('ListaObservacionesId/<int:id>/', ListaObservacionesId.as_view(),name="ListaObservacionesId"),
    path('ListaObservacionesIdPOA/<int:id>/', ListaObservacionesIdPOA.as_view(),name="ListaObservacionesIdPOA"),
    path('actualizarObservacionFormulario/<int:pk>/', actualizarObservacionFormulario.as_view(),name="actualizarObservacionFormulario"),
    path('ListaObservacionesIdEs/<int:id>/', ListaObservacionesIdEs.as_view(),name="ListaObservacionesIdEs"),
    path('ListaObservacionesIdPOA_1/<int:id>/', ListaObservacionesIdPOA_1.as_view(),name="ListaObservacionesIdPOA_1"),
    path('ListaObservacionesIdActivos/<int:id>/', ListaObservacionesIdActivos.as_view(),name="ListaObservacionesIdActivos"),
    path('GuardarObservacionAvance/', GuardarObservacionAvance.as_view(),name="GuardarObservacionAvance"),
    path('ListaObservacionesIdAvance/<int:id>/', ListaObservacionesIdAvance.as_view(),name="ListaObservacionesIdAvance"),
    path('actualizarObservacionAvance/<int:pk>/', actualizarObservacionAvance.as_view(),name="actualizarObservacionAvance"),
    path('ListaObservacionesIdObjetivo/<int:idO>/', ListaObservacionesIdObjetivo.as_view(),name="ListaObservacionesIdObjetivo"),
    path('ListaObservacionesIdSeguimiento/<int:idS>/', ListaObservacionesIdSeguimiento.as_view(),name="ListaObservacionesIdSeguimiento"),
]