from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .models import ObservacionFormulario
from .serializers import ObservacionFormularioSerializer
from .serializers import ObservacionFormularioSerializerPOA
from .serializers import ObservacionFormularioSerializerUnidad
from .serializers import ObservacionAvanceSerializer
from .models import ObservacionAvance

# Create your views here.

class GuardarObservacionFormulario(APIView):
    def post(self, request, format=None):
        serializer = ObservacionFormularioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaObservacionFormulario(APIView):
    def get(self, request,format=None):
        ListaObservacionFormulario = ObservacionFormulario.objects.all()
        serializer = ObservacionFormularioSerializer(ListaObservacionFormulario, many=True)
        return Response(serializer.data)

class ListaObservacionesId(APIView):
    def get(self,request,id,format=None):
        snippets = ObservacionFormulario.objects.filter(idFormulario=id).order_by('Fecha')
        serializer = ObservacionFormularioSerializerPOA(snippets, many=True)
        return Response(serializer.data)

class ListaObservacionesIdPOA(APIView):
    def get(self,request,id,format=None):
        snippets = ObservacionFormulario.objects.filter(idPOA__Unidad=id)
        serializer = ObservacionFormularioSerializerUnidad(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE MS''' 
class actualizarObservacionFormulario(APIView):
    def get_object(self, pk):
        try:
            return ObservacionFormulario.objects.get(pk=pk)
        except ObservacionFormulario.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ObservacionFormularioSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ObservacionFormularioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaObservacionesIdEs(APIView):
    def get(self,request,id,format=None):
        snippets = ObservacionFormulario.objects.filter(idFEspecifico=id).order_by('Fecha')
        serializer = ObservacionFormularioSerializerPOA(snippets, many=True)
        return Response(serializer.data)

class ListaObservacionesIdPOA_1(APIView):
    def get(self,request,id,format=None):
        snippets = ObservacionFormulario.objects.filter(idPOA=id).filter(Estado="Activo")
        serializer = ObservacionFormularioSerializer(snippets, many=True)
        return Response(serializer.data)
    
class ListaObservacionesIdActivos(APIView):
    def get(self,request,id,format=None):
        snippets = ObservacionFormulario.objects.filter(idFormulario=id).filter(Estado="Activo").order_by('Fecha')
        serializer = ObservacionFormularioSerializerPOA(snippets, many=True)
        return Response(serializer.data)

''' Observaciones para avances '''   
class GuardarObservacionAvance(APIView):
    def post(self, request, format=None):
        serializer = ObservacionAvanceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaObservacionesIdAvance(APIView):
    def get(self,request,id, format=None):
        snippets = ObservacionAvance.objects.filter(idAvance=id).order_by('Fecha')
        serializer = ObservacionAvanceSerializer(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE OBERVACIONES DE AVANCE ''' 
class actualizarObservacionAvance(APIView):
    def get_object(self, pk):
        try:
            return ObservacionAvance.objects.get(pk=pk)
        except ObservacionAvance.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ObservacionAvanceSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ObservacionAvanceSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaObservacionesIdObjetivo(APIView):
    def get(self,request,idO, format=None):
        snippets = ObservacionAvance.objects.filter(idOGestion=idO).filter(Estado="Activo").order_by('Fecha')
        serializer = ObservacionAvanceSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaObservacionesIdSeguimiento(APIView):
    def get(self,request,idS, format=None):
        snippets = ObservacionAvance.objects.filter(idSeguimiento=idS).filter(Estado="Activo").order_by('Fecha')
        serializer = ObservacionAvanceSerializer(snippets, many=True)
        return Response(serializer.data)