from django.apps import AppConfig


class ProcesorevisionConfig(AppConfig):
    name = 'ProcesoRevision'
