from django.db import models
from ..Formulacion.models import formularios
from ..Poai.models import POAi
from ..Seguimiento.models import Seguimiento
from ..Seguimiento.models import AvanceEjecutadoTrimestral

# Create your models here.
class ObservacionFormulario(models.Model):
    Descripcion =  models.CharField(max_length=800)
    Fecha = models.DateField(auto_now=True)
    NombreR = models.CharField(max_length=100)
    ApellidosR = models.CharField(max_length=100)
    Cargo = models.CharField(max_length=100)
    # idFEspecifico=models.IntegerField()
    idPOA=models.ForeignKey(POAi,on_delete=models.CASCADE) #ayuda para localizar el poa
    idFormulario = models.ForeignKey(formularios,on_delete=models.CASCADE)
    Estado_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='Inactivo',blank=True)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Observaciones")
    
class ObservacionAvance(models.Model):
    Descripcion =  models.CharField(max_length=800)
    Fecha = models.DateField(auto_now=True)
    NombreR = models.CharField(max_length=100)
    ApellidosR = models.CharField(max_length=100)
    Cargo = models.CharField(max_length=100)
    idSeguimiento = models.ForeignKey(Seguimiento,on_delete=models.CASCADE) #ayuda 
    idAvance = models.ForeignKey(AvanceEjecutadoTrimestral,on_delete=models.CASCADE)
    Estado_CHOICES= (
    ('Activo', 'Activo'),
    ('Inactivo', 'Inactivo')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='Inactivo',blank=True)
    idOGestion=models.IntegerField() #id del cual pertence el avance
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Observaciones - Avances")