from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import ObservacionFormulario
from .models import ObservacionAvance

@admin.register(ObservacionFormulario)
class ObservacionFormularioAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion","Fecha","NombreR","ApellidosR","Cargo","idPOA","Estado","idFormulario"]

@admin.register(ObservacionAvance)
class ObservacionAvanceAdmin(admin.ModelAdmin):
    list_display = ["pk","Descripcion","Fecha","NombreR","ApellidosR","Cargo","idSeguimiento","idAvance","Estado","idOGestion"]
