from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin
from .models import Periodicidad
from .models import Periodo
from .models import PeriodicidadGestion
from .models import FechaPeriodo 
# Register your models here.

@admin.register(Periodicidad)
class PeriodicidadAdmin(admin.ModelAdmin):
    list_display = ["pk", "TipoPeriodo"]

@admin.register(Periodo)
class PeriodoAdmin(ImportExportModelAdmin):
    list_display = ["pk", "Periodo", "NumeroPerido","MesInicio","MesFin","idPeriodicidad"]

@admin.register(PeriodicidadGestion)
class PeriodicidadGestionAdmin(admin.ModelAdmin):
    list_display = ["pk","Fecha", "Gestion","Estado","idPeriodicidad"]

@admin.register(FechaPeriodo)
class FechaPeriodoAdmin(ImportExportModelAdmin):
    list_display = ["pk", "FechaInicio", "FechaFin","Estado","idPeriodo"]
