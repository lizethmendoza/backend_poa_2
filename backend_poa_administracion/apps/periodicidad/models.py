from django.db import models

# Create your models here.
class Periodicidad(models.Model):
    TIPO_PERIODO_CHOICES= (
        ('Bimestral', 'Bimestral'),
        ('Trimestral', 'Trimestral'),
        ('Cuatrimestral','Cuatrimestral'),
        ('Semestral', 'Semestral'),
    )
    TipoPeriodo = models.CharField(max_length=50, choices=TIPO_PERIODO_CHOICES, default='')
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Periodicidad")

class Periodo(models.Model):
    PERIODO_CHOICES= (
        ('primer bimestre', '1° Bimentre'),
        ('segundo bimestre', '2° Bimentre'),
        ('tercer bimestre','3° Bimentre'),
        ('cuarto bimestre', '4° Bimentre'),
        ('quinto bimestre', '5° Bimentre'),
        ('sexto bimestre', '6° Bimentre'),
        ('primer trimestre', '1° Trimestre'),
        ('segundo trimestre', '2° Trimestre'),
        ('tercer trimestre', '3° Trimestre'),
        ('cuarto trimestre', '4° Trimestre'),
        ('primer cuatrimestre', '1° Cuatrimestre'),
        ('segundo cuatrimestre', '2° Cuatrimestre'),
        ('tercer cuatrimestre', '3° Cuatrimestre'),
        ('primer semestre', '1° Semestre'),
        ('segundo semestre', '2° Semestre'),
    )
    Periodo = models.CharField(max_length=50, choices=PERIODO_CHOICES, default='')
    NumeroPerido = models.IntegerField()
    MESES_CHOICES= (
        ('Enero', 'Enero'),
        ('Febrero', 'Febrero'),
        ('Marzo','Marzo'),
        ('Abril', 'Abril'),
        ('Mayo', 'Mayo'),
        ('Junio', 'Junio'),
        ('Julio', 'Julio'),
        ('Agosto', 'Agosto'),
        ('Septiembre', 'Septiembre'),
        ('Octubre', 'Octubre'),
        ('Noviembre', 'Noviembre'),
        ('Diciembre', 'Diciembre'),
    )
    MesInicio = models.CharField(max_length=15, choices=MESES_CHOICES, default='')
    MesFin = models.CharField(max_length=15, choices=MESES_CHOICES, default='')
    idPeriodicidad = models.ForeignKey(Periodicidad,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Periodo")

class PeriodicidadGestion(models.Model):
    Fecha =  models.DateField(auto_now=True)
    Gestion = models.IntegerField()
    Estado_CHOICES= (
        ('Habilitado', 'Habilitado'),
        ('Inhabilitado', 'Inhabilitado')
    )
    Estado= models.CharField(max_length=50, choices=Estado_CHOICES, default='Inhabilitado',blank=True)
    idPeriodicidad = models.ForeignKey(Periodicidad,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de periodicidad por gestion")

class FechaPeriodo(models.Model):
    FechaInicio =  models.DateField()
    FechaFin =  models.DateField()
    Estado_CHOICES= (
    ('Vigente', 'Vigente'),
    ('Expirado', 'Expirado')
    )
    Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='Expirado',blank=True)
    idPeriodo = models.ForeignKey(Periodo,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Fechas de periodo")
        
# class PeriodicidadSeguimiento(models.Model):
#     FechaInicio =  models.DateField()
#     FechaFin =  models.DateField()
#     Observacion = models.CharField(max_length=500,blank=True)
#     Gestion = models.IntegerField()
#     Estado_CHOICES= (
#     ('Vigente', 'Vigente'),
#     ('Expirado', 'Expirado')
#     )
#     Estado= models.CharField(max_length=20, choices=Estado_CHOICES, default='Expirado',blank=True)
#     TIPO_PERIODO_CHOICES= (
#         ('Bimensual', 'Bimensual'),
#         ('Trimestral', 'Trimestral'),
#         ('Cuatrimestral','Cuatrimestral'),
#         ('Semestral', 'Semestral'),
#     )
#     TipoPeriodo = models.CharField(max_length=50, choices=TIPO_PERIODO_CHOICES, default='')
#     PERIODO_CHOICES= (
#         ('primer bimestre', '1° Bimentre'),
#         ('segundo bimestre', '2° Bimentre'),
#         ('tercer bimestre','3° Bimentre'),
#         ('cuarto bimestre', '4° Bimentre'),
#         ('quinto bimestre', '5° Bimentre'),
#         ('sexto bimestre', '6° Bimentre'),
#         ('primer trimestre', '1° Trimestre'),
#         ('segundo trimestre', '2° Trimestre'),
#         ('tercer trimestre', '3° Trimestre'),
#         ('cuarto trimestre', '4° Trimestre'),
#         ('primer cuatrimestre', '1° Cuatrimestre'),
#         ('segundo cuatrimestre', '2° Cuatrimestre'),
#         ('tercer cuatrimestre', '3° Cuatrimestre'),
#         ('primer semestre', '1° Semestre'),
#         ('segundo semestre', '2° Semestre'),
#     )
#     Periodo = models.CharField(max_length=100, choices=PERIODO_CHOICES, default='')
#     NumeroPerido = models.IntegerField()
#     MESES_CHOICES= (
#         ('Enero', 'Enero'),
#         ('Febrero', 'Febrero'),
#         ('Marzo','Marzo'),
#         ('Abril', 'Abril'),
#         ('Mayo', 'Mayo'),
#         ('Junio', 'Junio'),
#         ('Julio', 'Julio'),
#         ('Agosto', 'Agosto'),
#         ('Septiembre', 'Septiembre'),
#         ('Octubre', 'Octubre'),
#         ('Noviembre', 'Noviembre'),
#         ('Diciembre', 'Diciembre'),
#     )
#     MesInicio = models.CharField(max_length=100, choices=MESES_CHOICES, default='')
#     MesFin = models.CharField(max_length=100, choices=MESES_CHOICES, default='')
#     def __str__(self):
#         return str(self.pk)
#     class Meta:
#         verbose_name_plural = ("Periodicidad")

# class HabilitarPeriodicidadSeguimiento(models.Model):
#     Fecha =  models.DateField(auto_now=True)
#     Gestion = models.IntegerField()
#     Estado_CHOICES= (
#         ('Habilitado', 'Habilitado'),
#         ('Inhabilitado', 'Inhabilitado')
#     )
#     Estado= models.CharField(max_length=50, choices=Estado_CHOICES, default='Inhabilitado',blank=True)
#     TIPO_PERIODO_CHOICES= (
#         ('Bimensual', 'Bimensual'),
#         ('Trimestral', 'Trimestral'),
#         ('Cuatrimestral','Cuatrimestral'),
#         ('Semestral', 'Semestral'),
#     )
#     TipoPeriodo = models.CharField(max_length=50, choices=TIPO_PERIODO_CHOICES, default='')
#     def __str__(self):
#         return str(self.pk)
#     class Meta:
#         verbose_name_plural = ("Estado de Periodo")
