from django.urls import path
from .views import ListaPeriodicidad
from .views import ListaPeriodo
from .views import ListaPeriodoDetalle
from .views import ListaFechasPeriodo
from .views import ListaDeFechasPerido
from .views import ListaFechasPeriodo
from .views import GuardarPeriodicidadGestion
from .views import VerificaPeriodicidadGestion
from .views import ListaPeriodicidadTipo
from .views import actualizarFechaPeriodo
from .views import actualizarPeriodicidadGestion
from .views import ListaPeriodicidadGestion
from .views import VerificaPeriodicidadGestionHab
from .views import ListaFechasPeriodoPeriodicidad
from .views import ListaDeFechasPerido_v1

urlpatterns = [
    path('ListaPeriodicidad/', ListaPeriodicidad.as_view(),name="ListaPeriodicidad"),
    path('ListaPeriodo/', ListaPeriodo.as_view(),name="ListaPeriodo"),
    path('ListaPeriodoDetalle/', ListaPeriodoDetalle.as_view(),name="ListaPeriodoDetalle"),
    path('ListaFechasPeriodo/', ListaFechasPeriodo.as_view(),name="ListaFechasPeriodo"),
    path('ListaDeFechasPerido/<str:Tperiodo>/', ListaDeFechasPerido.as_view(),name="ListaDeFechasPerido"),
    path('ListaDeFechasPerido_v1/<str:Tperiodo>/', ListaDeFechasPerido_v1.as_view(),name="ListaDeFechasPerido_v1"),
    path('ListaFechasPeriodo/', ListaFechasPeriodo.as_view(),name="ListaFechasPeriodo"),
    path('GuardarPeriodicidadGestion/', GuardarPeriodicidadGestion.as_view(),name="GuardarPeriodicidadGestion"),
    path('VerificaPeriodicidadGestion/<int:gestion>/', VerificaPeriodicidadGestion.as_view(),name="VerificaPeriodicidadGestion"),
    path('ListaPeriodicidadTipo/<str:tipo>/', ListaPeriodicidadTipo.as_view(),name="ListaPeriodicidadTipo"),
    path('actualizarFechaPeriodo/<int:pk>/', actualizarFechaPeriodo.as_view(),name="actualizarFechaPeriodo"),
    path('actualizarPeriodicidadGestion/<int:pk>/', actualizarPeriodicidadGestion.as_view(),name="actualizarPeriodicidadGestion"),
    path('ListaPeriodicidadGestion/', ListaPeriodicidadGestion.as_view(),name="ListaPeriodicidadGestion"),
    path('VerificaPeriodicidadGestionHab/<int:gestion>/', VerificaPeriodicidadGestionHab.as_view(),name="VerificaPeriodicidadGestionHab"),
    path('ListaFechasPeriodoPeriodicidad/<int:idPeriodo>/', ListaFechasPeriodoPeriodicidad.as_view(),name="ListaFechasPeriodoPeriodicidad"),
]