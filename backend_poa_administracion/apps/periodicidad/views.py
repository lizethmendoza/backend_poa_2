from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from tablib import Dataset

from .models import Periodicidad
from .models import Periodo
from .serializers import PeriodicidadSerializer
from .serializers import PeriodoSerializer
from .serializers import PeriodoSerializerPeriodicidad
from .models import FechaPeriodo
from .serializers import fechasPeriodoSerializer
from .models import PeriodicidadGestion
from .serializers import PeriodicidadGestionSerializer
from .serializers import PeriodicidadGestionSerializer_1
from .serializers import FechaPeriodoSerializer

class ListaPeriodicidad(APIView):
    def get(self, request,format=None):
        ListaFormularioEstado = Periodicidad.objects.all().order_by('id')
        serializer = PeriodicidadSerializer(ListaFormularioEstado, many=True)
        return Response(serializer.data)

class ListaPeriodo(APIView):
    def get(self, request,format=None):
        ListaFormularioEstado = Periodo.objects.all().order_by('id')
        serializer = PeriodoSerializer(ListaFormularioEstado, many=True)
        return Response(serializer.data)

class ListaPeriodoDetalle(APIView):
    def get(self, request,format=None):
        ListaFormularioEstado = Periodo.objects.all().order_by('id')
        serializer = PeriodoSerializerPeriodicidad(ListaFormularioEstado, many=True)
        return Response(serializer.data)

class ListaPeriodoDetalle(APIView):
    def get(self, request,format=None):
        ListaFormularioEstado = Periodo.objects.all().order_by('id')
        serializer = PeriodoSerializerPeriodicidad(ListaFormularioEstado, many=True)
        return Response(serializer.data)

class ListaFechasPeriodoPeriodicidad(APIView):
    def get(self, request,idPeriodo,format=None):
        ListaFormularioEstado = Periodo.objects.filter(id=idPeriodo).order_by('id')
        serializer = PeriodoSerializerPeriodicidad(ListaFormularioEstado, many=True)
        return Response(serializer.data)

''' Lista todos los periodos con sus fechas de acuerdo al tipo de periodo'''
class ListaDeFechasPerido(APIView):
    def get(self, request,Tperiodo,format=None):
        respuesta= Periodo.objects.filter(idPeriodicidad__TipoPeriodo=Tperiodo)
        lista=respuesta.values_list('id',flat=True)
        # 
        snippets=FechaPeriodo.objects.all().filter(idPeriodo__in=lista).order_by('id').order_by('idPeriodo__NumeroPerido')
        serializer = fechasPeriodoSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaDeFechasPerido_v1(APIView):
    def get(self, request,Tperiodo,format=None):
        respuesta= Periodo.objects.filter(idPeriodicidad__TipoPeriodo=Tperiodo)
        lista=respuesta.values_list('id',flat=True)
        snippets=FechaPeriodo.objects.all().filter(idPeriodo__in=lista).order_by('id').order_by('idPeriodo__NumeroPerido').filter(Estado="Vigente")
        serializer = fechasPeriodoSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaFechasPeriodo(APIView):
    def get(self, request,format=None):
        HistorialPeridicidad = PeriodicidadGestion.objects.all().order_by('id')
        serializer = PeriodicidadGestionSerializer(HistorialPeridicidad, many=True)
        return Response(serializer.data)

class GuardarPeriodicidadGestion(APIView):
    def post(self, request, format=None):
        serializer = PeriodicidadGestionSerializer_1(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class VerificaPeriodicidadGestion(APIView):
    def get(self, request, gestion ,format=None):        
        snippets= PeriodicidadGestion.objects.filter(Gestion=gestion)
        serializer = PeriodicidadGestionSerializer_1(snippets, many=True)
        return Response(serializer.data)

''' verifica si la periodicidad para el seguimiento fue creado y es habilitado'''
class VerificaPeriodicidadGestionHab(APIView):
    def get(self, request, gestion ,format=None):        
        snippets= PeriodicidadGestion.objects.filter(Gestion=gestion).filter(Estado='Habilitado')
        serializer = PeriodicidadGestionSerializer(snippets, many=True)
        return Response(serializer.data)

''' Obtiene el id del periodo de acuerdo al tipo'''
class ListaPeriodicidadTipo(APIView):
    def get(self, request, tipo ,format=None):        
        snippets= Periodicidad.objects.filter(TipoPeriodo=tipo)
        serializer = PeriodicidadSerializer(snippets, many=True)
        return Response(serializer.data)

''' Actualiza un registro medio de verificacion''' 
class actualizarFechaPeriodo(APIView):
    def get_object(self, pk):
        try:
            return FechaPeriodo.objects.get(pk=pk)
        except FechaPeriodo.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FechaPeriodoSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FechaPeriodoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' ACTUALIZAR UN REGISTRO DE PERIODICIDAD GESTION''' 

class actualizarPeriodicidadGestion(APIView):
    def get_object(self, pk):
        try:
            return PeriodicidadGestion.objects.get(pk=pk)
        except PeriodicidadGestion.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PeriodicidadGestionSerializer_1(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PeriodicidadGestionSerializer_1(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ListaPeriodicidadGestion(APIView):
    def get(self, request,format=None):
        HistorialPeridicidad = PeriodicidadGestion.objects.filter(Estado='Habilitado').order_by('id')
        serializer = PeriodicidadGestionSerializer_1(HistorialPeridicidad, many=True)
        return Response(serializer.data)

# class ListaPeriodicidadCuatrimestral(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = PeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Cuatrimestral').order_by('NumeroPerido')
#         serializer = PeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# class ListaPeriodicidadSemestral(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = PeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Semestral').order_by('NumeroPerido')
#         serializer = PeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)


# ''' ACTUALIZAR UN REGISTRO DE MS''' 
# class actualizarPeriodicidad(APIView):
#     def get_object(self, pk):
#         try:
#             return PeriodicidadSeguimiento.objects.get(pk=pk)
#         except PeriodicidadSeguimiento.DoesNotExist:
#             raise Http404
#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = PeriodicidadSeguimientoSerializer(snippet)
#         return Response(serializer.data)
#     """ACTUALIZA UN REGISTRO"""
#     def put(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = PeriodicidadSeguimientoSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#     ''' elimina un registro'''
#     def delete(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         snippet.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# class ListaPeriodicidadBimestralEstado(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = HabilitarPeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Bimensual')
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# class ListaPeriodicidadTrimestralEstado(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = HabilitarPeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Trimestral')
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# class ListaPeriodicidadCuatrimestralEstado(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = HabilitarPeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Cuatrimestral')
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# class ListaPeriodicidadSemestralEstado(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = HabilitarPeriodicidadSeguimiento.objects.all().filter(TipoPeriodo='Semestral')
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# ''' ACTUALIZAR UN REGISTRO DE MS''' 
# class actualizarPeriodicidadEstado(APIView):
#     def get_object(self, pk):
#         try:
#             return HabilitarPeriodicidadSeguimiento.objects.get(pk=pk)
#         except HabilitarPeriodicidadSeguimiento.DoesNotExist:
#             raise Http404
#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(snippet)
#         return Response(serializer.data)
#     """ACTUALIZA UN REGISTRO"""
#     def put(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class ExisteHabilitado(APIView):
#     def get(self, request,format=None):
#         ListaFormularioEstado = HabilitarPeriodicidadSeguimiento.objects.all().filter(Estado='Habilitado')
#         serializer = HabilitarPeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)

# class ListarPeriodicidadPeriodo(APIView):
#     def get(self, request,Tperiodo,periodo,format=None):
#         ListaFormularioEstado = PeriodicidadSeguimiento.objects.all().filter(TipoPeriodo=Tperiodo).filter(Periodo=periodo)
#         serializer = PeriodicidadSeguimientoSerializer(ListaFormularioEstado, many=True)
#         return Response(serializer.data)



def simple_upload(request):
    if request.method == 'POST':
        Periodo_resource = PeriodoResource()
        dataset = Dataset()
        new_Periodo = request.FILES['myfile']
        imported_data = dataset.load(new_Periodo.read())
        result = Periodo_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            Periodo_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload.html')

def simple_upload1(request):
    if request.method == 'POST':
        FechaPeriodo_resource = FechaPeriodoResource()
        dataset = Dataset()
        new_FechaPeriodo = request.FILES['myfile']
        imported_data = dataset.load(new_FechaPeriodo.read())
        result = FechaPeriodo_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            FechaPeriodo_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload1.html')
