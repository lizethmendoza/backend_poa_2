from import_export import resources
from .models import Periodo


class PeriodoResource(resources.ModelResource):
    class Meta:
        model = Periodo

class FechaPeriodoResource(resources.ModelResource):
    class Meta:
        model = FechaPeriodo
    
