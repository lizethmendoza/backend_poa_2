from rest_framework import serializers
from .models import Periodicidad
from .models import Periodo
from .models import FechaPeriodo
from .models import PeriodicidadGestion

class PeriodicidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periodicidad
        fields = '__all__'

class PeriodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periodo
        fields = '__all__'

class PeriodoSerializerPeriodicidad(serializers.ModelSerializer):
    idPeriodicidad = PeriodicidadSerializer(read_only=True)
    class Meta:
        model = Periodo
        fields = '__all__'

class fechasPeriodoSerializer(serializers.ModelSerializer):
    idPeriodo = PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = FechaPeriodo
        fields = '__all__'

class FechaPeriodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FechaPeriodo
        fields = '__all__'

class PeriodicidadGestionSerializer(serializers.ModelSerializer):
    idPeriodicidad = PeriodicidadSerializer(read_only=True)
    class Meta:
        model = PeriodicidadGestion
        fields = '__all__'

class PeriodicidadGestionSerializer_1(serializers.ModelSerializer):
    class Meta:
        model = PeriodicidadGestion
        fields = '__all__'