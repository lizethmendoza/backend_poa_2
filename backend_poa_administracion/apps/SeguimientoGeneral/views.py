from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser

from .models import SeguimientoGeneral
from .serializers import SeguimientoGeneralSerializer
from .serializers import SeguimientoGeneralSerializer_1
from .models import FormularioSeguimientoGeneral
from .serializers import FormularioSeguimientoGeneralSerializer
from .serializers import ContieneAvancesSeguimientosSerializer
from .serializers import FormSeguimientoPeriodoGeneralSerializer
from .serializers import ContieneAvancesSegSerializer
from .models import ContieneAvancesSeguimientos

from .models import FormularioSeguimientoGeneralOpe
from .models import ContieneAvancesOperaciones

from .serializers import FormularioSeguimientoGeneralOpeSerializer
from .serializers import ContieneAvancesOperacionesSerializer
from .serializers import FormularioSeguimientoGeneralOpeSerializer_1
from .serializers import ContieneAvancesOperacionesSerializerOPE
from .serializers import ContieneAvancesSegSerializer_1
from .serializers import ContieneAvancesSegSerializer_2
from .serializers import ContieneAvancesSegOPESerializer_1
from .serializers import ContieneAvancesSegOPESerializer_2
from .serializers import SeguimientoGeneralSerializer_2
from django.db.models import Q
from django.db.models import Avg, Count, Min, Sum
# from .models import MedioVerificacionFile
# from django.db.models import Avg, Count, Min, Sum

# from .serializers import AvanceEjecutadoTrimestralSerializer_4
# Create your views here.

'''Lista todos los seguimientos'''
class ListarSeguimientoGeneral(APIView):
    def get(self, request):
        datos = SeguimientoGeneral.objects.all().order_by('Gestion')
        data = SeguimientoGeneralSerializer_2(datos, many=True).data
        return Response(data)

''' Guarda seguimiento'''
class GuardarSeguimientoGeneral(APIView):
    def post(self, request, format=None):
        serializer = SeguimientoGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListarSeguimientoGPP(APIView):
   def get(self, request, gestion,periodo,idPOA,format=None):        
        datos = SeguimientoGeneral.objects.all().filter(Gestion=gestion).filter(periodo__id=periodo).filter(idPOAg=idPOA).order_by('-Gestion')
        data = SeguimientoGeneralSerializer_1(datos, many=True).data
        return Response(data)

''' Guarda Formulario de seguimiento General'''
class GuardarFormSeguimientoGeneral(APIView):
    def post(self, request, format=None):
        serializer = FormularioSeguimientoGeneralSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


''' Guarda Avances de un determiado Formulario y seguimiento'''
class GuardarAvancesGenerales(APIView):
    def post(self, request, format=None):
        serializer = ContieneAvancesSeguimientosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista los seguimientos de una determinada gestion, periodo , unidad'''
class FormulariosSeguimientoGeneral(APIView):
   def get(self, request,idSeg,gestion,IdPer,format=None):        
        datos = FormularioSeguimientoGeneral.objects.filter(Gestion=gestion).filter(periodo=IdPer).filter(idSegGeneral=idSeg)
        data = FormularioSeguimientoGeneralSerializer(datos, many=True).data
        return Response(data)

''' Lista el formulario con el id dado con el detalle de periodo'''
class ListaFormularioSeguimientoGeneral(APIView):
    def get(self, request, idForm, format=None):
        snippets= FormularioSeguimientoGeneral.objects.filter(id=idForm)
        serializer = FormSeguimientoPeriodoGeneralSerializer(snippets, many=True)
        return Response(serializer.data)

''' Lista los avances guardados en formularios generales'''
class ListaAvancesGuardadosGeneral(APIView):
    def get(self, request, idForm, format=None):
        snippets= ContieneAvancesSeguimientos.objects.filter(idFormSegGeneral=idForm)
        serializer = ContieneAvancesSegSerializer(snippets, many=True)
        return Response(serializer.data)

#---- para el formulario de operaciones de funcionamiento -SEGUIMIENTO-#
''' Guarda Formulario de seguimiento General'''
class GuardarFormSeguimientoGeneralOPE(APIView):
    def post(self, request, format=None):
        serializer = FormularioSeguimientoGeneralOpeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Guarda Avances de un determiado Formulario y seguimiento'''
class GuardarAvancesGeneralesOPE(APIView):
    def post(self, request, format=None):
        serializer = ContieneAvancesOperacionesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista los seguimientos de una determinada gestion, periodo , unidad'''
class FormulariosSeguimientoGeneralOPE(APIView):
   def get(self, request,idSeg,gestion,IdPer,format=None):        
        datos = FormularioSeguimientoGeneralOpe.objects.filter(Gestion=gestion).filter(periodo=IdPer).filter(idSegGeneral=idSeg)
        data = FormularioSeguimientoGeneralOpeSerializer(datos, many=True).data
        return Response(data)

''' Lista el formulario OPERACIONES con el id dado con el detalle de periodo'''
class ListaFormularioSeguimientoGeneralOPE(APIView):
    def get(self, request, idForm, format=None):
        snippets= FormularioSeguimientoGeneralOpe.objects.filter(id=idForm)
        serializer = FormularioSeguimientoGeneralOpeSerializer_1(snippets, many=True)
        return Response(serializer.data)


''' Lista los avances Operaciones de Funcionamiento guardados en formularios generales'''
class ListaAvancesGuardadosGeneralOPE(APIView):
    def get(self, request, idForm, format=None):
        snippets= ContieneAvancesOperaciones.objects.filter(idFormSegGeneral=idForm)
        serializer = ContieneAvancesOperacionesSerializerOPE(snippets, many=True)
        return Response(serializer.data)

''' Lista los avances guardados en formularios generales por unidad'''
class ListaAvancesGuardadosGeneral_U(APIView):
    def get(self, request, idForm, idU,format=None):
        snippets= ContieneAvancesSeguimientos.objects.filter(idFormSegGeneral=idForm).filter(idAvanceUnidad__idSeguimiento__Unidad__cod_unidad=idU)
        serializer = ContieneAvancesSegSerializer(snippets, many=True)
        return Response(serializer.data)

''' Lista los avances Operaciones de Funcionamiento guardados en formularios generales por unidad'''
class ListaAvancesGuardadosGeneralOPE_U(APIView):
    def get(self, request, idForm,idU, format=None):
        snippets= ContieneAvancesOperaciones.objects.filter(idFormSegGeneral=idForm).filter(idAvanceUnidad__idSeguimiento__Unidad__cod_unidad=idU)
        serializer = ContieneAvancesOperacionesSerializerOPE(snippets, many=True)
        return Response(serializer.data)

''' ACTUALIZAR el estado de seguimiento general''' 
class actualizarSeguimientoGeneral(APIView):
    def get_object(self, pk):
        try:
            return SeguimientoGeneral.objects.get(pk=pk)
        except SeguimientoGeneral.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoGeneralSerializer(snippet)
        return Response(serializer.data)
    """ACTUALIZA UN REGISTRO"""
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SeguimientoGeneralSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# snippets= POAGeneral.objects.filter(Q(Estado="Proceso Aprobacion") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") ).order_by('-Gestion')

'''Lista todos los seguimientos en estado de aprobacion aprobado ssu y aprobado promes'''
class ListarSeguimientoGeneralEstados(APIView):
    def get(self, request):
        datos = SeguimientoGeneral.objects.filter(Q(Estado="Proceso Aprobacion") | Q(Estado="Aprobado SSU") | Q(Estado="Aprobado PROMES") ).order_by('-Gestion')
        data = SeguimientoGeneralSerializer_2(datos, many=True).data
        return Response(data)

'''Lista todos los seguimientos en estado de aprobacion aprobado ssu y aprobado promes'''
class ListarSeguimientoGeneralEstados_U(APIView):
    def get(self, request):
        datos = SeguimientoGeneral.objects.filter(Estado="Aprobado SSU").order_by('-Gestion')
        data = SeguimientoGeneralSerializer_2(datos, many=True).data
        return Response(data)


''' Lista los avances guardados en formularios generales de un determinado periodo y gestion'''
class ListaAvancesGuardadosGeneral_Eva(APIView):
    def get(self, request,gestion,idPeriodo,format=None):
        snippets= ContieneAvancesSeguimientos.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('idAvanceUnidad__idObjetivoGestion__OP','idAvanceUnidad__idObjetivoGestion__Codigo')
        serializer = ContieneAvancesSegSerializer_1(snippets, many=True)
        return Response(serializer.data)

''' Lista los avances guardados en formularios generales de un determinado periodo y gestion y unidad especifica'''
class ListaAvancesGuardadosGeneral_Eva_U(APIView):
    def get(self, request,gestion,idPeriodo,idUnidad,format=None):
        snippets= ContieneAvancesSeguimientos.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__Unidad__cod_unidad=idUnidad).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('idAvanceUnidad__idObjetivoGestion__OP','idAvanceUnidad__idObjetivoGestion__Codigo')
        serializer = ContieneAvancesSegSerializer_1(snippets, many=True)
        return Response(serializer.data)

''' Devuelve la suma de todos los avances guardados en el seguimiento general objetivos de gestion'''
class SumaTotalPorcentajeAvances(APIView):
    def get(self, request,gestion,idPeriodo,format=None):
        respuesta= ContieneAvancesSeguimientos.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets= ContieneAvancesSeguimientos.objects.values('idFormSegGeneral_id').filter(id__in=lista).annotate(sum=Sum('idAvanceUnidad__PonderacionPorc'))
        serializer = ContieneAvancesSegSerializer_2(snippets, many=True)
        return Response(serializer.data)

#DESDE AQUI PARA OPERACIONES DE FUNCIONAMIENTOS GENERAL

''' Lista los avances guardados en formularios generales de un determinado periodo y gestion y idUnidad para operaciones de funcionamiento'''
class ListaAvancesGuardadosGeneral_Eva_1(APIView):
    def get(self, request,gestion,idPeriodo,format=None):
        snippets= ContieneAvancesOperaciones.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('idAvanceUnidad__idOperacion__id')
        serializer = ContieneAvancesSegOPESerializer_1(snippets, many=True)
        return Response(serializer.data)

''' Lista los avances guardados en formularios generales de un determinado periodo y gestion para operaciones de funcionamiento'''
class ListaAvancesGuardadosGeneral_Eva_1_U(APIView):
    def get(self, request,gestion,idPeriodo,idUnidad,format=None):
        snippets= ContieneAvancesOperaciones.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__Unidad__cod_unidad=idUnidad).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('idAvanceUnidad__idOperacion__id')
        serializer = ContieneAvancesSegOPESerializer_1(snippets, many=True)
        return Response(serializer.data)

''' Devuelve la suma de todos los avances guardados en el seguimiento general operaciones de funcionamiento'''
class SumaTotalPorcentajeAvancesOPE(APIView):
    def get(self, request,gestion,idPeriodo,format=None):
        respuesta= ContieneAvancesOperaciones.objects.all().filter(idFormSegGeneral__Gestion=gestion).filter(idFormSegGeneral__periodo_id=idPeriodo).filter(idAvanceUnidad__idSeguimiento__idPOAi__Estado_version="Activo").order_by('id')
        lista=respuesta.values_list('id',flat=True)
        snippets= ContieneAvancesOperaciones.objects.values('idFormSegGeneral_id').filter(id__in=lista).annotate(sum=Sum('idAvanceUnidad__PonderacionPorc'))
        serializer = ContieneAvancesSegOPESerializer_2(snippets, many=True)
        return Response(serializer.data)