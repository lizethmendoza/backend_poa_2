from rest_framework import serializers
from .models import SeguimientoGeneral
from ..periodicidad.serializers import PeriodoSerializerPeriodicidad
from .models import FormularioSeguimientoGeneral
from .models import ContieneAvancesSeguimientos
from ..Seguimiento.serializers import AvanceEjecutadoTrimestralSerializer_4
from ..SeguimientoOperacion.serializers import AvanceEjecutadoOperacionesSerializer_1
from .models import FormularioSeguimientoGeneralOpe
from .models import ContieneAvancesOperaciones
from ..POA_General.serializers import POAGeneralSerializer

class SeguimientoGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeguimientoGeneral
        fields = '__all__'

class SeguimientoGeneralSerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = SeguimientoGeneral
        fields = '__all__'

class SeguimientoGeneralSerializer_2(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    idPOAg=POAGeneralSerializer(read_only=True)
    class Meta:
        model = SeguimientoGeneral
        fields = '__all__'

class FormularioSeguimientoGeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioSeguimientoGeneral
        fields = '__all__'

''' Serializers para generar y mostrar datos del periodo mas'''
class FormSeguimientoPeriodoGeneralSerializer(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = FormularioSeguimientoGeneral
        fields = '__all__'

class ContieneAvancesSeguimientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContieneAvancesSeguimientos
        fields = '__all__'

class ContieneAvancesSegSerializer(serializers.ModelSerializer):
    idAvanceUnidad=AvanceEjecutadoTrimestralSerializer_4(read_only=True)
    class Meta:
        model = ContieneAvancesSeguimientos
        fields = '__all__'

class FormularioSeguimientoGeneralOpeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioSeguimientoGeneralOpe
        fields = '__all__'

class FormularioSeguimientoGeneralOpeSerializer_1(serializers.ModelSerializer):
    periodo=PeriodoSerializerPeriodicidad(read_only=True)
    class Meta:
        model = FormularioSeguimientoGeneralOpe
        fields = '__all__'

class ContieneAvancesOperacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContieneAvancesOperaciones
        fields = '__all__'

class ContieneAvancesOperacionesSerializerOPE(serializers.ModelSerializer):
    idAvanceUnidad=AvanceEjecutadoOperacionesSerializer_1(read_only=True)
    class Meta:
        model = ContieneAvancesOperaciones
        fields = '__all__'


class ContieneAvancesSegSerializer_1(serializers.ModelSerializer):
    idAvanceUnidad=AvanceEjecutadoTrimestralSerializer_4(read_only=True)
    idFormSegGeneral=FormSeguimientoPeriodoGeneralSerializer(read_only=True)
    class Meta:
        model = ContieneAvancesSeguimientos
        fields = '__all__'

class ContieneAvancesSegSerializer_2(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    class Meta:
        model = ContieneAvancesSeguimientos
        fields = ('idFormSegGeneral_id','sum')

class ContieneAvancesSegOPESerializer_1(serializers.ModelSerializer):
    idAvanceUnidad=AvanceEjecutadoOperacionesSerializer_1(read_only=True)
    idFormSegGeneral=FormularioSeguimientoGeneralOpeSerializer_1(read_only=True)
    class Meta:
        model = ContieneAvancesOperaciones
        fields = '__all__'

class ContieneAvancesSegOPESerializer_2(serializers.ModelSerializer):
    sum = serializers.DecimalField(max_digits=19, decimal_places=2)
    class Meta:
        model = ContieneAvancesOperaciones
        fields = ('idFormSegGeneral_id','sum')