from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import SeguimientoGeneral
from .models import FormularioSeguimientoGeneral
from .models import ContieneAvancesSeguimientos
from .models import FormularioSeguimientoGeneralOpe
from .models import ContieneAvancesOperaciones
# Register your models here.

@admin.register(SeguimientoGeneral)
class SeguimientoGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","Estado","periodo","idPOAg"]

@admin.register(FormularioSeguimientoGeneral)
class FormularioSeguimientoGeneralAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","periodo","idSegGeneral"]

@admin.register(ContieneAvancesSeguimientos)
class ContieneAvancesSeguimientosAdmin(admin.ModelAdmin):
    list_display = ["pk", "idAvanceUnidad", "idFormSegGeneral"]


@admin.register(FormularioSeguimientoGeneralOpe)
class FormularioSeguimientoGeneralOpeAdmin(admin.ModelAdmin):
    list_display = ["pk", "Gestion", "FechaElaboracion","periodo","idSegGeneral"]

@admin.register(ContieneAvancesOperaciones)
class ContieneAvancesOperacionesAdmin(admin.ModelAdmin):
    list_display = ["pk", "idAvanceUnidad", "idFormSegGeneral"]