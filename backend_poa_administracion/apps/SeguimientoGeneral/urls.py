from django.urls import path
from .views import ListarSeguimientoGeneral
from .views import GuardarSeguimientoGeneral
from .views import ListarSeguimientoGPP
from .views import GuardarFormSeguimientoGeneral
from .views import GuardarAvancesGenerales
from .views import FormulariosSeguimientoGeneral
from .views import ListaFormularioSeguimientoGeneral
from .views import ListaAvancesGuardadosGeneral
from .views import GuardarFormSeguimientoGeneralOPE
from .views import GuardarAvancesGeneralesOPE
from .views import FormulariosSeguimientoGeneralOPE
from .views import ListaFormularioSeguimientoGeneralOPE
from .views import ListaAvancesGuardadosGeneralOPE
from .views import ListaAvancesGuardadosGeneral_U
from .views import ListaAvancesGuardadosGeneralOPE_U
from .views import actualizarSeguimientoGeneral
from .views import ListarSeguimientoGeneralEstados
from .views import ListarSeguimientoGeneralEstados_U
from .views import ListaAvancesGuardadosGeneral_Eva
from .views import SumaTotalPorcentajeAvances
from .views import ListaAvancesGuardadosGeneral_Eva_1
from .views import SumaTotalPorcentajeAvancesOPE
from .views import ListaAvancesGuardadosGeneral_Eva_U
from .views import ListaAvancesGuardadosGeneral_Eva_1_U

urlpatterns = [
    path('ListarSeguimientoGeneral/', ListarSeguimientoGeneral.as_view(),name="ListarSeguimientoGeneral"),
    path('GuardarSeguimientoGeneral/', GuardarSeguimientoGeneral.as_view(),name="GuardarSeguimientoGeneral"),
    path('ListarSeguimientoGPP/<int:gestion>/<int:periodo>/<int:idPOA>/', ListarSeguimientoGPP.as_view(),name="ListarSeguimientoGPP"),
    path('GuardarFormSeguimientoGeneral/', GuardarFormSeguimientoGeneral.as_view(),name="GuardarFormSeguimientoGeneral"),
    path('GuardarAvancesGenerales/', GuardarAvancesGenerales.as_view(),name="GuardarAvancesGenerales"),
    path('FormulariosSeguimientoGeneral/<int:idSeg>/<int:gestion>/<int:IdPer>/', FormulariosSeguimientoGeneral.as_view(),name="FormulariosSeguimientoGeneral"),
    path('ListaFormularioSeguimientoGeneral/<int:idForm>/', ListaFormularioSeguimientoGeneral.as_view(),name="ListaFormularioSeguimientoGeneral"),
    path('ListaAvancesGuardadosGeneral/<int:idForm>/', ListaAvancesGuardadosGeneral.as_view(),name="ListaAvancesGuardadosGeneral"),
    path('GuardarFormSeguimientoGeneralOPE/', GuardarFormSeguimientoGeneralOPE.as_view(),name="GuardarFormSeguimientoGeneralOPE"),
    path('GuardarAvancesGeneralesOPE/', GuardarAvancesGeneralesOPE.as_view(),name="GuardarAvancesGeneralesOPE"),
    path('FormulariosSeguimientoGeneralOPE/<int:idSeg>/<int:gestion>/<int:IdPer>/', FormulariosSeguimientoGeneralOPE.as_view(),name="FormulariosSeguimientoGeneralOPE"),
    path('ListaFormularioSeguimientoGeneralOPE/<int:idForm>/', ListaFormularioSeguimientoGeneralOPE.as_view(),name="ListaFormularioSeguimientoGeneralOPE"),
    path('ListaAvancesGuardadosGeneralOPE/<int:idForm>/', ListaAvancesGuardadosGeneralOPE.as_view(),name="ListaAvancesGuardadosGeneralOPE"),
    path('ListaAvancesGuardadosGeneral_U/<int:idForm>/<int:idU>/', ListaAvancesGuardadosGeneral_U.as_view(),name="ListaAvancesGuardadosGeneral_U"),
    path('ListaAvancesGuardadosGeneralOPE_U/<int:idForm>/<int:idU>/', ListaAvancesGuardadosGeneralOPE_U.as_view(),name="ListaAvancesGuardadosGeneralOPE_U"),
    path('actualizarSeguimientoGeneral/<int:pk>/', actualizarSeguimientoGeneral.as_view(),name="actualizarSeguimientoGeneral"),
    path('ListarSeguimientoGeneralEstados/', ListarSeguimientoGeneralEstados.as_view(),name="ListarSeguimientoGeneralEstados"),
    path('ListarSeguimientoGeneralEstados_U/', ListarSeguimientoGeneralEstados_U.as_view(),name="ListarSeguimientoGeneralEstados_U"),
    path('ListaAvancesGuardadosGeneral_Eva/<int:gestion>/<int:idPeriodo>/', ListaAvancesGuardadosGeneral_Eva.as_view(),name="ListaAvancesGuardadosGeneral_Eva"),
    path('SumaTotalPorcentajeAvances/<int:gestion>/<int:idPeriodo>/', SumaTotalPorcentajeAvances.as_view(),name="SumaTotalPorcentajeAvances"),
    path('ListaAvancesGuardadosGeneral_Eva_1/<int:gestion>/<int:idPeriodo>/', ListaAvancesGuardadosGeneral_Eva_1.as_view(),name="ListaAvancesGuardadosGeneral_Eva_1"),
    path('SumaTotalPorcentajeAvancesOPE/<int:gestion>/<int:idPeriodo>/', SumaTotalPorcentajeAvancesOPE.as_view(),name="SumaTotalPorcentajeAvancesOPE"),
    path('ListaAvancesGuardadosGeneral_Eva_U/<int:gestion>/<int:idPeriodo>/<int:idUnidad>/', ListaAvancesGuardadosGeneral_Eva_U.as_view(),name="ListaAvancesGuardadosGeneral_Eva_U"),
    path('ListaAvancesGuardadosGeneral_Eva_1_U/<int:gestion>/<int:idPeriodo>/<int:idUnidad>/', ListaAvancesGuardadosGeneral_Eva_1_U.as_view(),name="ListaAvancesGuardadosGeneral_Eva_1_U"),

]