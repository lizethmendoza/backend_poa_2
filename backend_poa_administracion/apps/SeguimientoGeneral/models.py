from django.db import models
from .. periodicidad.models import Periodo
from .. Seguimiento.models import AvanceEjecutadoTrimestral
from .. SeguimientoOperacion.models import AvanceEjecutadoOperaciones
from ..POA_General.models import POAGeneral

# Create your models here.

class SeguimientoGeneral(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField(auto_now=True)
    ESTADO_CHOICES= (
        ('Proceso seguimiento', 'Proceso seguimiento'),
        ('Proceso Revision', 'Proceso Revision'),
        ('Proceso Aprobacion', 'Proceso Aprobacion'),
        ('Observado', 'Observado'),
        ('Aprobado PROMES', 'Aprobado PROMES'),
        ('Vigente', 'Vigente'),
        ('Aprobado SSU', 'Aprobado SSU'),
    )
    Estado = models.CharField(max_length=50, choices=ESTADO_CHOICES, default='Proceso seguimiento')
    periodo = models.ForeignKey(Periodo,on_delete=models.CASCADE)
    idPOAg = models.ForeignKey(POAGeneral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Lista de seguimientos Generales")

class FormularioSeguimientoGeneral(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField()
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    idSegGeneral=models.OneToOneField(SeguimientoGeneral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formulario Seguimiento General")

class ContieneAvancesSeguimientos(models.Model):
    idAvanceUnidad=models.ForeignKey(AvanceEjecutadoTrimestral,on_delete=models.CASCADE)
    idFormSegGeneral=models.ForeignKey(FormularioSeguimientoGeneral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Avances en General")

class FormularioSeguimientoGeneralOpe(models.Model):
    Gestion = models.IntegerField()
    FechaElaboracion=models.DateField()
    periodo=models.ForeignKey(Periodo,on_delete=models.CASCADE)
    idSegGeneral=models.OneToOneField(SeguimientoGeneral,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Formulario Seguimiento General - Operaciones Funcionamiento")

class ContieneAvancesOperaciones(models.Model):
    idAvanceUnidad=models.ForeignKey(AvanceEjecutadoOperaciones,on_delete=models.CASCADE)
    idFormSegGeneral=models.ForeignKey(FormularioSeguimientoGeneralOpe,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Avances en General - Operaciones Funcionamiento")