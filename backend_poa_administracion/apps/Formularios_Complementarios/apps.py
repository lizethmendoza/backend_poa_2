from django.apps import AppConfig


class FormulariosComplementariosConfig(AppConfig):
    name = 'Formularios_Complementarios'
