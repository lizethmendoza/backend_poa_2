from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from django.utils.translation import ugettext_lazy as _
from .models import accionesCortoPlazo,indicador
from .models import indicadoresSeleccionados

@admin.register(accionesCortoPlazo)
class accionesCortoPlazoAdmin(ImportExportModelAdmin):
    list_display = ["pk", "CMedianoPlazo", "CCortoPlazo","CAccionesEspecificas","DescripcionACP","Resultado"]

@admin.register(indicador)
class indicadorAdmin(ImportExportModelAdmin):
    list_display = ["pk", "Codigo", "Denominacion","LineaBase","MetaGestion","MedioVerificacion","UnidadResponsable","ProgFisicaT1er","ProgFisicaT2do","ProgFisicaT3er","ProgFisicaT4to","IdAccionCortoPlazo"]

@admin.register(indicadoresSeleccionados)
class indicadoresSeleccionadosAdmin(admin.ModelAdmin):
    list_display = ["pk", "idIndicadorS", "valor","gestion","idUnidad","idPOA"]