from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from tablib import Dataset
from .models import accionesCortoPlazo
from .serializers import accionesCortoPlazoSerializer
from .models import indicador
from .serializers import indicadorSerializer
from .serializers import AccionesIndicadoresSerializer
from .models import indicadoresSeleccionados
from .serializers import indicadoresSeleccionadosSerializer
from .serializers import indicadoresSeleccionadosDetalleSerializer
from django.db.models import Count

class ListaAccionesCortoPlazo(APIView):
    def get(self, request,format=None):
        Acciones = accionesCortoPlazo.objects.all()
        serializer = accionesCortoPlazoSerializer(Acciones, many=True)
        return Response(serializer.data)

class IndicadorDetalleID(APIView):
    """Lista todos los indicadores con id de la Accion a Corto Plazo"""
    def get(self, request, pk, format=None):
        snippets= indicador.objects.filter(IdAccionCortoPlazo=pk).order_by('Codigo')
        serializer = indicadorSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaIndicadores(APIView):
    def get(self, request,format=None):
        Indicadores = indicador.objects.all()
        serializer = indicadorSerializer(Indicadores, many=True)
        return Response(serializer.data)

class IndicadoresDetalleID(APIView):
    """Obtiene el indicador igual al id"""
    def get(self, request, pk, format=None):
        snippets= indicador.objects.filter(id=pk).order_by('id')
        serializer = AccionesIndicadoresSerializer(snippets, many=True)
        return Response(serializer.data)

class ListaIndicadoresSeleccionadosDetalle(APIView):
    def get(self, request,format=None):
        IndicadoresSele = indicador.objects.all()
        serializer = AccionesIndicadoresSerializer(IndicadoresSele, many=True)
        return Response(serializer.data)

# lista todos los indicadores que fueron seleccionados con una gestion y unidad 
class ListaIndicadoresSeleccionados(APIView):
    def get(self, request,gestion,idunidad,idPOA,format=None):
        IndicadoresSele = indicadoresSeleccionados.objects.distinct('idIndicadorS__id').filter(gestion=gestion).filter(idUnidad=idunidad).filter(idPOA=idPOA).order_by('idIndicadorS__id')
        serializer = indicadoresSeleccionadosDetalleSerializer(IndicadoresSele, many=True)
        return Response(serializer.data)

# guarda todos los indicadores seleccionados
class GuardarIndicadoresSeleccionados(APIView):
    def post(self, request, format=None):
        serializer = indicadoresSeleccionadosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

''' Lista los indicadores seleccionados de una determinada gestion, unidad y idPOA'''
class ListaIndicadoresSeleccionadosGestion(APIView):
    def get(self, request,gestion,idunidad,idPOA,format=None):
        IndicadoresSele = indicadoresSeleccionados.objects.filter(gestion=gestion).filter(idUnidad=idunidad).filter(idPOA=idPOA)
        serializer = indicadoresSeleccionadosSerializer(IndicadoresSele, many=True)
        return Response(serializer.data)

''' ACTUALIZAR UN REGISTRO DE MS''' 
class actualizarindicadoresSeleccionados(APIView):
    def get_object(self, pk):
        try:
            return indicadoresSeleccionados.objects.get(pk=pk)
        except indicadoresSeleccionados.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = indicadoresSeleccionadosSerializer(snippet)
        return Response(serializer.data) 
           
    ''' elimina un registro'''
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

''' para subir desde excel'''
def simple_upload1(request):
    if request.method == 'POST':
        accionesCortoPlazo_resource = accionesCortoPlazoResource()
        dataset = Dataset()
        new_accionesCortoPlazo = request.FILES['myfile']
        imported_data = dataset.load(new_accionesCortoPlazo.read())
        result = accionesCortoPlazo_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            accionesCortoPlazo_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload1.html')

def simple_upload2(request):
    if request.method == 'POST':
        indicador_resource = indicadorResource()
        dataset = Dataset()
        new_indicador = request.FILES['myfile']
        imported_data = dataset.load(new_indicador.read())
        result = indicador_resource.import_data(dataset, dry_run=True)  # Test the data import

        if not result.has_errors():
            indicador_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'core/simple_upload2.html')