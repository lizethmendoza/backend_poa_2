from django.urls import path
from .views import ListaAccionesCortoPlazo
from .views import IndicadorDetalleID
from .views import ListaIndicadores
from .views import IndicadoresDetalleID
from .views import ListaIndicadoresSeleccionadosDetalle
from .views import ListaIndicadoresSeleccionadosGestion
from .views import GuardarIndicadoresSeleccionados
from .views import ListaIndicadoresSeleccionados
from .views import actualizarindicadoresSeleccionados

urlpatterns = [
    path('ListaAccionesCortoPlazo/', ListaAccionesCortoPlazo.as_view(),name="ListaAccionesCortoPlazo"),
    path('ListaIndicadores/', ListaIndicadores.as_view(),name="ListaIndicadores"),
    path('IndicadorDetalleID/<int:pk>/', IndicadorDetalleID.as_view(), name="IndicadorDetalleID"),
    path('ListaIndicadoresSeleccionadosDetalle/', ListaIndicadoresSeleccionadosDetalle.as_view(), name="ListaIndicadoresSeleccionadosDetalle"),
    path('IndicadoresDetalleID/<int:pk>/', IndicadoresDetalleID.as_view(), name="IndicadoresDetalleID"),
    path('ListaIndicadoresSeleccionadosGestion/<int:gestion>/<int:idunidad>/<int:idPOA>/', ListaIndicadoresSeleccionadosGestion.as_view(), name="ListaIndicadoresSeleccionadosGestion"),
    path('ListaIndicadoresSeleccionados/<int:gestion>/<int:idunidad>/<int:idPOA>/', ListaIndicadoresSeleccionados.as_view(), name="ListaIndicadoresSeleccionados"),
    path('GuardarIndicadoresSeleccionados/', GuardarIndicadoresSeleccionados.as_view(), name="GuardarIndicadoresSeleccionados"),
    path('actualizarindicadoresSeleccionados/<int:pk>/', actualizarindicadoresSeleccionados.as_view(), name="actualizarindicadoresSeleccionados"),
]

