from rest_framework import serializers
from .models import accionesCortoPlazo
from .models import indicador
from .models import  indicadoresSeleccionados

class accionesCortoPlazoSerializer(serializers.ModelSerializer):
    class Meta:
        model = accionesCortoPlazo
        fields = '__all__'

class indicadorSerializer(serializers.ModelSerializer):
    class Meta:
        model = indicador
        fields = '__all__'

class AccionesIndicadoresSerializer(serializers.ModelSerializer):
    IdAccionCortoPlazo = accionesCortoPlazoSerializer(read_only=True)
    class Meta:
        model = indicador
        fields = '__all__'

class indicadoresSeleccionadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = indicadoresSeleccionados
        fields = '__all__'

class indicadoresSeleccionadosDetalleSerializer(serializers.ModelSerializer):
    idIndicadorS = AccionesIndicadoresSerializer(read_only=True)
    class Meta:
        model = indicadoresSeleccionados
        fields = '__all__'
        