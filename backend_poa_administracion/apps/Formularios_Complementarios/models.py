from django.db import models
from ..responsablesInformacion.models import Unidad
from ..Poai.models import POAi
#Modelo para crear la tabla de Acciones a corto plazo prueba
class accionesCortoPlazo(models.Model):
    id = models.IntegerField(primary_key=True)
    CMedianoPlazo=models.CharField(max_length=50)
    CCortoPlazo=models.CharField(max_length=500)
    CAccionesEspecificas=models.CharField(max_length=50)
    DescripcionACP = models.CharField(max_length=500)
    Resultado = models.CharField(max_length=500)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Acciones Corto Plazo (Objetivos Institucionales)")

#indicadores  de acuerdo a las acciones a corto plazo
class indicador(models.Model):
    id = models.IntegerField(primary_key=True)
    Codigo=models.IntegerField()
    Denominacion = models.CharField(max_length=500)
    LineaBase=models.IntegerField(default=0)
    MetaGestion=models.IntegerField(default=0)
    MedioVerificacion=models.CharField(max_length=500)
    UnidadResponsable=models.CharField(max_length=500)  #porque este campo verificar
    ProgFisicaT1er = models.CharField(max_length=100)
    ProgFisicaT2do = models.CharField(max_length=100)
    ProgFisicaT3er = models.CharField(max_length=100)
    ProgFisicaT4to = models.CharField(max_length=100)
    IdAccionCortoPlazo=models.ForeignKey(accionesCortoPlazo,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Indicadores")

# simulacion de una tabla temporal para el guardado de los indicadores seleccionados
class indicadoresSeleccionados(models.Model):
    idIndicadorS=models.ForeignKey(indicador,on_delete=models.CASCADE)   #esto es la lleve foranea del indicador 
    valor = models.CharField(max_length=50)
    gestion=models.IntegerField(default=0)
    idUnidad=models.ForeignKey(Unidad,on_delete=models.PROTECT)
    idPOA=models.ForeignKey(POAi,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    class Meta:
        verbose_name_plural = ("Indicadores Seleccionados")