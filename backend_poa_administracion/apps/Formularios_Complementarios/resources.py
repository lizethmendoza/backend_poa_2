from import_export import resources


from .models import accionesCortoPlazo
from .models import indicador

class accionesCortoPlazoResource(resources.ModelResource):
    class Meta:
        model = accionesCortoPlazo

class indicadorResource(resources.ModelResource):
    class Meta:
        model = indicador
    
